import { Injectable } from '@angular/core';

import { SERVER_API_URL } from '../../app.constants';
import { ProfileInfo } from './profile-info.model';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ProfileService {

    private profileInfoUrl = SERVER_API_URL + 'api/profile-info';
    private profileInfo: Promise<ProfileInfo>;

    constructor(private http: HttpClient) { }

    getProfileInfo(): Promise<ProfileInfo> {
        if (!this.profileInfo) {
            this.profileInfo = this.http.get<ProfileInfo>(this.profileInfoUrl)
                .map((res: ProfileInfo) => {
                    const pi = new ProfileInfo();
                    pi.activeProfiles = res.activeProfiles;
                    pi.ribbonEnv = res.ribbonEnv;
 //                   pi.inProduction = res.activeProfiles.includes('prod');
  //                  pi.swaggerEnabled = res.activeProfiles.includes('swagger');
                    return pi;
            }).toPromise();
        }
        return this.profileInfo;
    }
}
