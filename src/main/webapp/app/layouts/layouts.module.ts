import { article_declarations } from './../entities/article/article.constants';
import { client_declarations } from './../entities/client/client.constants';
import { ActiveMenuDirective } from './main/active-menu.directive';
 
import './../vendor.ts';
 
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Ng2Webstorage } from 'ngx-webstorage';
import { StockSharedModule, UserRouteAccessService } from './../shared';
import { PaginationConfig } from './../blocks/config/uib-pagination.config';
import { CustomFormsModule } from 'ng5-validation'
// jhipster-needle-angular-add-module-import JHipster will add new module here
import {
    ProductComponent,
    ProductDetailComponent,
    ProductDialogComponent,
    ProductDeleteDialogComponent,
    ProductPopupComponent,
    ProductDeletePopupComponent,
    ProductService,
    ProductPopupService,
    ProductResolvePagingParams,
} from './../entities/product/index';
import {
    FooterComponent,
    ProfileService,
    PageRibbonComponent,
    ErrorComponent
} from './';
import { StockLayoutsRoutingModule, LAYOUT_COMPONENTS, LAYOUT_ENTRIES, LAYOUT_PROVIDERS } from './layouts.routes';
import { CommonModule } from '@angular/common';
import { NgxBarcodeModule } from 'ngx-barcode';
import {NgxPaginationModule} from 'ngx-pagination';
 
@NgModule({
    imports: [
        CommonModule,
        CustomFormsModule,
        NgxPaginationModule,
        StockSharedModule,
        StockLayoutsRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-' }),
        NgxBarcodeModule
    ],
    declarations: [
        LAYOUT_COMPONENTS,
        ErrorComponent,
        PageRibbonComponent,
        FooterComponent,
    ],
    entryComponents: [
        LAYOUT_ENTRIES
    ],
    providers: [
        LAYOUT_PROVIDERS,
        ProfileService,
        PaginationConfig,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class StockLayoutsModule { }