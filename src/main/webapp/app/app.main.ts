import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { ProdConfig } from './blocks/config/prod.config';
import { StockAppModule } from './app.module';
import {enableProdMode} from '@angular/core';
import { isDevMode } from '@angular/core';
ProdConfig();

if (module['hot']) {
    module['hot'].accept();
}
console.log("isDevMode()"+isDevMode());
//enableProdMode();
platformBrowserDynamic().bootstrapModule(StockAppModule)
.then((success) => console.log(`Application started`))
.catch((err) => console.error(err));
