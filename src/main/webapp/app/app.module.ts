import './vendor.ts';

import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { StockSharedModule, UserRouteAccessService } from './shared';

import { ActiveMenuDirective } from './layouts/main/active-menu.directive';
import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { LayoutsComponent } from './layouts/layouts.component';
import { MenuItems } from './shared/menu-items/menu-items';
import { Ng2Webstorage } from 'ngx-webstorage';
import { PaginationConfig } from './blocks/config/uib-pagination.config';
import { StockAccountModule } from './account/account.module';
import { StockAppRoutingModule } from './app-routing.module';
import { StockLayoutsModule } from './layouts/layouts.module';
import { StockLoginComponent } from './login/login.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { XAuthInterceptor } from './blocks/interceptor/x-auth.interceptor';
import { NgxBarcodeModule } from 'ngx-barcode';
import {NgbModule,NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
// jhipster-needle-angular-add-module-import JHipster will add new module here

@NgModule({
    imports: [
        BrowserModule,
        StockAppRoutingModule,
        NgbModule.forRoot(),
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-' }),
        StockSharedModule,
        StockLayoutsModule,
        StockAccountModule,
        NgxBarcodeModule,
        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    declarations: [
        AppComponent,
        LayoutsComponent,
        StockLoginComponent,
        ActiveMenuDirective
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: XAuthInterceptor,
            multi: true,
        },
        UserRouteAccessService,
        MenuItems,
        NgbActiveModal
    ],
    bootstrap: [AppComponent]
})
export class StockAppModule { }
