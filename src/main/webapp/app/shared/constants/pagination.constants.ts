export const ITEMS_PER_PAGE = 10;
export const ITEMS_PER_FILTER_PAGE = 5;
export const ITEMS_MAX = 1000;
