import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
 
import { SERVER_API_URL } from '../../app.constants';
import { createRequestOption } from '../model/request-util';
import { HttpResponse, HttpClient } from '@angular/common/http';
 
@Injectable()
export class MainService {
 
    constructor(private http: HttpClient) { }
 
    convertDateToString(date) {
        const yyyy = date.getFullYear().toString();
        const MM = (date.getMonth() + 1).toString();
        const dd = date.getDate().toString();
        const hh = date.getHours().toString();
        const mm = date.getMinutes().toString();
        const ss = date.getSeconds().toString();
        return yyyy + '-' + (MM[1] ? MM : '0' + MM[0]) + '-' + (dd[1] ? dd : '0' + dd[0]) + ' '
            + (hh[1] ? hh : '0' + hh[0]) + ':' + (mm[1] ? mm : '0' + mm[0]) + ':' + (ss[1] ? ss : '0' + ss[0]);
    }
 
    convertDateForInputDate(date: Date) {
        return date.toISOString().slice(0, 10)
    }
 
    convertDateForInputDateTime(date: Date) {
        return date.toISOString().slice(0, 16)
    }
 
    convertDateForInputDateLocal(date: Date) {
        return date.toISOString().slice(0, 19)
    }
 
    getToDayFirstHour(date) {
        const yyyy = date.getFullYear().toString();
        const mm = (date.getMonth() + 1).toString();
        const dd = date.getDate().toString();
        const hh = date.getHours().toString();
        const MM = date.getMinutes().toString();
        const ss = date.getSeconds().toString();
        return yyyy + '-' + (mm[1] ? mm : '0' + mm[0]) + '-' + (dd[1] ? dd : '0' + dd[0]) + ' '
            + ('00:00:00');
    }
    convertStringDateForInputZonedDate(date){
        const yyyy = date.split('-')[0];
        const mm = date.split('-')[1];
        const dd = date.split('-')[2];
        return yyyy + '-' + mm + '-' + dd + 'T00:00:00';
    }
    getLastDateInCurrentYearInStringFormat(date) {
        const yyyy = date.getFullYear().toString();
        const hh = date.getHours().toString();
        const MM = date.getMinutes().toString();
        const ss = date.getSeconds().toString();
        return yyyy + '-' + 12 + '-' + 31 + ' '
            + ('00:00:00');
    }
 
    getLastDateInCurrentYear(date) {
        const yyyy = date.getFullYear().toString();
        return new Date(yyyy, 12, 31, 0, 0, 0);
    }
 
    getToDayEndHour(date) {
        const yyyy = date.getFullYear().toString();
        const mm = (date.getMonth() + 1).toString();
        const dd = date.getDate().toString();
        const hh = date.getHours().toString();
        const MM = date.getMinutes().toString();
        const ss = date.getSeconds().toString();
        return yyyy + '-' + (mm[1] ? mm : '0' + mm[0]) + '-' + (dd[1] ? dd : '0' + dd[0]) + ' '
            + ('23:59:59');
    }
 
    toTimestamp(date) {
        return date + ' 00:00:00.000';
    }
 
    addDays(theDate, days) {
        return new Date(theDate.getTime() + days * 24 * 60 * 60 * 1000);
    }
}