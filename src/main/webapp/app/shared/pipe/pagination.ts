import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'paginate',
    pure: false
})
export class PaginationFilterPipe implements PipeTransform {
    transform(items: any[], config: any): any[] {
        if (!items || !config) {
            return items;
        }
        const start = config.start;
        const limit = config.limit;
        return items.slice(start, limit);
        // filter items array, items which match and return true will be kept, false will be filtered out
        // return items.filter((item: any) => this.applyFilter(item, filter));
    }

    applyFilter(book: any, filter: any): boolean {
        for (const field in filter) {
            if (filter[field]) {
                if (typeof filter[field] === 'string') {
                    if (book[field].toLowerCase().indexOf(filter[field].toLowerCase()) === -1) {
                        return false;
                    }
                } else if (typeof filter[field] === 'number') {
                    if (book[field] !== filter[field]) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
