import { HttpParams } from '@angular/common/http';

export const createRequestOption = (req?: any): HttpParams => {
    let params = new HttpParams();
    if (req) {
        // Begin assigning parameters
        if (req.state) {
            params = params.append('state', req.state);
        }
        if (req.page) {
            params = params.append('page', req.page);
        }
        if (req.size) {
            params = params.append('size', req.size);
        }
        if (req.query) {
            params = params.append('q', req.query);
        }
        if (req.filter) {
            params = params.append('filter', req.filter);
        }
        if (req.sort) {
            for (const s of req.sort) {
                params = params.append('sort', s);
            }
        }
    }
    return params;
};
