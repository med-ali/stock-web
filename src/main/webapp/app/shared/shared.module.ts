import { LoginService } from '../login/login.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TitleComponent } from './../layouts/main/title/title.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DatePipe, CommonModule } from '@angular/common';
import { ClickOutsideModule } from 'ng-click-outside';
import { ToggleFullScreenDirective } from './fullscreen/toggle-fullscreen.directive';
import { PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface, PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import {
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    CardToggleDirective,
    CardComponent,
    ModalBasicComponent,
    ModalAnimationComponent,
    SpinnerComponent,
    FilterComponent,

    StockSharedLibsModule,
    StockSharedCommonModule,
    CSRFService,
    AuthServerProvider,
    AccountService,
    UserService,
    PrintService,
    FilterService,
    StateStorageService,
    Principal,
    HasAnyAuthorityDirective,
    MainService,
    PagerService,
    PaginationFilterPipe,
    SpinnerService,
} from './';
import { HttpClientModule } from '@angular/common/http';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true
};

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        StockSharedLibsModule,
        StockSharedCommonModule,
        PerfectScrollbarModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [
        ToggleFullScreenDirective,
        AccordionAnchorDirective,
        AccordionLinkDirective,
        AccordionDirective,
        CardToggleDirective,
        TitleComponent,
        CardComponent,
        ModalBasicComponent,
        ModalAnimationComponent,
        SpinnerComponent,
        FilterComponent,
        PaginationFilterPipe,
        HasAnyAuthorityDirective,
        FileUploadComponent
    ],
    providers: [
        LoginService,
        AccountService,
        StateStorageService,
        Principal,
        CSRFService, 
        AuthServerProvider,
        UserService,
        PrintService,
        SpinnerService,
        FilterService,
        MainService,
        PagerService,
        DatePipe,
        PaginationFilterPipe
    ],
    entryComponents: [FileUploadComponent],
    exports: [
        BrowserAnimationsModule,
        ToggleFullScreenDirective,
        AccordionAnchorDirective,
        AccordionLinkDirective,
        AccordionDirective,
        CardToggleDirective,
        HttpClientModule,
        TitleComponent,
        CardComponent,
        ModalBasicComponent,
        ModalAnimationComponent,
        SpinnerComponent,
        FilterComponent,
        ClickOutsideModule,
        StockSharedCommonModule,
        HasAnyAuthorityDirective,
        DatePipe,
        PerfectScrollbarModule,
        FileUploadComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class StockSharedModule { }
