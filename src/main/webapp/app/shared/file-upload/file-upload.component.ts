
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit, ViewContainerRef, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { RequestOptions } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService } from 'ng-jhipster';

declare var $: any;
@Component({
    selector: 'app-file-upload',
    template: `
    <div class="modal-header">
	<h4 class="modal-title" jhiTranslate="upload.title">Upload products and articls!</h4>
	<button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
</div>
<div class="modal-body">
            <div class="swal2-content ">
            <input type="file" id="file" (change)="onFileChange($event)" [formControl]="fileInput" >
            </div>

</div>
<div class="modal-footer">
</div>
`
})
export class FileUploadComponent implements OnInit {
    @Input() url: string;
    @Input() title: string;
    @Input() search: any;
    fileInput = new FormControl();
    @Output() onFileUploaded = new EventEmitter();
    date: boolean = false;
    time: boolean = false;
    

    constructor(private http: HttpClient,
         public activeModal: NgbActiveModal,
         private jhiAlertService: JhiAlertService) {

    }

    ngOnInit() {
    }


    onFileChange(event) {
        const fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            const file: File = fileList[0];
            const formData: FormData = new FormData();
            formData.append('file', file, file.name);
            const headers = new HttpHeaders();
            /** No need to include Content-Type in Angular 4 */
            headers.append('Content-Type', 'multipart/form-data');
            headers.append('Accept', 'application/json');
            const options = { headers: headers};
            this.http.post(this.url, formData, options)
                .subscribe(
                data => {
                    this.onFileUploaded.emit('success');
                    console.log("successssssssssssss");
                    this.search;
                    this.closeModal();
                },
                error => {
                    this.closeModal();
                    
                    console.log("not successssssssssssss");
                    if (error.status !== 417 || error.error.code) {
                       // this.alertService.showModal({ error: true, message: 'error.upload' });
                       this.jhiAlertService.error(error.upload);
                    } else {
                        const messages = [];
                        Object.keys(error.error).forEach(key => {
                            messages.push("row " + key + ", columns: " + error.error[key]);
                        });
                       // this.alertService.showModal({ error: true, message: 'error.upload', messages: messages });
                       this.jhiAlertService.error(error.upload,null,null);
                    }
                }
                );
        }
    }

    openModal() {
        console.log("fffffffff");
        this.fileInput.markAsUntouched();
        this.fileInput.setValue("");
        $('#upload' + this.title).appendTo('body').modal("show");
    }
    closeModal() {
        //$('#upload' + this.title).modal("hide");
        this.activeModal.close();
    }
}
