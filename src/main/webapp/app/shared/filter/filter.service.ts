
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import { SERVER_API_URL } from '../../app.constants';
import { createRequestOption } from '../model/request-util';
import { HttpResponse, HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class FilterService {

    private resourceUrl = SERVER_API_URL + 'api/';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/';

    constructor(private http: HttpClient) { }

    queryGet(path: any, req?: any): Observable<HttpResponse<any[]>> {
        this.updateRequests(path);
        const options = createRequestOption(req);
        return this.http.get<any[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    queryPost(path: any, obj: any, req?: any): Observable<HttpResponse<any[]>> {
        this.updateRequests(path);
        const options = createRequestOption(req);
        return this.http.post<any[]>(this.resourceUrl, obj,  { params: options, observe: 'response' });
    }

    search(path: string, req?: any): Observable<HttpResponse<any[]>> {
        this.updateSearchRequests(path);
        const options = createRequestOption(req);
        return this.http.get<any[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }

    updateRequests(path) {
        this.resourceUrl = SERVER_API_URL + 'api/' + path;
    }

    updateSearchRequests(path: string) {
        this.resourceSearchUrl = SERVER_API_URL + 'api/_search/' + path;
    }

    private xcreateRequestOption(req?: any): HttpParams {
        let options: HttpParams = new HttpParams();
        if (req) {
            Object.keys(req).forEach((key) => {
                if (key !== 'sort') {
                    options = options.set(key, req[key]);
                }
            });
            if (req.sort) {
                req.sort.forEach((val) => {
                    options = options.append('sort', val);
                });
            }
        }
        return options;
    }
}
