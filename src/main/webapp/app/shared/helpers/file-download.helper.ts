import { Response } from '@angular/http';
import { saveAs } from 'file-saver';

/**
 * Saves a file by opening file-save-as dialog in the browser
 * using file-save library.
 * @param blobContent file content as a Blob
 * @param fileName name file should be saved as
 */
export const saveFile = (blobContent, fileName: string) => {
    // var contentBuffer = base64ToArrayBuffer(blobContent);
    const blob = new Blob([blobContent], { type: 'application/data' });
    saveAs(blob, fileName);
};

export const getFileNameFromResponseContentDisposition = (headers) => {
    const contentDisposition = headers.get('content-disposition') || '';
   
    const matches = /filename=([^;]+)/ig.exec(contentDisposition);
    console.log("matches",matches)
    const fileName = (matches[1] || 'untitled').trim();
    return fileName;
};
