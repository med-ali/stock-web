import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { SpinnerState } from './spinner.state';

@Injectable()
export class SpinnerService {
    loading: boolean;
    private spinnerSubject = new Subject<SpinnerState>();
    spinnerState = this.spinnerSubject.asObservable();
    constructor() { }
    show() {
        // set timeout is used to escape the error ExpressionChangedAfterItHasBeenCheckedError
        setTimeout(() => {
            this.loading = true;
            this.spinnerSubject.next(<SpinnerState>{ show: true });
        })

    }
    hide() {
        setTimeout(() => {
            this.loading = false;
            this.spinnerSubject.next(<SpinnerState>{ show: false });
        })

    }
}
