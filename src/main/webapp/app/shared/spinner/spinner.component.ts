import { Component, Input, OnDestroy, Inject, ViewEncapsulation, OnInit } from '@angular/core';
import { Spinkit } from './spinkits';
import { Router, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { SpinnerState } from './spinner.state';
import { SpinnerService } from './spinner.service';

@Component({
    selector: 'app-spinner',
    templateUrl: './spinner.component.html',
    styleUrls: [
        './spinner.component.scss',
        './spinkit-css/sk-line-material.scss'
    ],
    encapsulation: ViewEncapsulation.None
})
export class SpinnerComponent implements OnInit, OnDestroy {
    public isSpinnerVisible = true;
    public Spinkit = Spinkit;
    show = false;
    private subscription: Subscription;
    @Input() public backgroundColor = 'rgba(255, 255, 255, 0.8)';
    @Input() public spinner = Spinkit.skLine;
    constructor(private router: Router, @Inject(DOCUMENT) private document: Document, private spinnerService: SpinnerService) {
        this.router.events.subscribe((event) => {
            if (event instanceof NavigationStart) {
                this.isSpinnerVisible = true;
            } else if (event instanceof NavigationEnd || event instanceof NavigationCancel || event instanceof NavigationError) {
                this.isSpinnerVisible = false;
            }
        }, () => {
            this.isSpinnerVisible = false;
        });
    }

    ngOnInit() {
        this.subscription = this.spinnerService.spinnerState
            .subscribe((state: SpinnerState) => {
                this.show = state.show;
            });
    }

    ngOnDestroy(): void {
        this.isSpinnerVisible = false;
        this.subscription.unsubscribe();
    }
}
