import { Injectable } from '@angular/core';

interface SizedEvent {
  width: number;
  height: number;
}

@Injectable()
export class PrintService {

  public print(printEl, popupWinindow: Window) {
    popupWinindow.document.open();
    popupWinindow.document.write(`<html><head>
      <style>
      @media print {
        @page { size:85mm 110mm; margin: 2mm }
        .page-break { display: block; page-break-before: always; }
        body { -webkit-print-color-adjust: exact; }
       }
       </style>
      </head><body onload="window.print()"> ${printEl} </html>`);
    popupWinindow.document.close();
  }

  public printBarcode(printEl, popupWin: Window) {

    // const popupWin = window.open('', '_blank', 'top=0,left=0,width=1200,height=900');
    popupWin.document.open();
    popupWin.document.write(`<html><head>
            <style>
            @media print { body { -webkit-print-color-adjust: exact; }
            @page {
              size: 77mm 33mm; /* landscape */
              /* you can also specify margins here: */
              margin: 45mm;
              margin-right: 65mm; /* for compatibility with both A4 and Letter */
            } }
            </style>
            </head><body onload="window.print();"> ${printEl}</body></html>`);
    popupWin.document.close();
  }

  public printReceipt(printEl: HTMLElement) {

    const popupWin = window.open('', '_blank', 'top=0,left=0,width=400,height=300');
    popupWin.document.open();
    popupWin.document.write(`<html><head>
            <style>
            body {
              font-family: 'Muli', 'Helvetica', Arial, sans-serif;
              font-size:14px;
            }
            table > tbody > tr > td {
              border-top: 1px solid #efefef;
            }
            @media all {
             .page-break { display: none; }
            }
            @media print {
             .page-break { display: block; page-break-before: always; }
            }
            </style>
            </head><body onload="window.print();"> ${printEl}</body></html>`);
    popupWin.document.close();
  }

  public printWholePaper(printEl: HTMLElement) {

    const popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`<html><head>
            <style>
            body {
              font-family: 'Muli', 'Helvetica', Arial, sans-serif;
              font-size:14px;
            }
            table > tbody > tr > td {
              border-top: 1px solid #efefef;
            }
            @media all {
             .page-break { display: none; }
            }
            @media print {
             .page-break { display: block; page-break-before: always; }
             .footer { position: fixed;bottom: 0; }
            }
            </style>
            </head><body onload="window.print();"> ${printEl}</body></html>`);
    popupWin.document.close();
  }
}
