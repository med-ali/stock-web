import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { InventoryService } from './inventory.service';
import { ShelvingUnit } from '../shelving-unit/shelving-unit.model';
import { ShelvingUnitService } from '../shelving-unit/shelving-unit.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { EditQuantityPopupService } from './edit-quantity-popup.service';
import { shelvesContainsProducts } from './shelvesContainsProducts.model';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
    selector: 'jhi-edit-Quantite',
    templateUrl: './editQuantite.component.html'
})
export class EditQuantityComponent implements OnInit, OnDestroy{
    Id:number;
    alerts = [];
    shelvesContainsProducts:shelvesContainsProducts;
    maxQuantity:number;
    constructor(private modalService: NgbModal,
        private alertService: JhiAlertService,
        private inventoryService: InventoryService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager,
        private shelvingUnitService: ShelvingUnitService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.maxQuantity = this.shelvesContainsProducts.qt;
        //this.inventoryService.query(7)
    }

    ngOnDestroy() {
         
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }

    addErrorAlert(message, key?, data?) {
        key = (key && key !== null) ? key : message;
        this.alerts.push(
            this.alertService.addAlert(
                {
                    type: 'danger',
                    msg: key,
                    params: data,
                    timeout: 5000,
                    toast: this.alertService.isToast(),
                    scoped: true
                },
                this.alerts
            )
        );
    }

    confirmEdit() {
        /*this.inventoryService.changeQuantity(this.shelvesContainsProducts).subscribe(
            (value) => { 
                console.log(value);
            },
            (error) => {
                console.log(error);
            }
          );
        this.clear()*/
    }
}

@Component({
    selector: 'jhi-editQuantity-popup',
    template: ''
})
export class EditQuantityPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private editQuantityPopupService: EditQuantityPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.editQuantityPopupService
                .open(EditQuantityComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }

    
}
