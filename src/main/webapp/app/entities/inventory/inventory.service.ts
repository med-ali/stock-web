import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { createRequestOption } from '../../shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { JhiDateUtils } from 'ng-jhipster';
import { DeliveredEquipement } from '../delivered-equipement';
import { StockMovement } from '../stock-movement';

@Injectable()
export class InventoryService {

    private resourceUrl = SERVER_API_URL + 'api/inventory';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/inventory';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    executeInventory(shelfCode: string, shelvingUnitCode: string, headquarterCode: string,page:number,size:number): Observable<HttpResponse<any[]>> {
        return this.http.get<any[]>
            (`${this.resourceUrl}/executeInventory?shelfCode=${shelfCode}&shelvingUnitCode=${shelvingUnitCode}&headquarterCode=${headquarterCode}&size=${size}&page=${page}`, { observe: 'response' });
    }
    
    /*filterByNullQuantity(type: string): Observable<HttpResponse<any[]>> {
        return this.http.get<any[]>
            (`${this.resourceUrl}/filterByNullQuantity?type=${type}`, { observe: 'response' });
    }*/
    filterByNullQuantity(type: string,stockMovementsByShelf: any[]): Observable<HttpResponse<any[]>> {
        return this.http.post<any[]>(`${this.resourceUrl}/filterByNullQuantity?type=${type}`, stockMovementsByShelf, { observe: 'response' });
    }
    updateInventory(stockMovements: StockMovement[]): any {
        return this.http.post<any>
            (`${this.resourceUrl}/updateInventorySession`, stockMovements, { observe: 'response' });
    }

    saveAll(shelfCode: string, shelvingUnitCode: string, headquarterCode: string,stockMovements: StockMovement[]): Observable<HttpResponse<any[]>> {
        return this.http.post<any>(`${this.resourceUrl}/saveAll?shelfCode=${shelfCode}&shelvingUnitCode=${shelvingUnitCode}&headquarterCode=${headquarterCode}`,stockMovements, { observe: 'response' });
    
    }

    createAdvancedSearchOption = (req?: any): HttpParams => {
        let Params = new HttpParams();
        if (req) {
            // Begin assigning parameters
            if (req.page) {
                Params = Params.append('page', req.page)
            }
            if (req.size) {
                Params = Params.append('size', req.size)
            }
            if (req.query) {
                Params = Params.append('q', req.query)
            }
            if (req.filter) {
                Params = Params.append('filter', req.filter)
            }
            if (req.sort) {
                for (const s of req.sort) {
                    Params = Params.append('sort', s);
                }
            }
            if (req.status) {
                Params = Params.append('status', req.status)
            }
            if (req.fromMovement) {
                Params = Params.append('fromMov', req.fromMovement)
            }
            if (req.toMovement) {
                Params = Params.append('toMov', req.toMovement)
            }
            if (req.annulled) {
                Params = Params.append('annulled', req.annulled)
            }
            if (req.client) {
                Params = Params.append('client', req.client)
            }
        }
        return Params;
    };

    convertDate(stockMovement: StockMovement) {
        stockMovement.movementDate = this.dateUtils.toDate(stockMovement.movementDate);
    }
    generateInventory(shelfCode: string, shelvingUnitCode: string, headquarterCode: string,type:string,lang:string): Observable<any> {
        let url = `${this.resourceUrl}/executeInventory/print/?shelfCode=${shelfCode}&shelvingUnitCode=${shelvingUnitCode}&headquarterCode=${headquarterCode}&type=${type}&lang=${lang}`;
        return this.http.get<any>
            (url);
    }
    generateInventoryList(stockMovementsByShelf:any[],type:string): Observable<any> {
        return this.http.post<any>
            (`${this.resourceUrl}/executeInventory/print/list/?type=`+type, stockMovementsByShelf);
    }
   
}
