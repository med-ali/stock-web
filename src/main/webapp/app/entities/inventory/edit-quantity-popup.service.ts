import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { shelvesContainsProducts } from './shelvesContainsProducts.model';
import { ProductService } from '../product/product.service';

@Injectable()
export class EditQuantityPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private productService: ProductService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                /*this.headquarterService.find(id).subscribe((headquarter) => {
                    this.ngbModalRef = this.headquarterModalRef(component, headquarter);
                    resolve(this.ngbModalRef);
                });*/
                this.ngbModalRef = this.editQuantityModalRef(component);
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.editQuantityModalRef(component);
                    //this.ngbModalRef = this.headquarterModalRef(component, new Headquarter());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }
    
    editQuantityModalRef(component: Component): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        //modalRef.componentInstance.headquarter = headquarter;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
