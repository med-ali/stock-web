import { BaseEntity } from '../../shared';
import { ShelvingUnit } from '../shelving-unit/shelving-unit.model';
export class shelvesContainsProducts implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public descrciption?: string,
        public name?: string,
        public barCode?: string,
        public shelvingUnit?: ShelvingUnit,
        public product?: number,
        public qt?: number
    ) {
    }
}
