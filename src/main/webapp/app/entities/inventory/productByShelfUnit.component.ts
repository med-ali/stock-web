import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { EditQuantityComponent } from './editQuantite.component';
import { ShelvingUnit } from '../shelving-unit/shelving-unit.model';
import { ShelvingUnitService } from '../shelving-unit/shelving-unit.service';
import { InventoryService } from './inventory.service';
import { shelvesContainsProducts } from './shelvesContainsProducts.model';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { Product } from '../product/product.model';
import { ProductService } from '../product/product.service';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
    selector: 'jhi-product-By-shelving-unit',
    templateUrl: './productByShelfUnit.component.html'
})
export class ProductByShelfUnitComponent implements OnInit, OnDestroy {

    currentAccount: any;
    shelvingUnits: ShelvingUnit[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    shelfUnitId:number;
    product:Product;
    productCode:string;
    shelvesContainsProducts:shelvesContainsProducts[]
    showQtBtn:boolean;
    constructor(private productService: ProductService,private modalService: NgbModal,
        private inventoryService: InventoryService,
        private shelvingUnitService: ShelvingUnitService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
            this.shelfUnitId = this.activatedRoute.snapshot.params['id'] 
    }

    loadAll() {
        /*if (this.currentSearch) {
            this.shelvingUnitService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res) => {this.onSuccess(res.body, res.headers)
                console.log("hiiiiiiiiiiiii1"+res)
                },
                (res) => this.onError(res.body)
                );
            return;
        }*/
        /*this.inventoryService.query(this.shelfUnitId).subscribe(
            (res) => {this.onSuccess(res.body, res.headers)
                console.log("hiiiiiiiiiiiii2"+res)},
            (res) => this.onError(res.body)
        );*/
    }
    
    showInputQt(){
        this.showQtBtn = true;
    }
    changeQuantity(shelvesContainsProduct){ 
        //this.showQtBtn = false;
        //this.inventoryService.changeQuantity(shelvesContainsProduct)
    }
    openEdit(shelvesContainsProducts) {
        const modalRef = this.modalService.open(EditQuantityComponent, {windowClass:  'fadeStyle'});
        modalRef.componentInstance.shelvesContainsProducts = shelvesContainsProducts; 
      }
    getProduct(id) {
        this.productService.find(id).subscribe((product) => {
            this.product = product;
            this.productCode = product.code;
        });
    }

    previousState() {
        window.history.back();
    }
    
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/inventory/product/shelfunit/'+this.activatedRoute.snapshot.params['id']], {
            queryParams:
                {
                    page: this.page,
                    size: this.itemsPerPage,
                    search: this.currentSearch,
                    sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
                }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/inventory/product/shelfunit/'+this.activatedRoute.snapshot.params['id'], {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/inventory/product/shelfunit/'+this.activatedRoute.snapshot.params['id'], {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.showQtBtn = false;
        /*this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInShelvingUnits();*/
    }

    ngOnDestroy() {
        //this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ShelvingUnit) {
        return item.id;
    }
    registerChangeInShelvingUnits() {
        this.eventSubscriber = this.eventManager.subscribe('shelvingUnitListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        //this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.shelvesContainsProducts = data;
    }
    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
