import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService, JhiLanguageService } from 'ng-jhipster';

import { InventoryService } from './inventory.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, PrintService } from '../../shared';
import { NgxBarcodeComponent } from 'ngx-barcode';
import { Client } from '../client';
import swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { StockMovement } from '../stock-movement';
import { listLazyRoutes } from '@angular/compiler/src/aot/lazy_routes';
@Component({
    selector: 'jhi-inventory',
    templateUrl: './inventory.component.html'
})
export class InventoryComponent implements OnInit, OnDestroy {

    stockMovementsByShelf: any[];
    stockMovementsByShelfNull: any[];
    stockMovementsByShelfMain: any[];
    selectedRow: any;
    showDetail : boolean[];
    update = false;
    today = new Date();
    nullqt: string;

    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    criteria: any;
    previousPage: any;
    reverse: any;
    /*
    ** Inventaire **
    */
    execute: boolean;
    isLoading = false;
    curPage: number;

    shelfModal = false;
    shelvingUnitModal = false;
    headquarterModal = false;
    selectedShelf = null;
    selectedShelvingUnit = null;
    selectedHeadquarter = null;
    shelfCode = null;
    shelvingUnitCode = null;
    headquarterCode = null;
    oldSelectedRow: any;
    display = 'all';
    stockMovementList:StockMovement[];
    list:any[];
    codeList:any[];
    numbers:any[];
    mod:number;
    tab:any[];
    new_array:any[];
    itemPerPageBreak:number;
    priceType:string;
    isMoyPrice:boolean;
    currentPage:number;
    pageSize:number;
    selectedShelfId:any;
    selectedShelvingUnitId:any;
    saved:boolean;
    @ViewChild('inventoryReport') inventoryReport: ElementRef;

    constructor(
        private languageService: JhiLanguageService,
        private parseLinks: JhiParseLinks,
        private inventoryService: InventoryService,
        private jhiAlertService: JhiAlertService,
        private printService: PrintService,
        private translateService: TranslateService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = true;
            this.predicate = 'description';
            this.criteria = this.sort();
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    ngOnInit() {
        this.selectedShelfId = null;
        this.selectedShelvingUnitId = null;
        this.pageSize = 10;
        this.currentPage = 0
        this.itemPerPageBreak = 6;
        this.nullqt = 'all';
        this.stockMovementsByShelf = [];
        this.stockMovementsByShelfMain = []
        this.stockMovementsByShelfNull = []
        this.stockMovementList = [];
        this.showDetail = []
        this.isMoyPrice = true;
        this.saved = false;
        // this.selectedHeadquarter = { id: 5, code: '001', name: 'Laboratorio Bergamo', description: 'Laboratorio Bergamo Via Borgo Palazzo - Bergamo', isActive: true };
        // this.selectedShelvingUnit = { id: 12, code: 'BG002', name: 'Acc. Campioni', description: 'Acc. Campioni (Bg)', isActive: true };
        // this.selectedShelf = { id: 12, code: 'BG002', name: 'Acc. Campioni', description: 'Acc. Campioni (Bg)', isActive: true };
    }
    /*
    ** Inventaire **
    */

    confirmExecute() {
        this.execute = true;
    }

    executeInventory() {
        swal({
            title: 'Are you sure?',
            text: 'This operation may take a while !',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: 'No, cancel it!'
        }).then((resultEx) => {
            if (resultEx.value) {
                console.log('here we go again')
                this.execute = false;
                this.isLoading = true;
                if (this.confirmExecute) {
                    this.stockMovementList = [];
                    this.tab = [];
                    this.curPage = 0;
                    this.shelfCode = this.selectedShelf != null && this.selectedShelf.id != null ? this.selectedShelf.id : null;
                    this.shelvingUnitCode = this.selectedShelvingUnit != null && this.selectedShelvingUnit.id != null ? this.selectedShelvingUnit.id : null;
                    this.headquarterCode = this.selectedHeadquarter != null && this.selectedHeadquarter.id != null ? this.selectedHeadquarter.id : null;
                    this.inventoryService.executeInventory(this.shelfCode, this.shelvingUnitCode, this.headquarterCode,0,this.pageSize).subscribe((res) => {                      
                        this.stockMovementsByShelf = res.body;
                        this.selectedRow = this.stockMovementsByShelf[0];
                        this.stockMovementsByShelfMain = this.stockMovementsByShelf
                        //this.totalItems = this.stockMovementsByShelf.length;
                        //this.queryCount = this.totalItems;
                        this.currentPage = 0;
                        this.page = 0;
                        this.isLoading = false;
                        this.totalItems = res.headers.get('X-Total-Count');
                        this.queryCount = this.totalItems;
                        this.mod = this.totalItems % this.itemPerPageBreak;
                        let div = (this.totalItems-this.mod)/this.itemPerPageBreak;
                        this.numbers = Array((this.totalItems-this.mod)/this.itemPerPageBreak+1).fill(0).map((x,i)=>i);
                        let i = 0;
                        this.numbers.forEach(element => {
                            this.new_array = this.stockMovementsByShelf.slice(this.itemPerPageBreak*i, this.itemPerPageBreak+this.itemPerPageBreak*i);                 
                            this.tab.push(this.new_array)
                            i++;
                        });
                    });
                }
            }
        }).catch(swal.noop);

    }

    /*filterByNullQuantity() {
        this.inventoryService.filterByNullQuantity(this.nullqt).subscribe((res) => {
            if (res.body) {
                this.stockMovementsByShelf = res.body;
            } else {
                this.stockMovementsByShelf = [];
            }
            this.page = 0;
        });
    }*/
loadPage(page: number) {
    console.log("saved : " + this.saved)
    console.log("this.previousPage : " + this.previousPage)
    console.log("page : " + page)
    if (page !== this.previousPage && !this.saved) {
        this.currentPage = page;
        this.previousPage = page;
        //this.transition();
        console.log("here load of shit")
        this.inventoryService.executeInventory(this.shelfCode, this.shelvingUnitCode, this.headquarterCode,this.page-1,this.pageSize).subscribe((res) => {                      
            this.stockMovementsByShelf = res.body;
            this.selectedRow = this.stockMovementsByShelf[0];
            this.stockMovementsByShelfMain = this.stockMovementsByShelf
            //this.totalItems = this.stockMovementsByShelf.length;
            //this.queryCount = this.totalItems;
            this.isLoading = false;
            this.totalItems = res.headers.get('X-Total-Count');
            this.queryCount = this.totalItems;
            this.mod = this.totalItems % this.itemPerPageBreak;
            let div = (this.totalItems-this.mod)/this.itemPerPageBreak;
            this.numbers = Array((this.totalItems-this.mod)/this.itemPerPageBreak+1).fill(0).map((x,i)=>i);
            let i = 0;
            this.numbers.forEach(element => {
                this.new_array = this.stockMovementsByShelf.slice(this.itemPerPageBreak*i, this.itemPerPageBreak+this.itemPerPageBreak*i);
                
                this.tab.push(this.new_array)
                i++;
            });
        });
        //this.transition();
    }
    this.saved = false;
    this.previousPage = page;
}

filterByNullQuantity() {
    this.stockMovementsByShelfMain = []
    this.showDetail = [];
    this.inventoryService.filterByNullQuantity(this.nullqt,this.stockMovementsByShelf).subscribe((res) => {
        if (this.nullqt=='all'){
           
            if (res.body) {
                this.stockMovementsByShelf = res.body;
            } else {
                this.stockMovementsByShelf = [];
            }
            this.stockMovementsByShelfMain = this.stockMovementsByShelf
            this.page = 0;      
        }
        else{
             
            if (res.body) {
                this.stockMovementsByShelfNull = res.body;
            } else {
                this.stockMovementsByShelfNull = [];
            }
            this.stockMovementsByShelfMain = this.stockMovementsByShelfNull
            this.page = 0;  
        }
    });
    //this.numbers = Array(this.stockMovementsByShelfMain.length).fill(0).map((x,i)=>i);
}
    /*sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }*/

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        if(this.reverse == true){
            this.sortInvDesc(this.predicate)
        }
        else{
            this.sortInvAsc(this.predicate)  
        }
        return result;
    }

    sortInvDesc(predicate:string) {
        predicate = predicate.replace("it.","");
        if(this.stockMovementsByShelfMain && this.stockMovementsByShelfMain.length > 0){
        this.stockMovementsByShelfMain.sort(function(a, b) {
            if(predicate === "shelf.id"){
                var nameA = a["shelf"].id ; // ignore upper and lowercase
                var nameB = b["shelf"].id ; // ignore upper and lowercase
            }
            else if(predicate === "shelf.shelvingUnit.id"){
                var nameA = a["shelf"].shelvingUnit.id ; // ignore upper and lowercase
                var nameB = b["shelf"].shelvingUnit.id ; // ignore upper and lowercase
            }
            else if(predicate === "shelf.shelvingUnit.headquarter.id"){
                var nameA = a["shelf"].shelvingUnit.headquarter.id ; // ignore upper and lowercase
                var nameB = b["shelf"].shelvingUnit.headquarter.id ; // ignore upper and lowercase
            }
            else if(predicate === "shelf.shelvingUnit.headquarter.company.id"){
                var nameA = a["shelf"].shelvingUnit.headquarter.company.id ; // ignore upper and lowercase
                var nameB = b["shelf"].shelvingUnit.headquarter.company.id ; // ignore upper and lowercase
            }
            else if (predicate == "presume" || predicate == "effective" || predicate == "lastPrice" ||predicate == "totalLastPrice"){
                var nameA = a[predicate]; // ignore upper and lowercase
                var nameB = b[predicate]; // ignore upper and lowercase
            }
            else if (a[predicate] != null) {
                var nameA = a[predicate].toUpperCase(); // ignore upper and lowercase
                var nameB = b[predicate].toUpperCase(); // ignore upper and lowercase
            }
            if (nameA < nameB) {
              return 1;
            }
            if (nameA > nameB) {
              return -1;
            }
          
            // names must be equal
            return 0;
          });
        }
    }

    sortInvAsc(predicate:string) {
        predicate = predicate.replace("it.","");
        if(this.stockMovementsByShelfMain && this.stockMovementsByShelfMain.length > 0){
        this.stockMovementsByShelfMain.sort(function(a, b) {
            if(predicate === "shelf.id"){
                var nameA = a["shelf"].id ; // ignore upper and lowercase
                var nameB = b["shelf"].id ; // ignore upper and lowercase
            }
            else if(predicate === "shelf.shelvingUnit.id"){
                var nameA = a["shelf"].shelvingUnit.id ; // ignore upper and lowercase
                var nameB = b["shelf"].shelvingUnit.id ; // ignore upper and lowercase
            }
            else if(predicate === "shelf.shelvingUnit.headquarter.id"){
                var nameA = a["shelf"].shelvingUnit.headquarter.id ; // ignore upper and lowercase
                var nameB = b["shelf"].shelvingUnit.headquarter.id ; // ignore upper and lowercase
            }
            else if(predicate === "shelf.shelvingUnit.headquarter.company.id"){
                var nameA = a["shelf"].shelvingUnit.headquarter.company.id ; // ignore upper and lowercase
                var nameB = b["shelf"].shelvingUnit.headquarter.company.id ; // ignore upper and lowercase
            }
            else if (predicate == "presume" || predicate == "effective" || predicate == "lastPrice" ||predicate == "totalLastPrice"){
                var nameA = a[predicate]; // ignore upper and lowercase
                var nameB = b[predicate]; // ignore upper and lowercase
            }
            else if (a[predicate] != null) {
                var nameA = a[predicate].toUpperCase(); // ignore upper and lowercase
                var nameB = b[predicate].toUpperCase(); // ignore upper and lowercase
            }
            if (nameA < nameB) {
              return -1;
            }
            if (nameA > nameB) {
              return 1;
            }
          
            // names must be equal
            return 0;
          });
        }
    }
    transition() {

    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
    }
    /*loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }*/
    displayNestedList(item,i) {
        let index = this.stockMovementsByShelfMain.indexOf(this.oldSelectedRow);
        if (index == i){
            this.showDetail[i] = !this.showDetail[i];
        }
        else{
            this.showDetail[index] = false;
            this.oldSelectedRow = this.selectedRow;
            this.selectedRow = item;
            if (this.oldSelectedRow.id === this.selectedRow.id) {
                this.showDetail[i] = !this.showDetail[i];
                //this.falseFct(i);
            } else {
                this.showDetail[i] = true;
                //this.falseFct(i);
            }
        }    
    }

    falseFct(j){
        for(let k =0;k<0;k++){
          if(k!=j){
            this.showDetail[k] = false;
          }
        }
    }
    updateInventory(stockMovement:StockMovement,stockMovementList:StockMovement[]) {
        
        //this.list.push(this.stockMovementList)
        stockMovementList.forEach(element => {
            if (element.product['id']===stockMovement.product['id']&&element.shelf['id']===stockMovement.shelf['id'])
            { 
                let index = this.stockMovementList.indexOf(element);
                this.stockMovementList.splice(index,1);
            }
            
        });
        stockMovementList.push(stockMovement)
        this.inventoryService.updateInventory(stockMovementList).subscribe((data) => {
            this.update = false;
            this.stockMovementList = data["body"]
        });
    }
    getLastInventories() {

    }
    confirmSaveAll() {
        swal({
            title: 'Are you sure?',
            text: 'This operation may take a while !',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: 'No, cancel it!'
        }).then((result) => {
            if (result.value) {
                this.saveAllInventories();
            }
        }).catch(swal.noop);
    }
    saveAllInventories() {
        this.isLoading = true;
        this.inventoryService.saveAll(this.shelfCode, this.shelvingUnitCode, this.headquarterCode,this.stockMovementList).subscribe((data) => {
            //this.stockMovementsByShelf = 
            console.log('here saveAllInventories')
            //this.numbers = Array(this.stockMovementsByShelf.length).fill(0).map((x,i)=>i);
            /*this.stockMovementsByShelfMain.forEach(element1 => {
                this.stockMovementsByShelf.forEach(element2 => {
                    if (element2["itemCode"] == element1["itemCode"]){
                        let index = this.stockMovementsByShelfMain.indexOf(element1);
                        this.stockMovementsByShelfMain.splice(index,1);
                        this.stockMovementsByShelfMain.push(element2);
                    }
                });
            });*/
            this.stockMovementsByShelfMain = data.body;
            this.currentPage = 0;
            this.page = 0;
            this.isLoading = false;
            this.showDetail = []
            this.saved = true;
        }, (error) => {
            this.jhiAlertService.error(error.body.message, null, null);
        });
        this.stockMovementList = [];
    }
    filterPrice(){
        if (this.priceType == "last"){
          this.isMoyPrice = false;
        }
        else if (this.priceType == "moy"){
            this.isMoyPrice = true;
        }
    }
    base64toBlob(byteString) {
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
          ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ia], { type: "octet/stream" });
      }
      saveData = (function () {
        var a = document.createElement("a");
        document.body.appendChild(a);
        // a.style = "display: none";
        return function (data, fileName) {
          var json = atob(data),
            blob = this.base64toBlob(json),
            url = window.URL.createObjectURL(blob);
          a.href = url;
          a.download = fileName;
          a.click();
          window.URL.revokeObjectURL(url);
        };
      }());
    printLastInventoriesReport(type:string) {
        /*setTimeout(() => {
            this.printService.printWholePaper(this.inventoryReport.nativeElement.innerHTML);
        }, 0);*/
        if(this.selectedShelf != null){
            this.selectedShelfId = this.selectedShelf.id.toString();
        }
        if (this.selectedShelvingUnit != null){
            this.selectedShelvingUnitId = this.selectedShelvingUnit.id.toString();
        }
        if(type === "pdf"){
            //.generateInventory(this.stockMovementsByShelfMain,type)
            //(this.selectedShelfId.toString(),this.selectedShelvingUnitId.toString(),this.selectedHeadquarter.id.toString(),type)
         this.inventoryService.generateInventory(this.selectedShelfId,this.selectedShelvingUnitId,
            this.selectedHeadquarter.id.toString(),type,this.languageService.currentLang).subscribe((value) => {
            //this.product = product;
            const linkSource = 'data:application/pdf;base64,' + value["res"];
            const downloadLink = document.createElement("a");
            const fileName = "inventory.pdf";
            downloadLink.href = linkSource;
            downloadLink.download = fileName;
            //downloadLink.click();
            if (downloadLink.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                //downloadLink.setAttribute("href", window.URL.createObjectURL(linkSource));
                downloadLink.setAttribute("download", fileName);
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
            } else {
                console.log('Pdf export only works in Chrome, Firefox, and Opera.');
                var fileBlob = new Blob([value['res']], {type: 'application/pdf'});
                window.navigator.msSaveBlob(fileBlob, fileName); 
            }
        });
    }
    else{
        //(this.selectedShelf.id,this.selectedShelf.shelvingUnit.id,this.selectedShelf.shelvingUnit.headquarter.id,type)
        this.inventoryService.generateInventory(this.selectedShelfId,this.selectedShelvingUnitId,
            this.selectedHeadquarter.id.toString(),type,this.languageService.currentLang).subscribe((value) => {
            this.saveData(value['res'],"inventory.docx"); 
        });
    }
    }

    ngOnDestroy() {
    }

    openBasicSwal(entity) {
        const title = this.translateService.instant('entity.' + entity + '.swal.title');
        const text = this.translateService.instant('entity.' + entity + '.swal.text');
        swal({
            'title': title,
            'text': text,
            'type': 'error'

        }).catch(swal.noop);
    }

    openMyModal(event) {
        if (event === 'headquarterModal') {
            this.headquarterModal = true;
        } else if (event === 'shelvingUnitModal') {
            this.shelvingUnitModal = true;
        } else if (event === 'shelfModal') {
            this.shelfModal = true;
        }
        document.querySelector('#' + event).classList.add('md-show');
    }

    closeModal(event) {
        this.headquarterModal = this.shelvingUnitModal = this.shelfModal = false;
        (document.querySelector('#' + event).parentElement.parentElement).classList.remove('md-show');
    }

    assignHeadquarter(headquarter) {
        this.selectedHeadquarter = headquarter;
        this.closeModal('closeHeadquarterBtn');
    }

    assignShelvingUnit(shelvingUnit) {
        this.selectedShelvingUnit = shelvingUnit;
        this.closeModal('closeShelvingUnitBtn');
    }

    assignShelf(shelf) {
        this.selectedShelf = shelf;
        this.closeModal('closeShelfBtn');
    }
    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
