import {
    InventoryService,
    InventoryComponent,
    InventoryResolvePagingParams
} from './';

export const inventory_declarations = [
    InventoryComponent,
];

export const inventory_entryComponents = [
    InventoryComponent,
];

export const inventory_providers = [
    InventoryService,
    InventoryResolvePagingParams,
];
