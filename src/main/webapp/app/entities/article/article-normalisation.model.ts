export class ArticleNormalisation  {
    constructor(
        public id?: number,
        public code?: string,
        public normalisationUnit?: number,
        public conversionUnit?: number,
        public isActive?: boolean,
        public article?: any,
        public product?: any,
    ) {
        this.isActive = true;
        this.normalisationUnit = 1;
        this.conversionUnit = 0
    }
}
