import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Article } from './article.model';
import { ArticleService } from './article.service';

@Injectable()
export class ArticlePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private articleService: ArticleService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any, norm?: boolean): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.articleService.find(id).subscribe((article) => {
                    if (!norm) {
                        article.expirationDate = this.datePipe
                            .transform(article.expirationDate, 'yyyy-MM-ddTHH:mm:ss');
                        article.startValidation = this.datePipe
                            .transform(article.startValidation, 'yyyy-MM-ddTHH:mm:ss');
                        article.endValidation = this.datePipe
                            .transform(article.endValidation, 'yyyy-MM-ddTHH:mm:ss');
                        article.invoiceDate = this.datePipe
                            .transform(article.invoiceDate, 'yyyy-MM-ddTHH:mm:ss');
                        article.lastInvoiceDate = this.datePipe
                            .transform(article.lastInvoiceDate, 'yyyy-MM-ddTHH:mm:ss');
                    }
                    this.ngbModalRef = this.articleModalRef(component, article);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const article: Article = new Article();
                    article.startValidation = this.datePipe
                        .transform(new Date(), 'yyyy-MM-ddTHH:mm:ss');
                    article.endValidation = this.datePipe
                        .transform('2028-12-31T23:59:59+01:00', 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.articleModalRef(component, article);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    articleModalRef(component: Component, article: Article): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.article = article;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
