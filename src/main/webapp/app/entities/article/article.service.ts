import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Article } from './article.model';
import { createRequestOption } from '../../shared';
import { ArticleNormalisation } from './article-normalisation.model';
import { HttpClient, HttpResponse, HttpParams , HttpRequest, HttpEvent} from '@angular/common/http';
import { JhiDateUtils } from 'ng-jhipster';

@Injectable()
export class ArticleService {

    private resourceUrl = SERVER_API_URL + 'api/articles';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/articles';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(article: Article): Observable<Article> {
        this.convertDate(article);
        return this.http.post<Article>(this.resourceUrl, article);
    }

    update(article: Article): Observable<Article> {
        this.convertDate(article);
        return this.http.put<Article>(this.resourceUrl, article);
    }
    uploadFile(file: File):  Observable<HttpEvent<{}>> {
		const formdata: FormData = new FormData();
		formdata.append('file', file);
		/*const req = new HttpRequest('post', this.resourceUrl+ '/uploaded', formdata, {
			  reportProgress: true,
			  responseType: 'text'
        });*/
        console.log(formdata)
        return this.http.post<any>(this.resourceUrl+ '/uploaded', formdata);
		//return this.http.request(req);
    }
    
    find(id: number): Observable<Article> {
        return this.http.get<Article>(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<HttpResponse<Article[]>> {
        const options = createRequestOption(req);
        return this.http.get<Article[]>(this.resourceUrl, { params: options, observe: 'response' });
    }
    queryAssignedToProduct(id: number, req?: any): Observable<HttpResponse<Article[]>> {
        const options = createRequestOption(req);
        return this.http.get<Article[]>(`${this.resourceUrl}/product/${id}`, { params: options, observe: 'response' });
    }
    queryGetArticlesNotNormalised(req?: any): Observable<HttpResponse<Article[]>> {
        const options = createRequestOption(req);
        return this.http.get<Article[]>(`${this.resourceUrl}/notnormalised/`, { params: options, observe: 'response' });
    }
    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<HttpResponse<Article[]>> {
        const options = this.createAdvancedSearchOption(req);
        return this.http.get<Article[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }

    createAdvancedSearchOption = (req?: any): HttpParams => {
        let Params = new HttpParams();
        if (req) {
            // Begin assigning parameters
            if (req.page) {
                Params = Params.append('page', req.page)
            }
            if (req.size) {
                Params = Params.append('size', req.size)
            }
            if (req.query) {
                Params = Params.append('q', req.query)
            }
            if (req.filter) {
                Params = Params.append('filter', req.filter)
            }
            if (req.sort) {
                for (const s of req.sort) {
                    Params = Params.append('sort', s);
                }
            }
            if (req.supplier) {
                Params = Params.append('supplier', req.supplier)
            }
            if (req.qty) {
                Params = Params.append('qty', req.qty)
            }

        }
        return Params;
    };

    findNormByArticle(id: number): Observable<ArticleNormalisation> {
        return this.http.get(`${this.resourceUrl}/${id}/norm`)
    }

    createNorm(articleNormalisation: ArticleNormalisation): Observable<ArticleNormalisation> {
        return this.http.post<ArticleNormalisation>(`${this.resourceUrl}/norm`, articleNormalisation);
    }

    createNormInventory(articleNormalisation: ArticleNormalisation,idShelf:number,qty: number): Observable<ArticleNormalisation> {
        console.log(articleNormalisation)
        return this.http.post<ArticleNormalisation>(`${this.resourceUrl}/norm/inventory/`+idShelf+"/?qty="+qty, articleNormalisation);
    }
    
    updateNorm(articleNormalisation: ArticleNormalisation): Observable<ArticleNormalisation> {
        return this.http.put<ArticleNormalisation>(`${this.resourceUrl}/norm`, articleNormalisation);
    }

    deleteNorm(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/norm/${id}`, { observe: 'response' });
    }
    checkCode(code: string): Observable<Article> {
        return this.http.get<Article>(this.resourceUrl+"/checkcode/?code="+code);
    }
    convertDate(article: Article) {
        article.expirationDate = this.dateUtils.toDate(article.expirationDate);
        article.startValidation = this.dateUtils.toDate(article.startValidation);
        article.endValidation = this.dateUtils.toDate(article.endValidation);
        article.invoiceDate = this.dateUtils.toDate(article.invoiceDate);
        article.lastInvoiceDate = this.dateUtils.toDate(article.lastInvoiceDate);
    }
}
