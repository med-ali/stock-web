import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiParseLinks } from 'ng-jhipster';

import { ArticleNormalisation } from './article-normalisation.model';
import { Article, ArticleService, ArticlePopupService } from '../article';
import { Product, ProductService } from '../product';
import { ResponseWrapper, ITEMS_PER_FILTER_PAGE, PagerService } from '../../shared';
import swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'jhi-article-norm',
    templateUrl: './article-norm.component.html'
})
export class ArticleNormComponent implements OnInit {

    article: Article;
    articleNormalisation: ArticleNormalisation;
    isSaving: boolean;

    products: Product[];
    productSearch: string;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    links: any;
    reverse: any;
    pager: any = {};

    productModal: boolean;
    hasSameType: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private articleService: ArticleService,
        private productService: ProductService,
        private eventManager: JhiEventManager,
        private route: ActivatedRoute,
        private parseLinks: JhiParseLinks,
        private translateService: TranslateService
    ) {
        this.itemsPerPage = ITEMS_PER_FILTER_PAGE;
        this.page = 1;
        this.previousPage = 1;
        this.reverse = true;
        this.predicate = 'description';
    }

    ngOnInit() {
        this.isSaving = false;
        this.productModal = false;
        this.articleNormalisation = {};
        if (this.article.normalised) {
            this.load(this.article.id);
        }
    }

    load(id) {
        this.articleService.findNormByArticle(id).subscribe((articleNormalisation) => {
            this.articleNormalisation = articleNormalisation;
            if (this.articleNormalisation.article.measureUnit.type !== this.articleNormalisation.product.measureUnit.type) {
                this.hasSameType = true;
            }
        });
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.articleNormalisation.id !== undefined) {
            this.subscribeToSaveResponse(
                this.articleService.updateNorm(this.articleNormalisation));
        } else {
            this.articleNormalisation.article = this.article;
            this.articleNormalisation.isActive = true;
            //this.articleNormalisation.conversionUnit = 0;
            this.subscribeToSaveResponse(
                this.articleService.createNorm(this.articleNormalisation));
        }
    }

    confirmDelete(id: number) {
        this.articleService.deleteNorm(id).subscribe((response) => {
            this.eventManager.broadcast({ name: 'articleListModification', content: 'Deleted an article' });
            this.activeModal.dismiss(response);
        });
    }

    private subscribeToSaveResponse(result: Observable<ArticleNormalisation>) {
        result.subscribe((res: ArticleNormalisation) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: ArticleNormalisation) {
        this.eventManager.broadcast({ name: 'articleListModification', content: 'OK' });
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.activeModal.close();
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    openConfirmsSwal() {
        const title = this.translateService.instant('action.delete.swal.title');
        const text = this.translateService.instant('action.delete.swal.text');
        const btnconfirm = this.translateService.instant('action.delete.swal.btnconfirm');
        const btnclose = this.translateService.instant('action.delete.swal.btnclose');
        const msg = this.translateService.instant('action.delete.swal.msg');
        const success = this.translateService.instant('action.delete.swal.success');
        swal({
            'title': title,
            'text': text,
            'type': 'warning',
            'showCancelButton': true,
            'confirmButtonColor': '#3085d6',
            'cancelButtonColor': '#d33',
            'cancelButtonText': btnclose,
            'confirmButtonText': btnconfirm
        }).then((result) => {
            if (result.value) {
            this.confirmDelete(this.articleNormalisation.id);
            swal(
                msg,
                success,
                'success');
            }
        }).catch(swal.noop);
    }

    openMyModal(event) {
        if (event === 'productModal') {
            this.productModal = true;
            this.loadProducts();
        }
        document.querySelector('#' + event).classList.add('md-show');
    }

    loadProducts() {
        const params: any = {
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }
        if (this.productSearch) {
            params['query'] = this.productSearch;
        }
        this.productService.listProductsNotAssociatedBySupplier(this.article.supplier.id, params)
            .subscribe(
                (res) => this.onSuccess(res.body, res.headers),
                (res) => this.onError(res.body)
            );
    }

    private onSuccess(data, headers) {
        //this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.products = data;
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadProducts();
        }
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    closeModal(event) {
        this.resetParameters();
        this.productModal = false;
        (document.querySelector('#' + event).parentElement.parentElement).classList.remove('md-show');
    }

    resetParameters() {
        this.page = 1;
        this.predicate = 'description';
        this.reverse = true;
        this.productSearch = '';
    }

    closeMyModalEvent(event) {
        ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
    }

    assignProduct(product) {
        this.articleNormalisation.product = product;
        this.articleNormalisation.article = this.article;
        if (this.articleNormalisation.article.measureUnit.type !== product.measureUnit.type) {
            this.hasSameType = true;
        } else { 
            this.hasSameType = false;
            this.articleNormalisation.conversionUnit = 0;
        }
        this.closeModal('closeProductBtn');
    }
}

@Component({
    selector: 'jhi-article-norm-popup',
    template: ''
})
export class ArticleNormPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private articlePopupService: ArticlePopupService) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.articlePopupService
                .open(ArticleNormComponent as Component, params['id'], true);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
