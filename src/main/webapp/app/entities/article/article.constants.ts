import {
    ArticleService,
    ArticlePopupService,
    ArticleComponent,
    ArticleAssignedComponent,
    ArticleDetailComponent,
    ArticleDialogComponent,
    ArticlePopupComponent,
    ArticleDeletePopupComponent,
    ArticleDeleteDialogComponent,
    articleRoute,
    articlePopupRoute,
    ArticleResolvePagingParams,
    ArticleNormComponent,
    ArticleNormPopupComponent,
    ArticleAssignedProductComponent
} from './';

export const article_declarations = [
    ArticleComponent,
    ArticleAssignedComponent,
    ArticleNormComponent,
    ArticleDetailComponent,
    ArticleDialogComponent,
    ArticleDeleteDialogComponent,
    ArticlePopupComponent,
    ArticleDeletePopupComponent,
    ArticleNormPopupComponent,
    ArticleAssignedProductComponent
];

export const article_entryComponents = [
    ArticleComponent,
    ArticleAssignedComponent,
    ArticleNormComponent,
    ArticleDialogComponent,
    ArticlePopupComponent,
    ArticleDeleteDialogComponent,
    ArticleDeletePopupComponent,
    ArticleNormPopupComponent,
    ArticleAssignedProductComponent
];

export const article_providers = [
    ArticleService,
    ArticlePopupService,
    ArticleResolvePagingParams,
];
