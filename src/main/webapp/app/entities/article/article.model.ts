import { BaseEntity } from './../../shared';

export class Article implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public description?: string,
        public minPackagingOrder?: number,
        public discount?: number,
        public quantityPerPackage?: number,
        public unitPrice?: number,
        public isExpired?: boolean,
        public numberOfDaysToExpired?: number,
        public expirationDate?: any,
        public startValidation?: any,
        public endValidation?: any,
        public isNew?: boolean,
        public annualConsumption?: number,
        public numberOfDelivery?: number,
        public note?: string,
        public invoiceDate?: any,
        public lastInvoiceDate?: any,
        public billingFrequency?: number,
        public annualNetCost?: number,
        public annualCostWithTVA?: number,
        public normalised?: boolean,
        public packagingMethod?: any,
        public measureUnit?: any,
        public supplier?: any,
    ) {
        this.isExpired = false;
        this.isNew = false;
        this.normalised = false;
        this.minPackagingOrder = 1;
        this.quantityPerPackage = 1;
        this.numberOfDaysToExpired = 0;
    }
}
