import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Article } from './article.model';
import { ArticlePopupService } from './article-popup.service';
import { ArticleService } from './article.service';
import { PackagingMethod, PackagingMethodService } from '../packaging-method';
import { MeasureUnit, MeasureUnitService } from '../measure-unit';
import { Supplier, SupplierService } from '../supplier';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-article-dialog',
    templateUrl: './article-dialog.component.html'
})
export class ArticleDialogComponent implements OnInit {

    article: Article;
    isSaving: boolean;
    expired: any;
    step = 1;
    supplierModal: boolean;
    packagingMethodModal: boolean;
    measureUnitModal: boolean;
    descriptionCampany:string;
    descriptionPackagingMethod:string;
    descriptionMeasureUnit:string;
    existCode:boolean;
    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private articleService: ArticleService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() { 
        this.expired = 'no';
        this.supplierModal = this.packagingMethodModal = this.measureUnitModal = false;   
        this.isSaving = false; 
        if(this.article.supplier){
            this.descriptionCampany = this.article.supplier.companyName + "-" + this.article.supplier.email;
            if(this.article.isExpired){
                this.expired = 'yes'; 
            }
        }
        
        if(this.article.packagingMethod){
            this.descriptionPackagingMethod = this.article.packagingMethod.code + "-" + this.article.packagingMethod.description;
        }
        if(this.article.measureUnit){
            this.descriptionMeasureUnit = this.article.measureUnit.code + "-" + this.article.measureUnit.description;
        }
    }
 
    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.articleService.checkCode(this.article.code).subscribe((value) => 
        {  
            if (value==true && this.article.id == undefined){
                this.existCode = true;
            }
            else{
                this.existCode = false;
                this.isSaving = true;
                if (this.article.id !== undefined) {
                    this.subscribeToSaveResponse(
                        this.articleService.update(this.article));
                } else { 
                    this.subscribeToSaveResponse(
                        this.articleService.create(this.article));
                }
                this.clear()
            }
        },
        (error) => {
        })
    }

    public expiredChange(event) {
        if (event === 'yes') {
            this.article.isExpired = true;
        } else if (event === 'no') {
            this.article.isExpired = false;
            this.article.numberOfDaysToExpired = 0;
        }
    }

    private subscribeToSaveResponse(result: Observable<Article>) {
        result.subscribe((res: Article) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Article) {
        this.eventManager.broadcast({ name: 'articleListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    openMyModal(event) {
        if (event === 'packagingMethodModal') {
            this.packagingMethodModal = true;
        } else if (event === 'measureUnitModal') {
            this.measureUnitModal = true;
        } else if (event === 'supplier2Modal') {
            this.supplierModal = true;
        }
        document.querySelector('#' + event).classList.add('md-show');
    }

    closeModal(event) {
        this.supplierModal = this.packagingMethodModal = this.measureUnitModal = false;
        (document.querySelector('#' + event).parentElement.parentElement).classList.remove('md-show');
    }

    closeMyModalEvent(event) {
        ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
    }

    assignSupplier(supplier) {
        this.article.supplier = supplier;
        this.descriptionCampany = this.article.supplier.companyName + "-" + this.article.supplier.email;
        this.closeModal('closeSupplier2Btn');
    }

    assignPackagingMethod(packagingMethod) {
        this.article.packagingMethod = packagingMethod;
        this.descriptionPackagingMethod = this.article.packagingMethod.code + "-" + this.article.packagingMethod.description;
        this.closeModal('closePackagingMethodBtn');
    }

    assignMeasureUnit(measureunit) {
        this.article.measureUnit = measureunit;
        this.descriptionMeasureUnit = this.article.measureUnit.code + "-" + this.article.measureUnit.description;
        this.closeModal('closeMeasureUnitBtn');
    }
    getColor() {  
        return "red";
    }
}

@Component({
    selector: 'jhi-article-popup',
    template: ''
})
export class ArticlePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private articlePopupService: ArticlePopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.articlePopupService
                    .open(ArticleDialogComponent as Component, params['id']);
            } else {
                this.articlePopupService
                    .open(ArticleDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
