import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { HttpClient, HttpResponse, HttpEventType } from '@angular/common/http';
import { Article } from './article.model';
import { ArticleService } from './article.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, BaseEntity } from '../../shared';
import { Supplier, SupplierService } from '../supplier';

@Component({
    selector: 'jhi-article',
    templateUrl: './article.component.html'
})
export class ArticleComponent implements OnInit, OnDestroy {

    currentAccount: any;
    articles: Article[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    selectedSupplier: Supplier;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    supplierModal: boolean;
    selectedFiles: FileList;
	currentFile: File;
    constructor(
        private articleService: ArticleService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = false;
            this.predicate = 'description';
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }
    selectFile(event) {
        this.selectedFiles = event.target.files;
    }
    upload() {
        this.currentFile = this.selectedFiles.item(0);
        this.articleService.uploadFile(this.currentFile).subscribe(event => {
         if (event instanceof HttpResponse) {
            console.log('File is completely uploaded!');
          }
        });
        this.selectedFiles = undefined;
    }
    ngOnInit() {
        this.supplierModal = false;
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInArticles();
    }

    loadAll() {
        if (this.currentSearch || this.selectedSupplier) {
            const params: any = {
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()
            };
            if (this.selectedSupplier) {
                params['supplier'] = this.selectedSupplier.id;
            }
            this.articleService.search(params).subscribe(
                (res) => this.onSuccess(res.body, res.headers),
                (res) => this.onError(res.body)
            );
            return;
        }
        this.articleService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res) => this.onSuccess(res.body, res.headers),
            (res) => this.onError(res.body)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/article'], {
            queryParams:
                {
                    page: this.page,
                    size: this.itemsPerPage,
                    search: this.currentSearch,
                    sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
                }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/article', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query && !this.selectedSupplier) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        const params: any = {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        };
        this.router.navigate(['/article', params]);
        this.loadAll();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Article) {
        return item.id;
    }
    registerChangeInArticles() {
        this.eventSubscriber = this.eventManager.subscribe('articleListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        //this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.articles = data;
    }
    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    openMyModal(event) {
        if (event === 'supplierModal') {
            this.supplierModal = true;
        }
        document.querySelector('#' + event).classList.add('md-show');
    }

    closeMyModal(event) {
        this.supplierModal = false;
        (document.querySelector('#' + event).parentElement.parentElement).classList.remove('md-show');
    }

    closeMyModalEvent(event) {
        ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
    }

    assignSupplier(supplier) {
        this.selectedSupplier = supplier;
        this.closeMyModal('closeSupplierBtn');
        this.search(this.currentSearch);
    }
}
