import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { SnipComponent } from './snip.component';
import { SnipPopupComponent } from './snip-dialog.component';
import { SnipDetailComponent } from './snip-detail.component';
import { SnipDeletePopupComponent } from './snip-delete-dialog.component';

@Injectable()
export class SnipResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const  snipRoute: Routes = [
    {
        path: 'snip',
        component: SnipComponent,
        resolve: {
            'pagingParams': SnipResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.snip.default'
        },
        canActivate: [UserRouteAccessService]
    }
    , {
        path: 'snip/:id',
        component: SnipDetailComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.slot.default'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const snipPopupRoute: Routes = [
    {
        path: 'snip-new',
        component: SnipPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.snip.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'snip/:id/edit',
        component: SnipPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.snip.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'snip/:id/delete',
        component: SnipDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.slot.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
