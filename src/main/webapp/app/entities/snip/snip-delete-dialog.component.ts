import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { Snip } from './snip.model';
import { SnipPopupService } from './snip-popup.service';
import { SnipService } from './snip.service';

@Component({
    selector: 'jhi-snip-delete-dialog',
    templateUrl: './snip-delete-dialog.component.html'
})
export class SnipDeleteDialogComponent {

    snip: Snip;
    alerts = [];
    constructor(private alertService: JhiAlertService,
        private snipService: SnipService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    addErrorAlert(message, key?, data?) {
        key = (key && key !== null) ? key : message;
        this.alerts.push(
            this.alertService.addAlert(
                {
                    type: 'danger',
                    msg: key,
                    params: data,
                    timeout: 5000,
                    toast: this.alertService.isToast(),
                    scoped: true
                },
                this.alerts
            )
        );
    }

    confirmDelete(id: number) {
        this.snipService.delete(id).subscribe((response) => {
            if (response["status"] == 200){
                this.eventManager.broadcast({
                    name: 'snipListModification',
                    content: 'Deleted an snip'
                });
                this.activeModal.dismiss(true);
            }
            else{
                this.addErrorAlert('Delete not reachable', 'error.entity.delete.reachable');
            } 
        });
    }
}

@Component({
    selector: 'jhi-snip-delete-popup',
    template: ''
})
export class SnipDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private snipPopupService: SnipPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.snipPopupService
                .open(SnipDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
