import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Snip } from './snip.model';
import { SnipPopupService } from './snip-popup.service';
import { SnipService } from './snip.service';
import { ShelvingUnit, ShelvingUnitService } from '../shelving-unit';
import { ResponseWrapper } from '../../shared';
import { Shelf } from '../shelf';
import { Drawer } from '../drawer';
import { Slot } from '../slot';

@Component({
    selector: 'jhi-snip-dialog',
    templateUrl: './snip-dialog.component.html'
})
export class SnipDialogComponent implements OnInit {

    snip: Snip;
    isSaving: boolean;
    active: any;
    descriptionDrawer:string; 
    drawerModal: boolean;
    existCode:boolean;
    descriptionSlot: any;
    descriptionShelf: string;
    descriptionShelvingUnit: string;
    drawer:Drawer;
    shelf:Shelf;
    shelvingUnit:ShelvingUnit;
    slotModal: boolean;
    shelfModal: boolean;
    shelvingUnitModal: boolean;
    slot: any;
    barCode: string;
    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private snipService: SnipService,
        private shelvingUnitService: ShelvingUnitService,
        private eventManager: JhiEventManager
    ) {
    }
     
    ngOnInit() {
        this.isSaving = false;
        this.active = 'yes';
        this.snip.isActive = true; 
        this.drawerModal = false;
        if (this.snip.barCode){
            this.barCode = this.snip.barCode;
        }
        if(this.snip.slot){
            this.slot = this.snip.slot;
            this.descriptionSlot = this.snip.slot.barCode;
            if(this.snip.slot.drawer){
                this.drawer =  this.snip.slot.drawer;
                this.descriptionDrawer = this.snip.slot.drawer.code + "-" + this.snip.slot.drawer.description;
            }
            
            if(this.snip.slot.drawer.shelf){
                this.shelf = this.snip.slot.drawer.shelf;
                this.descriptionShelf = this.snip.slot.drawer.shelf.code + "-" + this.snip.slot.drawer.shelf.description;
            }
            if(this.snip.slot.drawer.shelf.shelvingUnit){
                this.shelvingUnit = this.snip.slot.drawer.shelf.shelvingUnit;
                this.descriptionShelvingUnit = this.snip.slot.drawer.shelf.shelvingUnit.code + "-" + this.snip.slot.drawer.shelf.shelvingUnit.description;
            }
        }
        
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    public activeChange(event:String) {
        console.log(event)
        if (event === 'yes') {
            this.snip.slot.isActive = true;
        } else if (event === 'no') {
            this.snip.slot.isActive = false;
        }
        console.log('up' + this.snip.slot.isActive)
    }

    /*save() {
        this.snip = new Snip(this.snip.id,this.barCode,true,this.slot);
        this.isSaving = true;
        if (this.snip.id !== undefined) {
            this.subscribeToSaveResponse(
                this.snipService.update(this.snip));
        } else {
            this.snip = new Snip(null,)
            this.subscribeToSaveResponse(
                this.snipService.create(this.snip));
        }
        this.clear()
    
    }*/
    save() {
        this.snip = new Snip(this.snip.id,this.barCode,true,this.slot);
        this.snipService.checkCode(this.snip.barCode).subscribe((value) => { 
            console.log(value) 
            if (value==true && this.snip.id == undefined){
                this.existCode = true;
            }
            else{
                this.existCode = false;
                this.isSaving = true;
                if (this.snip.id !== undefined) {
                    this.subscribeToSaveResponse(
                        this.snipService.update(this.snip));
                } else {
                    this.subscribeToSaveResponse(
                        this.snipService.create(this.snip));
                }
                this.clear()
            }
            },
            (error) => { 
            console.log("error"+error)
        })
    }
    private subscribeToSaveResponse(result: Observable<Shelf>) {
        result.subscribe((res: Shelf) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Shelf) {
        this.eventManager.broadcast({ name: 'snipListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackShelvingUnitById(index: number, item: ShelvingUnit) {
        return item.id;
    }

    openMyModal(event) {
        if (event === 'drawerModal') {
            this.drawerModal = true;
        }
        else if (event === 'slotModal'){
            this.slotModal = true;
        }
        else if (event === 'shelfModal'){
            this.shelfModal = true;
        }
        else if (event === 'shelvingUnitModal'){
            this.shelvingUnitModal = true;
        }
        document.querySelector('#' + event).classList.add('md-show');
    }

    closeModal(event) {
        this.slotModal = this.drawerModal = this.shelfModal = this.shelvingUnitModal = false;
        (document.querySelector('#' + event).parentElement.parentElement).classList.remove('md-show');
    }

    closeMyModalEvent(event) {
        ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
    }

    assignDrawer(drawer) {
        console.log('kdkdkdkdkkdkdkkdkdkkdkdkmmmmmmdrawer')
        this.drawer = drawer;
        this.descriptionDrawer = drawer.code + "-" + drawer.description;
        console.log("descriptionShelf;"+this.shelf.code)
        this.closeModal('closeDrawerBtn');
    }
    assignSlot(slot) {
        console.log('kdkdkdkdkkdkdkkdkdkkdkdkmmmmmmslot')
        this.slot = slot;
        this.descriptionSlot = slot.barCode;
        this.closeModal('closeSlotBtn');
        console.log("descriptionShelf;"+this.descriptionShelf)
    }
    assignShelf(shelf) {
        console.log('kdkdkdkdkkdkdkkdkdkkdkdkmmmmmm')
        this.shelf = shelf;
        this.descriptionShelf = shelf.code + "-" + shelf.description;
        this.closeModal('closeShelfBtn');
    }
    assignShelvingUnit(su) {
        this.shelvingUnit = su;
        this.descriptionShelvingUnit = su.code + "-" + su.description;
        this.closeModal('closeShelvingUnitBtn');
    }
}

@Component({
    selector: 'jhi-snip-popup',
    template: ''
})
export class SnipPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private snipPopupService: SnipPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.snipPopupService
                    .open(SnipDialogComponent as Component, params['id']);
            } else {
                this.snipPopupService
                    .open(SnipDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
