export * from './snip.model';
export * from './snip-popup.service';
export * from './snip.service';
//export * from './slot-delete-dialog.component';
//export * from './slot-detail.component';
export * from './snip.component';
export * from './snip.route';
//export * from './auto-slot-dialog.component';