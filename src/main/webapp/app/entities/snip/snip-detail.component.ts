import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Snip } from './snip.model';
import { SnipService } from './snip.service';

@Component({
    selector: 'jhi-snip-detail',
    templateUrl: './snip-detail.component.html'
})
export class SnipDetailComponent implements OnInit, OnDestroy {

    snip: Snip;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private snipService: SnipService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSlots();
    }

    load(id) {
        this.snipService.find(id).subscribe((d) => {
            this.snip = d;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSlots() {
        this.eventSubscriber = this.eventManager.subscribe(
            'snipListModification',
            (response) => this.load(this.snip.id)
        );
    }
}
