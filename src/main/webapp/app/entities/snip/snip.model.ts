import { BaseEntity } from '../../shared';

export class Snip implements BaseEntity {
    constructor(
        public id?: number,
        public barCode?: string,
        public isActive?: boolean, 
        public slot?: any,
    ) {
        this.isActive = true; 
    }
}
