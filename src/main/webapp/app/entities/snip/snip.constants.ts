import {
    SnipService,
    SnipComponent,
    /*SlotPopupService,
    SlotComponent,
    SlotDetailComponent,
    SlotDialogComponent,
    SlotPopupComponent,
    SlotDeletePopupComponent,
    SlotDeleteDialogComponent,*/
    snipRoute,
    snipPopupRoute,
    SnipResolvePagingParams,
    SnipPopupService,
} from './';
import { SnipDialogComponent, SnipPopupComponent } from './snip-dialog.component';
import { SnipDetailComponent } from './snip-detail.component';
import { SnipDeletePopupComponent, SnipDeleteDialogComponent } from './snip-delete-dialog.component';

export const snip_declarations = [
    SnipDeletePopupComponent,
    SnipComponent,
    SnipPopupComponent,
    SnipDialogComponent,
    SnipDetailComponent,
    SnipDeleteDialogComponent
    /*SlotDetailComponent,
    SlotDialogComponent,
    SlotDeleteDialogComponent,
    SlotPopupComponent,
    
    SlotAutoPopupComponent,
    AutoSlotDialogComponent*/
];

export const snip_entryComponents = [
    SnipDeletePopupComponent,
    SnipComponent,
    SnipDialogComponent,
    SnipPopupComponent,
    SnipDeleteDialogComponent,
   /* 
    SlotPopupComponent,
    SlotDeleteDialogComponent,
    SlotDeletePopupComponent,
    SlotAutoPopupComponent,
    AutoSlotDialogComponent*/
];

export const snip_providers = [
    SnipService,
    SnipPopupService,
    SnipResolvePagingParams,
];
