import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Snip } from './snip.model';
import { createRequestOption } from '../../shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { Drawer } from '../drawer';

@Injectable()
export class SnipService {

    private resourceUrl = SERVER_API_URL + 'api/snips';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/snips';

    constructor(private http: HttpClient) { }

    create(d: Snip): Observable<Snip> {
        return this.http.post<Snip>(this.resourceUrl, d);
    }

    checkCode(code: string): Observable<Snip> {
        return this.http.get<Snip>(this.resourceUrl+"/checkcode/?code="+code);
    }

    update(d: Snip): Observable<Snip> {
        return this.http.put<Snip>(this.resourceUrl, d);
    }

    find(id: number): Observable<Snip> {
        return this.http.get<Snip>(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<HttpResponse<Snip[]>> {
        const options = createRequestOption(req);
        return this.http.get<Snip[]>(this.resourceUrl, { params: options, observe: 'response' });
    }
 

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    search(req?: any): Observable<HttpResponse<Snip[]>> {
        const options = createRequestOption(req);
        return this.http.get<Snip[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }
    querySnipBySlot(slotId:number,req?: any): Observable<Snip> {
        return this.http.get<Snip>(this.resourceUrl+"/getSnipsOfGivenSlot/"+slotId);
    }
}
