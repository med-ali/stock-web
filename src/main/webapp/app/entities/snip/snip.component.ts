import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { Snip } from './snip.model';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, PrintService, SpinnerService } from '../../shared';
import { NgxBarcodeComponent } from 'ngx-barcode';
import { SnipService } from './snip.service';

@Component({
    selector: 'jhi-snip',
    templateUrl: './snip.component.html'
})
export class SnipComponent implements OnInit, OnDestroy {

    currentAccount: any;
    snips: Snip[]; 
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    checkall: boolean;
    @ViewChild('ngxBarCode') ngxBarCode: NgxBarcodeComponent;
    barcodeValue: string;

    constructor(
        private snipService: SnipService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private printService: PrintService,
        private spinnerService: SpinnerService,
        private eventManager: JhiEventManager
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        this.checkall = false;
        if (this.currentSearch) {
            this.snipService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res) => this.onSuccess(res.body, res.headers),
                (res) => this.onError(res.body)
            );
            return;
        }
        this.snipService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res) => this.onSuccess(res.body, res.headers),
            (res) => this.onError(res.body)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/snip'], {
            queryParams:
                {
                    page: this.page,
                    size: this.itemsPerPage,
                    search: this.currentSearch,
                    sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
                }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/snip', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/snip', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.checkall = false;
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInSlots();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Snip) {
        return item.id;
    }
    registerChangeInSlots() {
        this.eventSubscriber = this.eventManager.subscribe('snipListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        //this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.snips = data;
    }
    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
    previousState() {
        window.history.back();
    }
    renderIndex(snip, snips: Snip[]): number {
        for (let i = 0; i < snips.length; i++) {
            const currentSlot = snips[i];
            if (currentSlot.id === snip.id) {
                return i;
            }
        }
        return -1;
    }

}
