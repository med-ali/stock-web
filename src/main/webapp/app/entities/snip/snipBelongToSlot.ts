
export class SnipBelongToSlot {
    constructor(
        public snip?: boolean, 
        public slot?: any,
    ) {
    }
}
