import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Payment } from './payment.model';
import { createRequestOption } from '../../shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';

@Injectable()
export class PaymentService {

    private resourceUrl = SERVER_API_URL + 'api/payments';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/payments';

    constructor(private http: HttpClient) { }

    create(payment: Payment): Observable<Payment> {
        return this.http.post<Payment>(this.resourceUrl, payment);
    }

    checkCode(code: string): Observable<Payment> {
        return this.http.get<Payment>(this.resourceUrl+"/checkcode/?code="+code);
    }

    update(payment: Payment): Observable<Payment> {
        return this.http.put<Payment>(this.resourceUrl, payment);
    }

    find(id: number): Observable<Payment> {
        return this.http.get<Payment>(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<HttpResponse<Payment[]>> {
        const options = createRequestOption(req);
        return this.http.get<Payment[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<HttpResponse<Payment[]>> {
        const options = createRequestOption(req);
        return this.http.get<Payment[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }
}
