import {
    PaymentService,
    PaymentPopupService,
    PaymentComponent,
    PaymentDetailComponent,
    PaymentDialogComponent,
    PaymentPopupComponent,
    PaymentDeletePopupComponent,
    PaymentDeleteDialogComponent,
    paymentRoute,
    paymentPopupRoute,
    PaymentResolvePagingParams,
} from './';

export const payment_declarations = [
    PaymentComponent,
    PaymentDetailComponent,
    PaymentDialogComponent,
    PaymentDeleteDialogComponent,
    PaymentPopupComponent,
    PaymentDeletePopupComponent,
];

export const payment_entryComponents = [
    PaymentComponent,
    PaymentDialogComponent,
    PaymentPopupComponent,
    PaymentDeleteDialogComponent,
    PaymentDeletePopupComponent,
];

export const payment_providers = [
    PaymentService,
    PaymentPopupService,
    PaymentResolvePagingParams,
];
