import { BaseEntity } from '../../shared';

export class Payment implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public description?: string,
    ) {
    }
}
