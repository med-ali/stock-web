import {
    SlotService,
    SlotPopupService,
    SlotComponent,
    SlotDetailComponent,
    SlotDialogComponent,
    SlotPopupComponent,
    SlotDeletePopupComponent,
    SlotDeleteDialogComponent,
    slotRoute,
    slotPopupRoute,
    SlotResolvePagingParams,
} from './';
import { SlotAutoPopupComponent, AutoSlotDialogComponent } from './auto-slot-dialog.component';

export const slot_declarations = [
    SlotComponent,
    SlotDetailComponent,
    SlotDialogComponent,
    SlotDeleteDialogComponent,
    SlotPopupComponent,
    SlotDeletePopupComponent,
    SlotAutoPopupComponent,
    AutoSlotDialogComponent
];

export const slot_entryComponents = [
    SlotComponent,
    SlotDialogComponent,
    SlotPopupComponent,
    SlotDeleteDialogComponent,
    SlotDeletePopupComponent,
    SlotAutoPopupComponent,
    AutoSlotDialogComponent
];

export const slot_providers = [
    SlotService,
    SlotPopupService,
    SlotResolvePagingParams,
];
