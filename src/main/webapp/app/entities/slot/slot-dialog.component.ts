import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Slot } from './slot.model';
import { SlotPopupService } from './slot-popup.service';
import { SlotService } from './slot.service';
import { ShelvingUnit, ShelvingUnitService } from '../shelving-unit';
import { ResponseWrapper } from '../../shared';
import { Shelf } from '../shelf';

@Component({
    selector: 'jhi-slot-dialog',
    templateUrl: './slot-dialog.component.html'
})
export class SlotDialogComponent implements OnInit {

    slot: Slot;
    isSaving: boolean;
    active: any;
    descriptionDrawer:string; 
    drawerModal: boolean;
    existCode:boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private slotService: SlotService,
        private shelvingUnitService: ShelvingUnitService,
        private eventManager: JhiEventManager
    ) {
    }
     
    ngOnInit() {
        this.isSaving = false;
        this.active = 'yes';
        this.slot.isActive = true;
        this.slot.isEmpty = true;
        this.drawerModal = false;
        if(this.slot.drawer){
            this.descriptionDrawer = this.slot.drawer.code + "-" + this.slot.drawer.description;
        }
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    public activeChange(event:String) {
        console.log(event)
        if (event === 'yes') {
            this.slot.isActive = true;
        } else if (event === 'no') {
            this.slot.isActive = false;
        }
        console.log('up' + this.slot.isActive)
    }

    save() {
        this.isSaving = true;
        if (this.slot.id !== undefined) {
            this.subscribeToSaveResponse(
                this.slotService.update(this.slot));
        } else {
            this.subscribeToSaveResponse(
                this.slotService.create(this.slot));
        }
        this.clear()
    
    }

    private subscribeToSaveResponse(result: Observable<Shelf>) {
        result.subscribe((res: Shelf) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Shelf) {
        this.eventManager.broadcast({ name: 'slotListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackShelvingUnitById(index: number, item: ShelvingUnit) {
        return item.id;
    }

    openMyModal(event) {
        if (event === 'drawerModal') {
            this.drawerModal = true;
        }
        document.querySelector('#' + event).classList.add('md-show');
    }

    closeModal(event) {
        this.drawerModal = false;
        (document.querySelector('#' + event).parentElement.parentElement).classList.remove('md-show');
    }

    closeMyModalEvent(event) {
        ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
    }

    assignDrawer(drawer) {
        this.slot.drawer = drawer;
        this.descriptionDrawer = this.slot.drawer.code + "-" + this.slot.drawer.description;
        this.closeModal('closeDrawerBtn');
    }
}

@Component({
    selector: 'jhi-slot-popup',
    template: ''
})
export class SlotPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private slotPopupService: SlotPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.slotPopupService
                    .open(SlotDialogComponent as Component, params['id']);
            } else {
                this.slotPopupService
                    .open(SlotDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
