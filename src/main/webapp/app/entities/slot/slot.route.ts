import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { SlotComponent } from './slot.component';
import { SlotDetailComponent } from './slot-detail.component';
import { SlotPopupComponent } from './slot-dialog.component';
import { SlotDeletePopupComponent } from './slot-delete-dialog.component'; 
import { SlotAutoPopupComponent } from './auto-slot-dialog.component';

@Injectable()
export class SlotResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const  slotRoute: Routes = [
    {
        path: 'slot',
        component: SlotComponent,
        resolve: {
            'pagingParams': SlotResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.slot.default'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'slot/:id',
        component: SlotDetailComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.slot.default'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const slotPopupRoute: Routes = [
    {
        path: 'slot-new',
        component: SlotPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.slot.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'slot/:id/edit',
        component: SlotPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.slot.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'slot/:id/delete',
        component: SlotDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.slot.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },{
        path: 'slot/:id/drawer',
        component: SlotAutoPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.slot.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
];
