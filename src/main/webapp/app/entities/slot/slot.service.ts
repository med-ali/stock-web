import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Slot } from './slot.model';
import { createRequestOption } from '../../shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { Drawer } from '../drawer';

@Injectable()
export class SlotService {

    private resourceUrl = SERVER_API_URL + 'api/slots';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/slots';

    constructor(private http: HttpClient) { }

    create(d: Slot): Observable<Slot> {
        return this.http.post<Slot>(this.resourceUrl, d);
    }

    checkCode(code: string): Observable<Slot> {
        return this.http.get<Slot>(this.resourceUrl+"/checkcode/?code="+code);
    }

    update(d: Slot): Observable<Slot> {
        return this.http.put<Slot>(this.resourceUrl, d);
    }

    find(id: number): Observable<Slot> {
        return this.http.get<Slot>(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<HttpResponse<any[]>> {
        const options = createRequestOption(req);
        return this.http.get<Slot[]>(this.resourceUrl, { params: options, observe: 'response' });
    }
    querySlotsByDrawer(drawerId:number,req?: any): Observable<HttpResponse<Slot[]>> {
        const options = createRequestOption(req);
        return this.http.get<Slot[]>(this.resourceUrl+"/getSlotsOfGivenDrawer/"+drawerId, { params: options, observe: 'response' });
    }
    
    queryEmptySlotsByDrawer(drawerId:number,req?: any): Observable<HttpResponse<Slot[]>> {
        const options = createRequestOption(req);
        return this.http.get<Slot[]>(this.resourceUrl+"/getEmptySlotsOfGivenDrawer/"+drawerId, { params: options, observe: 'response' });
    }
    queryNotEmptySlotsByDrawer(drawerId:number,req?: any): Observable<HttpResponse<Slot[]>> {
        const options = createRequestOption(req);
        return this.http.get<Slot[]>(this.resourceUrl+"/getNotEmptySlotsOfGivenDrawer/"+drawerId, { params: options, observe: 'response' });
    }
    searchSlotsByDrawer(drawerId:number,req?: any): Observable<HttpResponse<any[]>> {
        const options = createRequestOption(req);
        return this.http.get<Slot[]>(this.resourceSearchUrl+"/getSlotsOfGivenDrawer/"+drawerId, { params: options, observe: 'response' });
    }
    searchEmptySlotsByDrawer(drawerId:number,req?: any): Observable<HttpResponse<Slot[]>> {
        const options = createRequestOption(req);
        return this.http.get<Slot[]>(this.resourceSearchUrl+"/getEmptySlotsOfGivenDrawer/"+drawerId, { params: options, observe: 'response' });
    }

    searchNotEmptySlotsByDrawer(drawerId:number,req?: any): Observable<HttpResponse<Slot[]>> {
        const options = createRequestOption(req);
        return this.http.get<Slot[]>(this.resourceSearchUrl+"/getNotEmptySlotsOfGivenDrawer/"+drawerId, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<HttpResponse<any[]>> {
        const options = createRequestOption(req);
        return this.http.get<Slot[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }
  
     createSlotToDrawer(d:Drawer,nbr:number): Observable<Slot[]> {
        return this.http.post<Slot[]>(this.resourceUrl + "/drawers?nbr="+nbr, d);
     }
}
