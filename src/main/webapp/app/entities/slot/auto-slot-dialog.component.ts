import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Slot } from './slot.model';
import { SlotPopupService } from './slot-popup.service';
import { SlotService } from './slot.service';
import { ShelvingUnit, ShelvingUnitService } from '../shelving-unit';
import { ResponseWrapper } from '../../shared';
import { Shelf } from '../shelf';
import { DrawerPopupService, Drawer } from '../drawer';

@Component({
    selector: 'jhi-auto-slot-dialog',
    templateUrl: './auto-slot-dialog.component.html'
})
export class AutoSlotDialogComponent implements OnInit {
   

    drawer: Drawer;
    alerts = [];
    descriptionDrawer;
    slotNumber:number;
    constructor(private alertService: JhiAlertService,
        private slotService: SlotService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }
    ngOnInit() {
        this.descriptionDrawer = this.drawer.code + "-" + this.drawer.description
    }
    clear() { 
        this.activeModal.dismiss('cancel');
    }
  
    confirm(drawer:Drawer) { 
        
        this.subscribeToSaveResponse(
            this.slotService.createSlotToDrawer(drawer,this.slotNumber));
        
        this.clear()
    
    }

    private subscribeToSaveResponse(result: Observable<Slot[]>) {
        result.subscribe((res: Slot[]) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result:Slot[]) {
        this.eventManager.broadcast({ name: 'slotForDrawerListModification', content: 'OK' });
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.clear();
    }

    addErrorAlert(message, key?, data?) {
        key = (key && key !== null) ? key : message;
        this.alerts.push(
            this.alertService.addAlert(
                {
                    type: 'danger',
                    msg: key,
                    params: data,
                    timeout: 5000,
                    toast: this.alertService.isToast(),
                    scoped: true
                },
                this.alerts
            )
        );
    }
}

@Component({
    selector: 'jhi-slot-auto-popup',
    template: ''
})
export class SlotAutoPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private slotPopupService: SlotPopupService,
        private drawerPopupService:  DrawerPopupService

    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.drawerPopupService
                .open(AutoSlotDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
