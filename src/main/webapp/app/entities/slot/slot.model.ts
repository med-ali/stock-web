import { BaseEntity } from '../../shared';

export class Slot implements BaseEntity {
    constructor(
        public id?: number,
        public note?: string,
        public barCode?: string,
        public isActive?: boolean,
        public isEmpty?: boolean,
        public drawer?: any,
        public checked?: boolean,
    ) {
        this.isActive = true;
        this.isEmpty = true;
    }
}
