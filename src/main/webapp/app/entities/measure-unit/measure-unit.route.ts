import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { MeasureUnitComponent } from './measure-unit.component';
import { MeasureUnitDetailComponent } from './measure-unit-detail.component';
import { MeasureUnitPopupComponent } from './measure-unit-dialog.component';
import { MeasureUnitDeletePopupComponent } from './measure-unit-delete-dialog.component';

@Injectable()
export class MeasureUnitResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const measureUnitRoute: Routes = [
    {
        path: 'measure-unit',
        component: MeasureUnitComponent,
        resolve: {
            'pagingParams': MeasureUnitResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.measureUnit.default'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'measure-unit/:id',
        component: MeasureUnitDetailComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.measureUnit.default'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const measureUnitPopupRoute: Routes = [
    {
        path: 'measure-unit-new',
        component: MeasureUnitPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.measureUnit.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'measure-unit/:id/edit',
        component: MeasureUnitPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.measureUnit.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'measure-unit/:id/delete',
        component: MeasureUnitDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.measureUnit.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
