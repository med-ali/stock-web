import {
    MeasureUnitService,
    MeasureUnitPopupService,
    MeasureUnitComponent,
    MeasureUnitDetailComponent,
    MeasureUnitDialogComponent,
    MeasureUnitPopupComponent,
    MeasureUnitDeletePopupComponent,
    MeasureUnitDeleteDialogComponent,
    measureUnitRoute,
    measureUnitPopupRoute,
    MeasureUnitResolvePagingParams,
} from './';

export const measureUnit_declarations = [
    MeasureUnitComponent,
    MeasureUnitDetailComponent,
    MeasureUnitDialogComponent,
    MeasureUnitDeleteDialogComponent,
    MeasureUnitPopupComponent,
    MeasureUnitDeletePopupComponent,
];

export const measureUnit_entryComponents = [
    MeasureUnitComponent,
    MeasureUnitDialogComponent,
    MeasureUnitPopupComponent,
    MeasureUnitDeleteDialogComponent,
    MeasureUnitDeletePopupComponent,
];

export const measureUnit_providers = [
    MeasureUnitService,
    MeasureUnitPopupService,
    MeasureUnitResolvePagingParams,
];
