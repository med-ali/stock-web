import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { MeasureUnit } from './measure-unit.model';
import { MeasureUnitPopupService } from './measure-unit-popup.service';
import { MeasureUnitService } from './measure-unit.service';

@Component({
    selector: 'jhi-measure-unit-dialog',
    templateUrl: './measure-unit-dialog.component.html'
})
export class MeasureUnitDialogComponent implements OnInit {

    measureUnit: MeasureUnit;
    isSaving: boolean;
    existCode: boolean;
    constructor(
        public activeModal: NgbActiveModal,
        private measureUnitService: MeasureUnitService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.measureUnitService.checkCode(this.measureUnit.code).subscribe((value) => { 
            console.log(value) 
            if (value==true && this.measureUnit.id == undefined){
                this.existCode = true;
            }
            else{
                this.existCode = false;
                this.isSaving = true;
                if (this.measureUnit.id !== undefined) {
                    this.subscribeToSaveResponse(
                        this.measureUnitService.update(this.measureUnit));
                } else {
                    this.subscribeToSaveResponse(
                        this.measureUnitService.create(this.measureUnit));
                }
                this.clear()
            }
            },
            (error) => { 
            console.log("error"+error)
        })  
    }

    private subscribeToSaveResponse(result: Observable<MeasureUnit>) {
        result.subscribe((res: MeasureUnit) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: MeasureUnit) {
        this.eventManager.broadcast({ name: 'measureUnitListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-measure-unit-popup',
    template: ''
})
export class MeasureUnitPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private measureUnitPopupService: MeasureUnitPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.measureUnitPopupService
                    .open(MeasureUnitDialogComponent as Component, params['id']);
            } else {
                this.measureUnitPopupService
                    .open(MeasureUnitDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
