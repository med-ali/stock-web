import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { MeasureUnit } from './measure-unit.model';
import { MeasureUnitService } from './measure-unit.service';

@Component({
    selector: 'jhi-measure-unit-detail',
    templateUrl: './measure-unit-detail.component.html'
})
export class MeasureUnitDetailComponent implements OnInit, OnDestroy {

    measureUnit: MeasureUnit;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private measureUnitService: MeasureUnitService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInMeasureUnits();
    }

    load(id) {
        this.measureUnitService.find(id).subscribe((measureUnit) => {
            this.measureUnit = measureUnit;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInMeasureUnits() {
        this.eventSubscriber = this.eventManager.subscribe(
            'measureUnitListModification',
            (response) => this.load(this.measureUnit.id)
        );
    }
}
