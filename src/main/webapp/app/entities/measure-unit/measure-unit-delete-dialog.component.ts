import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { MeasureUnit } from './measure-unit.model';
import { MeasureUnitPopupService } from './measure-unit-popup.service';
import { MeasureUnitService } from './measure-unit.service';

@Component({
    selector: 'jhi-measure-unit-delete-dialog',
    templateUrl: './measure-unit-delete-dialog.component.html'
})
export class MeasureUnitDeleteDialogComponent {

    measureUnit: MeasureUnit;
    alerts = [];
    constructor(private alertService: JhiAlertService,
        private measureUnitService: MeasureUnitService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }
    addErrorAlert(message, key?, data?) {
        key = (key && key !== null) ? key : message;
        this.alerts.push(
            this.alertService.addAlert(
                {
                    type: 'danger',
                    msg: key,
                    params: data,
                    timeout: 5000,
                    toast: this.alertService.isToast(),
                    scoped: true
                },
                this.alerts
            )
        );
    }

    confirmDelete(id: number) {
        this.measureUnitService.delete(id).subscribe((response) => {
            if (response["status"] == 200){
                this.eventManager.broadcast({
                    name: 'measureUnitListModification',
                    content: 'Deleted an measureUnit'
                });
                this.activeModal.dismiss(true);
            }
            else{
                this.addErrorAlert('Delete not reachable', 'error.entity.delete.reachable');
            } 
        });
    }
}

@Component({
    selector: 'jhi-measure-unit-delete-popup',
    template: ''
})
export class MeasureUnitDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private measureUnitPopupService: MeasureUnitPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.measureUnitPopupService
                .open(MeasureUnitDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
