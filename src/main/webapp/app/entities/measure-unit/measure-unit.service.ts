import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { MeasureUnit } from './measure-unit.model';
import { createRequestOption } from '../../shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';

@Injectable()
export class MeasureUnitService {

    private resourceUrl = SERVER_API_URL + 'api/measure-units';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/measure-units';

    constructor(private http: HttpClient) { }

    create(measureUnit: MeasureUnit): Observable<MeasureUnit> {
        return this.http.post<MeasureUnit>(this.resourceUrl, measureUnit);
    }

    checkCode(code: string): Observable<MeasureUnit> {
        return this.http.get<MeasureUnit>(this.resourceUrl+"/checkcode/?code="+code);
    }

    update(measureUnit: MeasureUnit): Observable<MeasureUnit> {
        return this.http.put<MeasureUnit>(this.resourceUrl, measureUnit);
    }

    find(id: number): Observable<MeasureUnit> {
        return this.http.get<MeasureUnit>(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<HttpResponse<MeasureUnit[]>> {
        const options = createRequestOption(req);
        return this.http.get<MeasureUnit[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<HttpResponse<MeasureUnit[]>> {
        const options = createRequestOption(req);
        return this.http.get<MeasureUnit[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }
}
