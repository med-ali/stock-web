import { BaseEntity } from './../../shared';
import { OrderDetails } from '../order-details';
import { Article } from '../article';

export const enum DEState {
    'ST',
    'PS',
    'NS'
}

export class DeliveredEquipement implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public description?: string,
        public deliveryDate?: any,
        public expirationDate?: any,
        public quantityPerPackage?: number,
        public receivedPackage?: number,
        public stockedQuantity?: number,
        public deliveredQuantity?: number,
        public ddt?: string,
        public isStocked?: boolean,
        public insertCode?: string,
        public productionBatch?: string,
        public annuled?: boolean,
        public deliveredEquipmentStatus?: DEState,
        public supplier?: any,
        public packagingMethod?: any,
        public article?: any, 
        public kit?: any,
        public orderDetail?: any,
        public product?: any,
        public unitPrice?:number,
    ) {
        this.isStocked = false;
        this.annuled = false;
        this.deliveredQuantity = 0;
    }
}
