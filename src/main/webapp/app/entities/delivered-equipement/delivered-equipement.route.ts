import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { DeliveredEquipementComponent } from './delivered-equipement.component';
import { DeliveredEquipementDetailComponent } from './delivered-equipement-detail.component';
import { DeliveredEquipementPopupComponent } from './delivered-equipement-dialog.component';
import { DeliveredEquipementDeletePopupComponent } from './delivered-equipement-delete-dialog.component';

@Injectable()
export class DeliveredEquipementResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const deliveredEquipementRoute: Routes = [
    {
        path: 'delivered-equipement',
        component: DeliveredEquipementComponent,
        resolve: {
            'pagingParams': DeliveredEquipementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.deliveredEquipement.default'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'delivered-equipement/:id',
        component: DeliveredEquipementDetailComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.deliveredEquipement.default'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const deliveredEquipementPopupRoute: Routes = [
    {
        path: 'delivered-equipement-new',
        component: DeliveredEquipementPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.deliveredEquipement.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'delivered-equipement/:id/edit',
        component: DeliveredEquipementPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.deliveredEquipement.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'delivered-equipement/:id/delete',
        component: DeliveredEquipementDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.deliveredEquipement.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
