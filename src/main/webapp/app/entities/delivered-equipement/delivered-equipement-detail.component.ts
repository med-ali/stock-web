import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { DeliveredEquipement } from './delivered-equipement.model';
import { DeliveredEquipementService } from './delivered-equipement.service';

@Component({
    selector: 'jhi-delivered-equipement-detail',
    templateUrl: './delivered-equipement-detail.component.html'
})
export class DeliveredEquipementDetailComponent implements OnInit, OnDestroy {

    deliveredEquipement: DeliveredEquipement;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private deliveredEquipementService: DeliveredEquipementService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDeliveredEquipements();
    }

    load(id) {
        this.deliveredEquipementService.find(id).subscribe((deliveredEquipement) => {
            this.deliveredEquipement = deliveredEquipement;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDeliveredEquipements() {
        this.eventSubscriber = this.eventManager.subscribe(
            'deliveredEquipementListModification',
            (response) => this.load(this.deliveredEquipement.id)
        );
    }
}
