import {
    DeliveredEquipementService,
    DeliveredEquipementPopupService,
    DeliveredEquipementComponent,
    DeliveredEquipementDetailComponent,
    DeliveredEquipementDialogComponent,
    DeliveredEquipementPopupComponent,
    DeliveredEquipementDeletePopupComponent,
    DeliveredEquipementDeleteDialogComponent,
    deliveredEquipementRoute,
    deliveredEquipementPopupRoute,
    DeliveredEquipementResolvePagingParams,
} from './';

export const deliveredEquipement_declarations = [
    DeliveredEquipementComponent,
    DeliveredEquipementDetailComponent,
    DeliveredEquipementDialogComponent,
    DeliveredEquipementDeleteDialogComponent,
    DeliveredEquipementPopupComponent,
    DeliveredEquipementDeletePopupComponent,
];

export const deliveredEquipement_entryComponents = [
    DeliveredEquipementComponent,
    DeliveredEquipementDialogComponent,
    DeliveredEquipementPopupComponent,
    DeliveredEquipementDeleteDialogComponent,
    DeliveredEquipementDeletePopupComponent,
];

export const deliveredEquipement_providers = [
    DeliveredEquipementService,
    DeliveredEquipementPopupService,
    DeliveredEquipementResolvePagingParams,
];
