import { Component, OnInit, OnDestroy, Output, EventEmitter, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { DeliveredEquipement } from './delivered-equipement.model';
import { DeliveredEquipementPopupService } from './delivered-equipement-popup.service';
import { DeliveredEquipementService } from './delivered-equipement.service';
import { Supplier, SupplierService } from '../supplier';
import { PackagingMethod, PackagingMethodService } from '../packaging-method';
import { Article, ArticleService } from '../article';
import { Kit, KitService } from '../kit';
import { OrderDetails, OrderDetailsService } from '../order-details';
import { Product, ProductService } from '../product';
import { Commande, CommandeService } from '../commande';
import { ResponseWrapper } from '../../shared';
import swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { KitNormalisation } from '../kit/kit-normalisation.model';
import { MainService } from '../../shared/utils/main.service';
import { ArticleNormalisation } from '../article/article-normalisation.model';

@Component({
    selector: 'jhi-delivered-equipement-dialog',
    templateUrl: './delivered-equipement-dialog.component.html',
    styleUrls: [
        '../../../../../../node_modules/sweetalert2/src/sweetalert2.scss'
    ]
})
export class DeliveredEquipementDialogComponent implements OnInit {

    deliveredEquipement: DeliveredEquipement;
    selectedCommande: Commande;
    articleNormalisations: ArticleNormalisation[];
    kitNormalisations: KitNormalisation[];
    isSaving: boolean;

    deliveredQuantityMaxValue: number;
    deliveredQuantityMinValue: number;
    productReceivedCommands = [];
    listProductsChecked = [];
    products = [];
    checkAll: boolean;

    step = 1;
    fromOrder: any;
    kit: false;
    supplierModal: boolean;
    articleModal: boolean;
    kitModal: boolean;
    productModal: boolean;
    commandeModal: boolean;
    orderDetailsModal: boolean;
    descriptionSupplier:string;
    descriptionSelectedCommande:string;
    descriptionArticle:string;
    descriptionKit:string;
    descriptionOrderDetail:string;
    descriptionPackagingMethod:string;
    dateDev:any;
    dateExp:any;
    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private deliveredEquipementService: DeliveredEquipementService,
        private supplierService: SupplierService,
        private commandeService: CommandeService,
        private packagingMethodService: PackagingMethodService,
        private articleService: ArticleService,
        private mainService: MainService,
        private kitService: KitService,
        private orderDetailsService: OrderDetailsService,
        private productService: ProductService,
        private eventManager: JhiEventManager,
        private translateService: TranslateService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.kit = false;
        this.orderDetailsModal = this.commandeModal = this.productModal = this.articleModal = this.kitModal = this.supplierModal = false;
        if (this.deliveredEquipement.id !== undefined) {
            this.deliveredQuantityMinValue = 0;
            this.deliveredQuantityMaxValue = 1000;
            if (this.deliveredEquipement.orderDetail != null) {
                this.fromOrder = 'yes';
            } else {
                this.fromOrder = 'no';              
            }
            this.dateDev = this.deliveredEquipement.deliveryDate;
            this.deliveredEquipement.deliveryDate = this.mainService.convertDateForInputDate(new Date());
            this.dateExp = this.deliveredEquipement.expirationDate;
            if(this.deliveredEquipement.expirationDate){
               this.deliveredEquipement.expirationDate = this.deliveredEquipement.expirationDate.split('T')[0];
            }
            
        } else {
            this.deliveredQuantityMinValue = 0;
            this.deliveredQuantityMaxValue = 1000;
            this.fromOrder = 'yes';
            this.deliveredEquipement.receivedPackage = 1;
            this.deliveredEquipement.deliveryDate = this.mainService.convertDateForInputDate(new Date());
        }
        if (this.deliveredEquipement.orderDetail !== undefined && this.deliveredEquipement.orderDetail != null) {
            this.selectedCommande = this.deliveredEquipement.orderDetail.order;
        }

        if(this.deliveredEquipement.supplier){
            this.descriptionSupplier = this.deliveredEquipement.supplier.companyName + "-" + this.deliveredEquipement.supplier.email;
        }

        if(this.selectedCommande){
            this.descriptionSelectedCommande = this.selectedCommande.code + "-" + this.selectedCommande.description;
            if (this.selectedCommande.header){
                this.deliveredEquipement.ddt = this.selectedCommande.header;
            }
        }

        if(this.deliveredEquipement.article){
            this.descriptionArticle = this.deliveredEquipement.article.code + "-" + this.deliveredEquipement.article.description  
        }
        if(this.deliveredEquipement.kit){
            this.descriptionKit = this.deliveredEquipement.kit.code + "-" + this.deliveredEquipement.kit.description  
        }
        
        if(this.deliveredEquipement.orderDetail){
            this.descriptionOrderDetail = this.deliveredEquipement.orderDetail.code + "-" + this.deliveredEquipement.orderDetail.description;
        }

        if(this.deliveredEquipement.packagingMethod){
            this.descriptionPackagingMethod = this.deliveredEquipement.packagingMethod.code + "-" + this.deliveredEquipement.packagingMethod.description;
        }
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    changeQuantity() {
        if (this.deliveredEquipement.orderDetail && this.deliveredEquipement.orderDetail.article) {
            this.deliveredEquipement.deliveredQuantity = this.deliveredEquipement.receivedPackage * this.deliveredEquipement.quantityPerPackage;
        } else if (this.deliveredEquipement.orderDetail && this.deliveredEquipement.orderDetail.kit) {
            this.changeProduct(this.listProductsChecked);
        } else {
            if (this.deliveredEquipement.article) {
                this.calculateQuantity(this.articleNormalisations);
            } else if (this.deliveredEquipement.kit) {
                this.changeProduct(this.listProductsChecked);
            }
        }

    }

    updateQuantityPerPackage(event) {
        this.deliveredEquipement.quantityPerPackage = event;
        if (event != null && this.deliveredEquipement.receivedPackage != null) {
            this.deliveredEquipement.deliveredQuantity = this.deliveredEquipement.quantityPerPackage * this.deliveredEquipement.receivedPackage;
        } else {
            this.deliveredEquipement.deliveredQuantity = null;
        }
    }
    updateReceivedPackage(event) {
        this.deliveredEquipement.receivedPackage = event;
        if (event != null && this.deliveredEquipement.quantityPerPackage != null) {
            this.deliveredEquipement.deliveredQuantity = this.deliveredEquipement.quantityPerPackage * this.deliveredEquipement.receivedPackage;
        } else {
            this.deliveredEquipement.deliveredQuantity = null;
        }
    }

    save() {
        //this.isSaving = true;
        if(this.dateExp){
            this.deliveredEquipement.expirationDate = this.mainService.convertStringDateForInputZonedDate(this.deliveredEquipement.expirationDate);
        }
        if(this.dateDev){
            this.deliveredEquipement.deliveryDate = this.dateDev;
        } 
        if (this.deliveredEquipement.orderDetail){
            this.deliveredEquipement.article=this.deliveredEquipement.orderDetail.article;
        }
        
        if (this.deliveredEquipement.id !== undefined) {
            this.subscribeToSaveResponse(
                this.deliveredEquipementService.update(this.deliveredEquipement));
        } else {
            this.deliveredEquipement.deliveryDate = this.mainService.convertDateForInputDateLocal(new Date());
            /* let normalisationCommand = {
                deliveredEquipement: this.deliveredEquipement,
                productNumber: this.productReceivedCommands.length,
                normalisations: null
            };
            if (this.deliveredEquipement.kit || (this.deliveredEquipement.orderDetail && this.deliveredEquipement.orderDetail.kit)) {
                normalisationCommand.normalisations = this.kitNormalisations;
            } else if (this.deliveredEquipement.article) {
                normalisationCommand.normalisations = this.articleNormalisations;
            } */
            if (this.deliveredEquipement.kit != null || (this.deliveredEquipement.orderDetail && this.deliveredEquipement.orderDetail.kit)) {
                this.subscribeToSaveResponse(
                    this.deliveredEquipementService.create(this.deliveredEquipement, true, this.kitNormalisations));
            } else {
                this.subscribeToSaveResponse( 
                    this.deliveredEquipementService.create(this.deliveredEquipement, false));
            }
        }
    }

    private findProductAssignedWithArticle(id) {
        this.productService.findAssignedWithArticle(id).subscribe((product) => {
            this.deliveredEquipement.product = product.body;
        });
    }

    private subscribeToSaveResponse(result: Observable<DeliveredEquipement>) {
        result.subscribe((res: DeliveredEquipement) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: DeliveredEquipement) {
        this.eventManager.broadcast({ name: 'deliveredEquipementListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    public fromOrderChange(event) {
        this.deliveredEquipement.receivedPackage = 1;
        if (event !== this.fromOrder) {
            this.selectedCommande = null;
            this.deliveredEquipement.orderDetail = null;
            this.deliveredEquipement.article = null;
            this.deliveredEquipement.kit = null;
            this.deliveredEquipement.product = null;
            this.deliveredEquipement.packagingMethod = null;
        }
    }

    public initSubs(entity) {
        
        if (entity === 'product') {
            this.deliveredEquipement.product = null;
        }
        if (entity === 'artkit') {
            this.deliveredEquipement.article = null;
            this.deliveredEquipement.kit = null;
            this.deliveredEquipement.product = null;
            this.deliveredEquipement.packagingMethod = null;
            this.descriptionArticle = ''
            this.descriptionKit = ''
        }
        if (entity === 'order') {
            this.selectedCommande = null;
            this.deliveredEquipement.orderDetail = null;
            this.descriptionOrderDetail = '';
            this.deliveredEquipement.product = null;
            this.deliveredEquipement.packagingMethod = null;
            this.descriptionSelectedCommande = ''
            this.deliveredEquipement.ddt = '';
        }
        if (entity === 'orderDetails') {
            this.deliveredEquipement.orderDetail = null;
            this.deliveredEquipement.product = null;
            this.deliveredEquipement.packagingMethod = null;
            this.descriptionOrderDetail = ''
        }
        if (entity === 'supplier') {
            this.deliveredEquipement.supplier = null;
            this.selectedCommande = null;
            this.deliveredEquipement.orderDetail = null;
            this.descriptionOrderDetail = '';
            this.deliveredEquipement.article = null;
            this.deliveredEquipement.kit = null;
            this.deliveredEquipement.product = null;
            this.deliveredEquipement.packagingMethod = null;
            this.descriptionSupplier = ''
            this.descriptionSelectedCommande = ''
            this.deliveredEquipement.ddt = '';
        }
    }

    getDeliveredQuantityMaxValue() {
        const listProductCommand = {
            'orderDetails': this.deliveredEquipement.orderDetail,
            'productReceivedCommands': this.listProductsChecked
        }; 
        this.deliveredEquipementService.getDeliveredQuantityMaxValue(listProductCommand).subscribe((res) => {
            this.deliveredQuantityMaxValue = res.body;
        });
    }

    loadKitNormalisations() {
        this.kitNormalisations = [];
        let kitId: any;
        if (this.deliveredEquipement.orderDetail && this.deliveredEquipement.orderDetail.kit) {
            kitId = this.deliveredEquipement.orderDetail.kit.id;
        } else if (this.deliveredEquipement.kit) {
            kitId = this.deliveredEquipement.kit.id;
        }
        this.kitService.queryNormByKit(kitId).subscribe(
            (res) => this.onSuccess(res.body, res.headers),
            (res) => this.onError(res.body)
        );
    }
    private onSuccess(data, headers) {
        this.kitNormalisations = data;
    }

    openBasicSwal(entity) {
        const title = this.translateService.instant('entity.' + entity + '.swal.title');
        const text = this.translateService.instant('entity.' + entity + '.swal.text');
        swal({
            'title': title,
            'text': text,
            'type': 'error'

        }).catch(swal.noop);
    }

    openMyModal(event) {
        if (event === 'articleModal') {
            this.articleModal = true;
        } else if (event === 'kitModal') {
            this.kitModal = true;
        } else if (event === 'supplierModal') {
            this.supplierModal = true;
        } else if (event === 'productModal') {
            this.productModal = true;
        } else if (event === 'commandeModal') {
            this.commandeModal = true;
        } else if (event === 'orderDetailsModal') {
            this.orderDetailsModal = true;
        }
        document.querySelector('#' + event).classList.add('md-show');
    }

    closeModal(event) {
        this.orderDetailsModal = this.commandeModal = this.productModal = this.articleModal = this.kitModal = this.supplierModal = false;
        (document.querySelector('#' + event).parentElement.parentElement).classList.remove('md-show');
    }

    closeMyModalEvent(event) {
        ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
    }

    assignArticle(normalisation) {
        this.articleNormalisations = [normalisation];
        this.products = [];
        this.products[0] = normalisation.product;
        this.deliveredEquipement.description = normalisation.article.description;
        this.deliveredEquipement.article = normalisation.article;
        this.deliveredEquipement.product = normalisation.product;
        this.deliveredEquipement.kit = null;
        this.deliveredEquipement.orderDetail = null;
        this.deliveredEquipement.code = normalisation.article.code;
        this.deliveredEquipement.packagingMethod = normalisation.article.packagingMethod;
        this.deliveredEquipement.quantityPerPackage = normalisation.article.quantityPerPackage;
        if (normalisation.article.isExpired) {
            this.deliveredEquipement.expirationDate = this.mainService.convertDateForInputDate
                (this.mainService.addDays(new Date(), normalisation.article.numberOfDaysToExpired));
                this.dateExp = this.mainService.convertDateForInputDateLocal
                (this.mainService.addDays(new Date(), normalisation.article.numberOfDaysToExpired));
        } else {
            this.deliveredEquipement.expirationDate = null;
        }
        this.resetDeliveredQuantityMaxValue();
        this.calculateQuantity(this.articleNormalisations);
        this.descriptionArticle = this.deliveredEquipement.article.code + "-" + this.deliveredEquipement.article.description;
        this.descriptionPackagingMethod = this.deliveredEquipement.packagingMethod.code + "-" + this.deliveredEquipement.packagingMethod.description;
        this.closeModal('closeArticleBtn');
    }

    assignKit(kit) {
        this.listProductsChecked = [];
        this.deliveredEquipement.description = kit.description;
        this.deliveredEquipement.kit = kit;
        this.deliveredEquipement.article = null;
        this.deliveredEquipement.orderDetail = null;
        this.deliveredEquipement.code = kit.code;
        this.deliveredEquipement.packagingMethod = kit.packagingMethod;
        this.deliveredEquipement.quantityPerPackage = kit.quantityPerPackage;

        if (kit.isExpired) {
            this.deliveredEquipement.expirationDate = this.mainService.convertDateForInputDate
                (this.mainService.addDays(new Date(), kit.numberOfDaysToExpired));
                this.dateExp = this.mainService.convertDateForInputDateLocal
                (this.mainService.addDays(new Date(), kit.numberOfDaysToExpired));
        } else {
            this.deliveredEquipement.expirationDate = null;
        }
        this.listProductReceivedCommands();
        this.descriptionKit = this.deliveredEquipement.kit.code + "-" + this.deliveredEquipement.kit.description;
        this.descriptionPackagingMethod = this.deliveredEquipement.packagingMethod.code + "-" + this.deliveredEquipement.packagingMethod.description;
        this.closeModal('closeKitBtn');
    }

    assignProduct(product) {
        // this.deliveredEquipement.product = product;
        // this.closeProductModal();
    }
here
    assignOrderDetails(orderDetails) {
        if (!this.deliveredEquipement.orderDetail) {
            this.doOrderDetailsActionsAfterAssignment(orderDetails);
        } else if (this.deliveredEquipement.orderDetail.id !== orderDetails.id) {
            this.doOrderDetailsActionsAfterAssignment(orderDetails);
        }
        this.descriptionOrderDetail = this.deliveredEquipement.orderDetail.code + "-" + this.deliveredEquipement.orderDetail.description;
        this.closeModal('closeOrderDetailsBtn');
    }

    assignCommande(commande) {
        this.selectedCommande = commande;
        this.deliveredEquipement.orderDetail = null;
        this.deliveredEquipement.product = null;
        this.deliveredEquipement.receivedPackage = 1;
        this.descriptionSelectedCommande = this.selectedCommande.code + "-" + this.selectedCommande.description;
        if (this.selectedCommande.header){
            this.deliveredEquipement.ddt = this.selectedCommande.header;
        }
        this.closeModal('closeCommandeBtn');
    }

    assignSupplier(supplier) {
        this.deliveredEquipement.supplier = supplier;
        this.deliveredEquipement.receivedPackage = 1;
        this.initSubs('artkit');
        this.initSubs('order');
        this.descriptionSupplier = this.deliveredEquipement.supplier.companyName + "-" + this.deliveredEquipement.supplier.email;
        this.closeModal('closeSupplierBtn');
    }

    doOrderDetailsActionsAfterAssignment(orderDetails) {
        this.deliveredEquipement.orderDetail = orderDetails;

        if (orderDetails.kit && orderDetails.kit.isExpired) {
            this.deliveredEquipement.expirationDate = this.mainService.convertDateForInputDate
                (this.mainService.addDays(new Date(), orderDetails.kit.numberOfDaysToExpired));
                this.dateExp = this.mainService.convertDateForInputDateLocal
                (this.mainService.addDays(new Date(), orderDetails.kit.numberOfDaysToExpired));
        } else if (orderDetails.article && orderDetails.article.isExpired) {
            this.deliveredEquipement.expirationDate = this.mainService.convertDateForInputDate
                (this.mainService.addDays(new Date(), orderDetails.article.numberOfDaysToExpired));
            this.dateExp = this.mainService.convertDateForInputDateLocal
                (this.mainService.addDays(new Date(), orderDetails.article.numberOfDaysToExpired));
        } else {
            this.deliveredEquipement.expirationDate = null;
        }
        this.deliveredEquipement.kit = null;
        this.deliveredEquipement.article = null;
        this.deliveredEquipement.quantityPerPackage = orderDetails.quantityPerPackage;
        this.deliveredEquipement.packagingMethod = orderDetails.packagingMethod;
        if (orderDetails.article) {
            this.deliveredQuantityMaxValue = (orderDetails.orderedQuantity - orderDetails.receivedQuantity) / orderDetails.quantityPerPackage;
            this.deliveredEquipement.deliveredQuantity = orderDetails.orderedQuantity - orderDetails.receivedQuantity;
            this.deliveredEquipement.receivedPackage = (orderDetails.orderedQuantity - orderDetails.receivedQuantity) / orderDetails.quantityPerPackage;
        }
        if (orderDetails.kit) {
            // this.deliveredEquipement.orderDetail.product = {};
            this.listProductReceivedCommands();
        }
    }

    resetDeliveredQuantityMaxValue() {
        this.deliveredQuantityMaxValue = 1000;
    }

    changeProduct(productReceivedCommands) {
        this.kitNormalisations = [];
        let kitNormalisationCommand = {};
        if (this.deliveredEquipement.orderDetail && this.deliveredEquipement.orderDetail.kit) {
            kitNormalisationCommand = {
                'kitId': this.deliveredEquipement.orderDetail.kit.id,
                'productReceivedCommands': productReceivedCommands
            };
        } else if (this.deliveredEquipement.kit) {
            kitNormalisationCommand = {
                'kitId': this.deliveredEquipement.kit.id,
                'productReceivedCommands': productReceivedCommands
            };
        }
        this.kitService.queryListKitNormalisations(kitNormalisationCommand).subscribe((res) => {
            if (res) {
                this.kitNormalisations = res.body;
                this.calculateQuantity(this.kitNormalisations);
            }
        });
    }

    calculateQuantity(normalisations) {
        let unitSomme = 0 ;
        let deliveredQuantity = 0;
        this.deliveredEquipement.deliveredQuantity = 0;
        let i;
        normalisations.forEach(element => {
            console.log(element+"unitSomme"+element["normalisationUnit"])
            unitSomme += element["normalisationUnit"]
        });
        if (this.deliveredEquipement.orderDetail && this.deliveredEquipement.orderDetail.kit && this.productReceivedCommands.length > 0) {
            for (i = 0; i < normalisations.length; i++) {  
                deliveredQuantity += (this.deliveredEquipement.receivedPackage * this.deliveredEquipement.quantityPerPackage)* normalisations[i].normalisationUnit;
                //deliveredQuantity = deliveredQuantity;
            }

        } else {
            for (i = 0; i < normalisations.length; i++) {
                deliveredQuantity += this.deliveredEquipement.receivedPackage * this.deliveredEquipement.quantityPerPackage;
            }
        }
        if(unitSomme!=0){
            this.deliveredEquipement.deliveredQuantity = deliveredQuantity*unitSomme;
        }
        else{
            this.deliveredEquipement.deliveredQuantity = deliveredQuantity
        }
        
    }

    listProductReceivedCommands() {
        this.productReceivedCommands = [];
        this.checkAll = false;
        if (this.deliveredEquipement.orderDetail && this.deliveredEquipement.orderDetail.kit) {
            this.kitService.queryAssignedProductsKitByStatusOfSupplier(this.deliveredEquipement.supplier.id, this.deliveredEquipement.orderDetail.kit.id,
                this.deliveredEquipement.orderDetail.id).subscribe((res) => {
                    this.productReceivedCommands = res.body;
                    if (this.productReceivedCommands.length > 0) {
                        let desc = '';
                        let i = 0;
                        this.checkAll = true;
                        this.openMyModal('productModal');
                        this.listProductsChecked = Object.assign([], this.productReceivedCommands);
                        this.getDeliveredQuantityMaxValue();
                        for (i; i < this.productReceivedCommands.length - 1; i++) {
                            desc += this.productReceivedCommands[i].product.description + ' / ';
                        }
                        desc += this.productReceivedCommands[i].product.description;
                        this.deliveredEquipement.orderDetail.product.description = desc;
                    }
                    this.changeProduct(this.listProductsChecked);
                });
        } else if (this.deliveredEquipement.kit) {
            this.kitService.queryAssignedProductsKitOfSupplier(this.deliveredEquipement.supplier.id, this.deliveredEquipement.kit.id).subscribe((res) => {
                this.productReceivedCommands = res.body;
                if (this.productReceivedCommands.length > 0) {
                    let descKit = '';
                    let j = 0;
                    this.checkAll = true;
                    this.openMyModal('productModal');
                    this.listProductsChecked = Object.assign([], this.productReceivedCommands);
                    this.deliveredEquipement.product = res.body[0];
                    for (j; j < this.productReceivedCommands.length - 1; j++) {
                        descKit += this.productReceivedCommands[j].product.description + ' / ';
                    }
                    descKit += this.productReceivedCommands[j].product.description;
                    this.deliveredEquipement.product.description = descKit;
                    this.resetDeliveredQuantityMaxValue();
                }
                this.changeProduct(this.listProductsChecked);
            });
        }
    }

    toggleSelection(it) {
        this.checkAll = false;
        if (this.deliveredEquipement.id === undefined) {
            const idx = this.isElemExistInList(this.listProductsChecked, it);
            if (idx > -1) {
                this.listProductsChecked.splice(idx, 1);
            } else {
                this.listProductsChecked.push(it);
            }
        } else {
            this.listProductsChecked = [it];
        }
        if (this.deliveredEquipement.kit && this.listProductsChecked && this.listProductsChecked.length > 0) {
            this.deliveredEquipement.product = this.listProductsChecked[0].product;
        } else if (this.deliveredEquipement.orderDetail && this.deliveredEquipement.orderDetail.kit && this.listProductsChecked && this.listProductsChecked.length > 0) {
            this.getDeliveredQuantityMaxValue();
            this.deliveredEquipement.orderDetail.product = this.listProductsChecked[0].product;
        } else {
            this.deliveredEquipement.product = {};
        }
        this.changeProduct(this.listProductsChecked);
    };

    toggleCheckAll() {
        this.listProductsChecked = [];
        if (!this.checkAll) {
            this.productReceivedCommands.forEach((item) => {
                if (item && !item.received) {
                    this.listProductsChecked.push(item);
                }
            })
            if (this.deliveredEquipement.product) {
                this.deliveredEquipement.product = this.listProductsChecked[0].product;
            } else if (this.deliveredEquipement.orderDetail && this.deliveredEquipement.orderDetail.kit) {
                this.getDeliveredQuantityMaxValue();
                this.deliveredEquipement.orderDetail.product = this.listProductsChecked[0].product;
            }
            this.checkAll = true;
            this.changeProduct(this.listProductsChecked);
        } else {
            this.checkAll = false;
            this.listProductsChecked = [];
            if (this.deliveredEquipement.orderDetail) {
                this.deliveredEquipement.orderDetail.product = {};
            } else {
                this.deliveredEquipement.product = {};
            }
            this.deliveredEquipement.deliveredQuantity = 0;
        }
    }
    isElemExistInList(list, elem) {
        if (elem && list && list.length > 0) {
            for (let i = 0; i < list.length; i++) {
                if (list[i].product.id === elem.product.id) {
                    return i;
                }
            }
        }
        return -1;
    }

    // Product Modal
    transition() {
        this.productReceivedCommands.sort(this.compare);
    }

    compare(a: any, b: any) {
        if (a.product.code < b.product.code) { return -1; }
        if (a.product.code > b.product.code) { return 1; }
        return 0;
    }
}

@Component({
    selector: 'jhi-delivered-equipement-popup',
    template: ''
})
export class DeliveredEquipementPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private deliveredEquipementPopupService: DeliveredEquipementPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.deliveredEquipementPopupService
                    .open(DeliveredEquipementDialogComponent as Component, params['id']);
            } else {
                this.deliveredEquipementPopupService
                    .open(DeliveredEquipementDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
