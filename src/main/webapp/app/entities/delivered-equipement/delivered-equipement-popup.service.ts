import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { DeliveredEquipement } from './delivered-equipement.model';
import { DeliveredEquipementService } from './delivered-equipement.service';

@Injectable()
export class DeliveredEquipementPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private deliveredEquipementService: DeliveredEquipementService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.deliveredEquipementService.find(id).subscribe((deliveredEquipement) => {
                    deliveredEquipement.deliveryDate = this.datePipe
                        .transform(deliveredEquipement.deliveryDate, 'yyyy-MM-ddTHH:mm:ss');
                    deliveredEquipement.expirationDate = this.datePipe
                        .transform(deliveredEquipement.expirationDate, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.deliveredEquipementModalRef(component, deliveredEquipement);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.deliveredEquipementModalRef(component, new DeliveredEquipement());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    deliveredEquipementModalRef(component: Component, deliveredEquipement: DeliveredEquipement): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.deliveredEquipement = deliveredEquipement;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
