import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { DeliveredEquipement } from './delivered-equipement.model';
import { DeliveredEquipementPopupService } from './delivered-equipement-popup.service';
import { DeliveredEquipementService } from './delivered-equipement.service';

@Component({
    selector: 'jhi-delivered-equipement-delete-dialog',
    templateUrl: './delivered-equipement-delete-dialog.component.html'
})
export class DeliveredEquipementDeleteDialogComponent {

    deliveredEquipement: DeliveredEquipement;
    alerts = [];
    constructor(private alertService: JhiAlertService,
        private deliveredEquipementService: DeliveredEquipementService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    addErrorAlert(message, key?, data?) {
        key = (key && key !== null) ? key : message;
        this.alerts.push(
            this.alertService.addAlert(
                {
                    type: 'danger',
                    msg: key,
                    params: data,
                    timeout: 5000,
                    toast: this.alertService.isToast(),
                    scoped: true
                },
                this.alerts
            )
        );
    }

    confirmDelete(id: number) {
        this.deliveredEquipementService.delete(id).subscribe((response) => {
            if (response["status"] == 200){
                this.eventManager.broadcast({
                    name: 'deliveredEquipementListModification',
                    content: 'Deleted an deliveredEquipement'
                });
                this.activeModal.dismiss(true);
            }
            else{
                this.addErrorAlert('Delete not reachable', 'error.entity.delete.reachable');
            } 
        });
    }
}

@Component({
    selector: 'jhi-delivered-equipement-delete-popup',
    template: ''
})
export class DeliveredEquipementDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private deliveredEquipementPopupService: DeliveredEquipementPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.deliveredEquipementPopupService
                .open(DeliveredEquipementDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
