import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService, JhiLanguageService } from 'ng-jhipster';

import { DeliveredEquipement } from './delivered-equipement.model';
import { DeliveredEquipementService } from './delivered-equipement.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, PrintService, SpinnerService } from '../../shared';
import { NgxBarcodeComponent } from 'ngx-barcode';

@Component({
    selector: 'jhi-delivered-equipement',
    templateUrl: './delivered-equipement.component.html'
})
export class DeliveredEquipementComponent implements OnInit, OnDestroy {

    currentAccount: any;
    deliveredEquipements: DeliveredEquipement[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    annulled: boolean;
    fromDelivery: any;
    toDelivery: any;
    fromExpiration: any;
    toExpiration: any;
    status: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    showFilter = false;
    @ViewChild('ngxBarCode') ngxBarCode: NgxBarcodeComponent;
    barcodeValue: string;

    constructor(
        private languageService: JhiLanguageService,
        private deliveredEquipementService: DeliveredEquipementService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private printService: PrintService,
        private spinnerService: SpinnerService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = false;
            this.predicate = 'deliveryDate';
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }
    ngOnInit() {
        this.status = 'All';
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInDeliveredEquipements();
    }
    loadAll() {
        if (this.currentSearch || this.fromDelivery || this.toDelivery || this.annulled || this.status !== 'All') {
            this.advancedSearch();
            return;
        }
        this.deliveredEquipementService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res) => {this.onSuccess(res.body, res.headers)
            },
            (res) => this.onError(res.body)
        );
    }

    displayAdvanced() {
        this.showFilter = !this.showFilter;
    }
    search(query) {
        if (!query && (this.status === 'All') && !this.fromDelivery && !this.toDelivery && !this.annulled) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        const params: any = {
            search: this.currentSearch,
            fromDelivery: this.fromDelivery,
            toDelivery: this.toDelivery,
            annulled: this.annulled,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        };
        if (this.status !== 'All') {
            params['status'] = this.status;
        }
        this.router.navigate(['/delivered-equipement', params]);
        this.advancedSearch();
    }

    advancedSearch() {
        const params: any = {
            page: this.page - 1,
            query: this.currentSearch,
            fromDelivery: this.fromDelivery,
            toDelivery: this.toDelivery,
            fromExpiration: this.fromExpiration,
            toExpiration: this.toExpiration,
            annulled: this.annulled,
            size: this.itemsPerPage,
            sort: this.sort()
        };
        if (this.status !== 'All') {
            params['status'] = this.status;
        }
        this.deliveredEquipementService.search(params).subscribe(
            (res) => this.onSuccess(res.body, res.headers),
            (res) => this.onError(res.body)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/delivered-equipement'], {
            queryParams:
                {
                    page: this.page,
                    size: this.itemsPerPage,
                    search: this.currentSearch,
                    sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
                }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/delivered-equipement', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: DeliveredEquipement) {
        return item.id;
    }
    registerChangeInDeliveredEquipements() {
        this.eventSubscriber = this.eventManager.subscribe('deliveredEquipementListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        //this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        console.log("dzdzdzd"+data)
        // this.page = pagingParams.page;
        this.deliveredEquipements = data;
    }
    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

   async printBarCode(deliveredEquipement) {
        this.deliveredEquipementService.getPdf(deliveredEquipement.id,this.languageService.currentLang).subscribe(
            (value) => {
                const linkSource = 'data:application/pdf;base64,' + value["res"];
                const downloadLink = document.createElement("a");
                const fileName = "deTicket.pdf";

                downloadLink.href = linkSource;
                downloadLink.download = fileName;
                //window.open("data:application/pdf;base64, " + value["res"], '', "height=600,width=800");
                //downloadLink.click();
            if (downloadLink.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                //downloadLink.setAttribute("href", window.URL.createObjectURL(linkSource));
                downloadLink.setAttribute("download", fileName);
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
            } else {
                console.log('Pdf export only works in Chrome, Firefox, and Opera.');
                var fileBlob = new Blob([value['res']], {type: 'application/pdf'});
                window.navigator.msSaveBlob(fileBlob, fileName); 
            }
            },
            (error) => {
                
            }
        );
        /*this.spinnerService.show();
        const barcodeWindow = window.open('', '_blank', 'width=900,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no ,<style> @media print{ .toPrint{display:block; border:0; width:10%; height:50px} } </style>')
        let element = '';
        element += '<div class ="print_area" style="text-align: center;line-height: 1;">';
        element += '<p> <strong>' + deliveredEquipement.product.code + '</strong> </p>' +
         '<style type="text/css"> @media print { .noprint {display:none;}  @page { margin: 0; } body { margin: 16cm;} body * { visibility: hidden;} .print_area, .print_area * { visibility: visible; } .print_area { position: absolute; left: 0;top: 0;} } </style>'+
         '<style> @media print{ .toPrint{display:block; border:0; width:10%; height:50px} } </style>';
        this.barcodeValue = deliveredEquipement.code;
        element = await this.appendBarCode(element, this.ngxBarCode);
        element += '</div>';
        this.spinnerService.hide();
        setTimeout(() => {
            this.printService.print(element, barcodeWindow);
        });*/
        //this.printDiv(element);
    }
    printDiv(element){
        //var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = element;
        window.print();
        document.body.innerHTML = originalContents;
    }
    async appendBarCode(element, ngx: NgxBarcodeComponent) {
        await this.delay(20);
        element += ngx.bcElement.nativeElement.innerHTML;
        return element;
    }

    delay(ms: number) {
        return new Promise((resolve) => setTimeout(resolve, ms));
    }
}
