import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { DeliveredEquipement } from './delivered-equipement.model';
import { createRequestOption } from '../../shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { JhiDateUtils } from 'ng-jhipster';
import { KitNormalisation } from '../kit/kit-normalisation.model';

@Injectable()
export class DeliveredEquipementService {

    private resourceUrl = SERVER_API_URL + 'api/delivered-equipements';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/delivered-equipements';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(deliveredEquipement: DeliveredEquipement, kit: boolean, kitNormalisations?: KitNormalisation[]): Observable<DeliveredEquipement> {
        this.convertDate(deliveredEquipement);
        if (kit) {
            return this.http.post<DeliveredEquipement>(`${this.resourceUrl}/belongsToKit`, { deliveredEquipement, kitNormalisations });
        } else {
            return this.http.post<DeliveredEquipement>(this.resourceUrl, deliveredEquipement);
        }
    }

    update(deliveredEquipement: DeliveredEquipement): Observable<DeliveredEquipement> {
        this.convertDate(deliveredEquipement);
        return this.http.put<DeliveredEquipement>(this.resourceUrl, deliveredEquipement);
    }

    find(id: number): Observable<DeliveredEquipement> {
        return this.http.get<DeliveredEquipement>(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<HttpResponse<DeliveredEquipement[]>> {
        const options = createRequestOption(req);
        return this.http.get<DeliveredEquipement[]>(this.resourceUrl, { params: options, observe: 'response' });
    }
    getPdf(id:number,lang:string): Observable<any> {
        //const options = createRequestOption(req);
        return this.http.get(this.resourceUrl+"/"+id+"/pdf?lang="+lang);
    }
    checkCode(code: string): Observable<DeliveredEquipement> {
        return this.http.get<DeliveredEquipement>(this.resourceUrl+"/checkcode/?code="+code);
    }
    getDeliveredQuantityMaxValue(listProductCommand: any): Observable<HttpResponse<number>> {
        return this.http.post<number>(`${this.resourceUrl}/getDeliveredQuantityMaxValue`, listProductCommand, { observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response',responseType: 'text' });
    }

    search(req?: any): Observable<HttpResponse<DeliveredEquipement[]>> {
        const options = this.createAdvancedSearchOption(req);
        return this.http.get<DeliveredEquipement[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }

    createAdvancedSearchOption = (req?: any): HttpParams => {
        let Params = new HttpParams();
        if (req) {
            // Begin assigning parameters
            if (req.page) {
                Params = Params.append('page', req.page)
            }
            if (req.size) {
                Params = Params.append('size', req.size)
            }
            if (req.query) {
                Params = Params.append('q', req.query)
            }
            if (req.filter) {
                Params = Params.append('filter', req.filter)
            }
            if (req.sort) {
                for (const s of req.sort) {
                    Params = Params.append('sort', s);
                }
            }
            if (req.status) {
                Params = Params.append('status', req.status)
            }
            if (req.fromDelivery) {
                Params = Params.append('fromDel', req.fromDelivery)
            }
            if (req.toDelivery) {
                Params = Params.append('toDel', req.toDelivery)
            }
            if (req.fromExpiration) {
                Params = Params.append('fromEx', req.supplier)
            }
            if (req.toExpiration) {
                Params = Params.append('toEx', req.toExpiration)
            }
            if (req.annulled) {
                Params = Params.append('annulled', req.annulled)
            }
        }
        return Params;
    };

    convertDate(deliveredEquipement: DeliveredEquipement) {
        deliveredEquipement.deliveryDate = this.dateUtils.toDate(deliveredEquipement.deliveryDate);
        deliveredEquipement.expirationDate = this.dateUtils.toDate(deliveredEquipement.expirationDate);
    }
}
