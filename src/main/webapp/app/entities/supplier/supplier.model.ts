import { BaseEntity } from './../../shared';

export class Supplier implements BaseEntity {
    constructor(
        public id?: number,
        public companyName?: string,
        public phoneNumber?: string,
        public email?: string,
        public discount?: number,
        public transportationCharge?: number,
        public discountRequirement?: number,
        public billingRequirement?: string,
        public reference?: string,
        public startValidation?: any,
        public endValidation?: any,
        public tvaNumber?: string,
        public tvaCode?: any,
        public payment?: any,
        public address?: any,
        public df?:any,
        public header?:string
    ) {
    }
}
