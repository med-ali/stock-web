import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Supplier } from './supplier.model';
import { SupplierService } from './supplier.service';

@Injectable()
export class SupplierPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private supplierService: SupplierService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.supplierService.find(id).subscribe((supplier) => {
                    supplier.startValidation = this.datePipe
                        .transform(supplier.startValidation, 'yyyy-MM-ddTHH:mm:ss');
                    supplier.endValidation = this.datePipe
                        .transform(supplier.endValidation, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.supplierModalRef(component, supplier);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const supplier: Supplier = new Supplier();
                    supplier.startValidation = this.datePipe
                        .transform(new Date(), 'yyyy-MM-ddTHH:mm:ss');
                    supplier.endValidation = this.datePipe
                        .transform('2028-12-31T23:59:59+01:00', 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.supplierModalRef(component, supplier);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    supplierModalRef(component: Component, supplier: Supplier): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.supplier = supplier;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
