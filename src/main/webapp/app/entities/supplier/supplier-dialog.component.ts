import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Supplier } from './supplier.model';
import { SupplierPopupService } from './supplier-popup.service';
import { SupplierService } from './supplier.service';
import { TvaCode, TvaCodeService } from '../tva-code';
import { Payment, PaymentService } from '../payment';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-supplier-dialog',
    templateUrl: './supplier-dialog.component.html'
})
export class SupplierDialogComponent implements OnInit {

    supplier: Supplier;
    isSaving: boolean;
    paymentModal: boolean;
    tvaCodeModal: boolean;
    step= 1;
    descriptionCode:string;
    descriptionTvaCode:string;
    existEmail:boolean;
    existPhone:boolean;
    isDefault:any;
    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private supplierService: SupplierService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.paymentModal = this.tvaCodeModal = false;
        
        if (this.supplier.address === undefined) {
             
            this.supplier.address = {};
        }
        if (this.supplier.df == false) {
            this.isDefault = 'no';    
        }
        else{
            this.isDefault = 'yes';
        }
        
        if(this.supplier.payment){
            this.descriptionCode = this.supplier.payment.code + "-" + this.supplier.payment.description;
        }
        if(this.supplier.tvaCode){
            this.descriptionTvaCode = this.supplier.tvaCode.code + "-" + this.supplier.tvaCode.description;
        }  
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        //this.isSaving = true;
        this.supplierService.checkEmail(this.supplier.email).subscribe((value) => { 
           
            if (value==true && this.supplier.id == undefined){
                this.existEmail = true;   
            }
            else{
                this.existEmail = false;
                //this.clear()             
                this.supplierService.checkPhone(this.supplier.phoneNumber).subscribe((value) => { 
                    
                    if (value==true && this.supplier.id == undefined){
                        this.existPhone = true;
                    }
                    else{
                        this.existPhone = false;
                        if (this.supplier.id !== undefined) {
                            this.subscribeToSaveResponse(
                                this.supplierService.update(this.supplier));
                        } else {
                            this.subscribeToSaveResponse(
                                this.supplierService.create(this.supplier));
                        }
                        this.isSaving = true;
                        this.clear()
                    }
                    },
                    (error) => {  
                })
            }
            },
            (error) => {  
        })
    }
      
    public defaultChange(event) {
        if (event === 'yes') {
            this.supplier.df = true;
        } else if (event === 'no') {
            this.supplier.df = false;
        }
    }

    onNullTvaCode(){
        this.supplier.tvaCode =  null
        this.descriptionTvaCode = ""
    }
    onNullPayment(){
        this.supplier.payment =  null
        this.descriptionCode = ""
    }
    private subscribeToSaveResponse(result: Observable<Supplier>) {
        result.subscribe((res: Supplier) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Supplier) {
        this.eventManager.broadcast({ name: 'supplierListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    openMyModal(event) {
        if (event === 'paymentModal') {
            this.paymentModal = true;
        } else if (event === 'tvaCodeModal') {
            this.tvaCodeModal = true;
        }
        document.querySelector('#' + event).classList.add('md-show');
    }

    closeModal(event) {
        this.paymentModal = this.tvaCodeModal = false;
        (document.querySelector('#' + event).parentElement.parentElement).classList.remove('md-show');
    }

    closeMyModalEvent(event) {
        ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
    }

    assignTvaCode(tvaCode) {
        this.supplier.tvaCode = tvaCode;
        this.descriptionTvaCode = this.supplier.tvaCode.code + "-" + this.supplier.tvaCode.description;
        this.closeModal('closeTvaCodeBtn');
    }

    assignPayment(payment) {
        this.supplier.payment = payment;
        this.descriptionCode = this.supplier.payment.code + "-" + this.supplier.payment.description;
        this.closeModal('closePaymentBtn');
    }

}

@Component({
    selector: 'jhi-supplier-popup',
    template: ''
})
export class SupplierPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private supplierPopupService: SupplierPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.supplierPopupService
                    .open(SupplierDialogComponent as Component, params['id']);
            } else {
                this.supplierPopupService
                    .open(SupplierDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
