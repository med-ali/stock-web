import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router'; 
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { Supplier } from './supplier.model';
import { SupplierPopupService } from './supplier-popup.service';
import { SupplierService } from './supplier.service';

@Component({
    selector: 'jhi-supplier-delete-dialog',
    templateUrl: './supplier-delete-dialog.component.html'
})
export class SupplierDeleteDialogComponent {

    supplier: Supplier;
    alerts = [];
    constructor(private alertService: JhiAlertService,
        private supplierService: SupplierService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }
    addErrorAlert(message, key?, data?) {
        key = (key && key !== null) ? key : message;
        this.alerts.push(
            this.alertService.addAlert(
                {
                    type: 'danger',
                    msg: key,
                    params: data,
                    timeout: 5000,
                    toast: this.alertService.isToast(),
                    scoped: true
                },
                this.alerts
            )
        );
    }
    confirmDelete(id: number) {
        this.supplierService.delete(id).subscribe((response) => {
            
            if (response["status"] == 200){
                this.eventManager.broadcast({
                    name: 'supplierListModification',
                    content: 'Deleted an supplier'
                });
                this.activeModal.dismiss(true);
            }
            else{
                this.addErrorAlert('Delete not reachable', 'error.entity.delete.reachable');
                
            } 
        });
    }
}

@Component({
    selector: 'jhi-supplier-delete-popup',
    template: ''
})
export class SupplierDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private supplierPopupService: SupplierPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.supplierPopupService
                .open(SupplierDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
