import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Supplier } from './supplier.model';
import { createRequestOption } from '../../shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { JhiDateUtils } from 'ng-jhipster';

@Injectable()
export class SupplierService {

    private resourceUrl = SERVER_API_URL + 'api/suppliers';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/suppliers';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(supplier: Supplier): Observable<Supplier> {
        this.convertDate(supplier);
        return this.http.post<Supplier>(this.resourceUrl, supplier);
    } 
    checkEmail(email: string): Observable<Supplier> {
        return this.http.get<Supplier>(this.resourceUrl+"/checkemail/?email="+email);
    }
    checkPhone(phone: string): Observable<Supplier> {
        return this.http.get<Supplier>(this.resourceUrl+"/checkphone/?phone="+phone);
    }
    update(supplier: Supplier): Observable<Supplier> {
        this.convertDate(supplier);
        return this.http.put<Supplier>(this.resourceUrl, supplier);
    }

    find(id: number): Observable<Supplier> {
        return this.http.get<Supplier>(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<HttpResponse<Supplier[]>> {
        const options = createRequestOption(req);
        return this.http.get<Supplier[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<HttpResponse<Supplier[]>> {
        const options = createRequestOption(req);
        return this.http.get<Supplier[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }

    convertDate(supplier: Supplier) {
        supplier.startValidation = this.dateUtils.toDate(supplier.startValidation);
        supplier.endValidation = this.dateUtils.toDate(supplier.endValidation);
    }
}
