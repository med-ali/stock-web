import {
    SupplierService,
    SupplierPopupService,
    SupplierComponent,
    SupplierDetailComponent,
    SupplierDialogComponent,
    SupplierPopupComponent,
    SupplierDeletePopupComponent,
    SupplierDeleteDialogComponent,
    supplierRoute,
    supplierPopupRoute,
    SupplierResolvePagingParams,
} from './';

export const supplier_declarations = [
    SupplierComponent,
    SupplierDetailComponent,
    SupplierDialogComponent,
    SupplierDeleteDialogComponent,
    SupplierPopupComponent,
    SupplierDeletePopupComponent,
];

export const supplier_entryComponents = [
    SupplierComponent,
    SupplierDialogComponent,
    SupplierPopupComponent,
    SupplierDeleteDialogComponent,
    SupplierDeletePopupComponent,
];

export const supplier_providers = [
    SupplierService,
    SupplierPopupService,
    SupplierResolvePagingParams,
];
