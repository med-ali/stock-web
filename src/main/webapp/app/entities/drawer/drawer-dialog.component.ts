    import { Component, OnInit, OnDestroy } from '@angular/core';
    import { ActivatedRoute } from '@angular/router';
    import { Response } from '@angular/http';

    import { Observable } from 'rxjs/Rx';
    import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
    import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

    import { Drawer } from './drawer.model';
    import { DrawerPopupService } from './drawer-popup.service';
    import { DrawerService } from './drawer.service';
    import { ShelvingUnit, ShelvingUnitService } from '../shelving-unit';
    import { ResponseWrapper } from '../../shared';
    import { Shelf } from '../shelf';

    @Component({
        selector: 'jhi-drawer-dialog',
        templateUrl: './drawer-dialog.component.html'
    })
    export class DrawerDialogComponent implements OnInit {

        drawer: Drawer;
        isSaving: boolean;
        active: any;
        descriptionShelf:string;
        shelfs: Shelf[];
        shelfModal: boolean;
        existCode:boolean;

        constructor(
            public activeModal: NgbActiveModal,
            private jhiAlertService: JhiAlertService,
            private drawerService: DrawerService,
            private shelvingUnitService: ShelvingUnitService,
            private eventManager: JhiEventManager
        ) {
        }

        ngOnInit() {
            this.isSaving = false;
            this.active = 'yes';
            this.drawer.isActive = true;
            this.shelfModal = false;
            if(this.drawer.shelf){
                this.descriptionShelf = this.drawer.shelf.code + "-" + this.drawer.shelf.description;
            }
        }

        clear() {
            this.activeModal.dismiss('cancel');
        }

        public activeChange(event:String) {
            console.log(event)
            if (event === 'yes') {
                this.drawer.isActive = true;
            } else if (event === 'no') {
                this.drawer.isActive = false;
            }
            console.log('up' + this.drawer.isActive)
        }

        save() {
            this.drawerService.checkCode(this.drawer.code).subscribe((value) => { 
                console.log(value) 
                if (value==true && this.drawer.id == undefined){
                    this.existCode = true;
                }
                else{
                    this.existCode = false;
                    this.isSaving = true;
                    if (this.drawer.id !== undefined) {
                        this.subscribeToSaveResponse(
                            this.drawerService.update(this.drawer));
                    } else {
                        this.subscribeToSaveResponse(
                            this.drawerService.create(this.drawer));
                    }
                    this.clear()
                }
                },
                (error) => { 
                console.log("error"+error)
            })
        }

        private subscribeToSaveResponse(result: Observable<Shelf>) {
            result.subscribe((res: Shelf) =>
                this.onSaveSuccess(res), (res: Response) => this.onSaveError());
        }

        private onSaveSuccess(result: Shelf) {
            this.eventManager.broadcast({ name: 'drawerListModification', content: 'OK' });
            this.isSaving = false;
            this.activeModal.dismiss(result);
        }

        private onSaveError() {
            this.isSaving = false;
        }

        private onError(error: any) {
            this.jhiAlertService.error(error.message, null, null);
        }

        trackShelvingUnitById(index: number, item: ShelvingUnit) {
            return item.id;
        }

        openMyModal(event) {
            if (event === 'shelfModal') {
                this.shelfModal = true;
            }
            document.querySelector('#' + event).classList.add('md-show');
        }

        closeModal(event) {
            this.shelfModal = false;
            (document.querySelector('#' + event).parentElement.parentElement).classList.remove('md-show');
        }

        closeMyModalEvent(event) {
            ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
        }

        assignShelf(shelf) {
            this.drawer.shelf = shelf;
            this.descriptionShelf = this.drawer.shelf.code + "-" + this.drawer.shelf.description;
            this.closeModal('closeShelfBtn');
        }
    }

    @Component({
        selector: 'jhi-drawer-popup',
        template: ''
    })
    export class DrawerPopupComponent implements OnInit, OnDestroy {

        routeSub: any;

        constructor(
            private route: ActivatedRoute,
            private drawerPopupService: DrawerPopupService
        ) { }

        ngOnInit() {
            this.routeSub = this.route.params.subscribe((params) => {
                if (params['id']) {
                    this.drawerPopupService
                        .open(DrawerDialogComponent as Component, params['id']);
                } else {
                    this.drawerPopupService
                        .open(DrawerDialogComponent as Component);
                }
            });
        }

        ngOnDestroy() {
            this.routeSub.unsubscribe();
        }
    }
