import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Drawer } from './drawer.model';
import { DrawerService } from './drawer.service';

@Component({
    selector: 'jhi-drawer-detail',
    templateUrl: './drawer-detail.component.html'
})
export class DrawerDetailComponent implements OnInit, OnDestroy {

    drawer: Drawer;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private drawerService: DrawerService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDrawers();
    }

    load(id) {
        this.drawerService.find(id).subscribe((d) => {
            this.drawer = d;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDrawers() {
        this.eventSubscriber = this.eventManager.subscribe(
            'drawerListModification',
            (response) => this.load(this.drawer.id)
        );
    }
}
