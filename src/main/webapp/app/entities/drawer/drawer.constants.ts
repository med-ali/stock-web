import {
    DrawerService,
    DrawerPopupService,
    DrawerComponent,
    DrawerDetailComponent,
    DrawerDialogComponent,
    DrawerPopupComponent,
    DrawerDeletePopupComponent,
    DrawerDeleteDialogComponent,
    drawerRoute,
    drawerPopupRoute,
    DrawerResolvePagingParams,
    SlotDrawerComponent,
} from './';

export const drawer_declarations = [
    DrawerComponent,
    DrawerDetailComponent,
    DrawerDialogComponent,
    DrawerDeleteDialogComponent,
    DrawerPopupComponent,
    DrawerDeletePopupComponent,
    SlotDrawerComponent
];

export const drawer_entryComponents = [
    DrawerComponent,
    DrawerDialogComponent,
    DrawerPopupComponent,
    DrawerDeleteDialogComponent,
    DrawerDeletePopupComponent,
];

export const drawer_providers = [
    DrawerService,
    DrawerPopupService,
    DrawerResolvePagingParams,
];
