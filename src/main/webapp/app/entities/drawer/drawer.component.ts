import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { Drawer } from './drawer.model';
import { DrawerService } from './drawer.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, PrintService, SpinnerService } from '../../shared';
import { NgxBarcodeComponent } from 'ngx-barcode';

@Component({
    selector: 'jhi-drawer',
    templateUrl: './drawer.component.html'
})
export class DrawerComponent implements OnInit, OnDestroy {

    currentAccount: any;
    drawers: Drawer[];
    selectedDrawers: Drawer[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    checkall: boolean;
    @ViewChild('ngxBarCode') ngxBarCode: NgxBarcodeComponent;
    barcodeValue: string;

    constructor(
        private drawerService: DrawerService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private printService: PrintService,
        private spinnerService: SpinnerService,
        private eventManager: JhiEventManager
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        this.checkall = false;
        if (this.currentSearch) {
            this.drawerService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res) => this.onSuccess(res.body, res.headers),
                (res) => this.onError(res.body)
            );
            return;
        }
        this.drawerService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res) => this.onSuccess(res.body, res.headers),
            (res) => this.onError(res.body)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    previousState() {
        window.history.back();
    }
    transition() {
        this.router.navigate(['/drawer'], {
            queryParams:
                {
                    page: this.page,
                    size: this.itemsPerPage,
                    search: this.currentSearch,
                    sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
                }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/drawer', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/drawer', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.selectedDrawers = [];
        this.checkall = false;
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInShelves();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Drawer) {
        return item.id;
    }
    registerChangeInShelves() {
        this.eventSubscriber = this.eventManager.subscribe('drawerListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        //this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.drawers = data;
    }
    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    selectOrDeselectAllShelves() {
        this.checkall = !this.checkall;
        if (this.checkall) {
            this.selectedDrawers = this.drawers;
            for (let i = 0; i < this.drawers.length; i++) {
                this.drawers[i].checked = true;
            }
        } else {
            this.selectedDrawers= [];
            for (let i = 0; i < this.drawers.length; i++) {
                this.drawers[i].checked = false;
            }
        }
    }

    updateSelectedShelves(shelf) {
        if (!shelf.checked) {
            this.selectedDrawers.splice(this.renderIndex(shelf, this.selectedDrawers), 1);
        } else {
            this.selectedDrawers.push(shelf);
        }
        if (this.selectedDrawers.length === this.drawers.length) {
            this.checkall = true;
        } else {
            this.checkall = false;
        }
    }

    renderIndex(shelf, shelves: Drawer[]): number {
        for (let i = 0; i < shelves.length; i++) {
            const currentShelf = shelves[i];
            if (currentShelf.id === shelf.id) {
                return i;
            }
        }
        return -1;
    }

    printOneBarCode(shelf) {
        const barcodeWindow = window.open('', '_blank', 'width=900,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        this.barcodeValue = shelf.barCode;
        setTimeout(() => {
            this.printService.printBarcode(this.ngxBarCode.bcElement.nativeElement.innerHTML, barcodeWindow);
        });
    }

    async printMultiBarCode() {
        this.spinnerService.show();
        const barcodeWindow = window.open('', '_blank', 'width=900,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no')
        let element = '';
        let shelfToPrint;
        for (let i = 0; i < this.selectedDrawers.length; i++) {
            shelfToPrint = this.selectedDrawers[i];
            element += '<div style="text-align: center;line-height: 1;">';
            this.barcodeValue = shelfToPrint.barCode;
            element = await this.appendBarCode(element, this.ngxBarCode);
            element += '</div>';
            if (i < this.selectedDrawers.length - 1) {
                element += '<div class="page-break"></div>';
            }
        }
        this.spinnerService.hide();
        setTimeout(() => {
            this.printService.printBarcode(element, barcodeWindow);
        });
    }

    async appendBarCode(element, ngx: NgxBarcodeComponent) {
        await this.delay(20);
        element += ngx.bcElement.nativeElement.innerHTML;
        return element;
    }

    delay(ms: number) {
        return new Promise((resolve) => setTimeout(resolve, ms));
    }
}
