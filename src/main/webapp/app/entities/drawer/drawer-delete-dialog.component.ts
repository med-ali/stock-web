import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { Drawer } from './drawer.model';
import { DrawerPopupService } from './drawer-popup.service';
import { DrawerService } from './drawer.service';

@Component({
    selector: 'jhi-drawer-delete-dialog',
    templateUrl: './drawer-delete-dialog.component.html'
})
export class DrawerDeleteDialogComponent {

    drawer: Drawer;
    alerts = [];
    constructor(private alertService: JhiAlertService,
        private drawerService: DrawerService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    addErrorAlert(message, key?, data?) {
        key = (key && key !== null) ? key : message;
        this.alerts.push(
            this.alertService.addAlert(
                {
                    type: 'danger',
                    msg: key,
                    params: data,
                    timeout: 5000,
                    toast: this.alertService.isToast(),
                    scoped: true
                },
                this.alerts
            )
        );
    }

    confirmDelete(id: number) {
        this.drawerService.delete(id).subscribe((response) => {
            if (response["status"] == 200){
                this.eventManager.broadcast({
                    name: 'drawerListModification',
                    content: 'Deleted an drawer'
                });
                this.activeModal.dismiss(true);
            }
            else{
                this.addErrorAlert('Delete not reachable', 'error.entity.delete.reachable');
            } 
        });
    }
}

@Component({
    selector: 'jhi-drawer-delete-popup',
    template: ''
})
export class DrawerDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private drawerPopupService: DrawerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.drawerPopupService
                .open(DrawerDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
