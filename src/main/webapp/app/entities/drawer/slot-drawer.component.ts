import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { SlotService } from '../slot';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, PrintService, SpinnerService } from '../../shared';
import { NgxBarcodeComponent } from 'ngx-barcode';
import { Slot } from '../slot';

@Component({
    selector: 'jhi-slot-drawer',
    templateUrl: './slot-drawer.component.html'
})
export class SlotDrawerComponent implements OnInit, OnDestroy {

    currentAccount: any;
    slots: any[]; 
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    checkall: boolean;
    subscription: Subscription;
    state:any;
    @ViewChild('ngxBarCode') ngxBarCode: NgxBarcodeComponent;
    barcodeValue: string;
    drawerId: number;

    constructor(
        private slotService: SlotService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private printService: PrintService,
        private spinnerService: SpinnerService,
        private eventManager: JhiEventManager,
        private route: ActivatedRoute
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        console.log("2222222222222222222"+this.state)
        this.checkall = false;
        if (this.currentSearch) {
            this.slotService.searchSlotsByDrawer(this.drawerId,{
                state:this.state,
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res) => this.onSuccess(res.body, res.headers),
                (res) => this.onError(res.body)
            );
            return;
        }
        this.slotService.querySlotsByDrawer(this.drawerId,{
            state:this.state,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res) => this.onSuccess(res.body, res.headers),
            (res) => this.onError(res.body)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/drawer/slots/',this.drawerId], {
            queryParams:
                {
                    state:"All",
                    page: this.page,
                    size: this.itemsPerPage,
                    search: this.currentSearch,
                    sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
                }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/drawer/slots/',this.drawerId, {
            state:this.state,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        console.log('ddddddddddddddddd'+this.state)
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/drawer/slots/',this.drawerId, {
            
            state:this.state,
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.state= "All"
        this.subscription = this.route.params.subscribe((params) => {
            this.drawerId = params['id'];
        });
        this.loadAll();
        this.checkall = false;
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        }); 
        this.registerChangeInSlotsDawers();
    }
    registerChangeInSlotsDawers() {
        this.eventSubscriber = this.eventManager.subscribe('slotForDrawerListModification', (response) => this.loadAll());
    }
    ngOnDestroy() {
        //this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Slot) {
        return item.id;
    }
    previousState() {
        
        window.history.back();
    }
    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        //this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.slots = data;
    }
    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
    renderIndex(slot, slots: Slot[]): number {
        for (let i = 0; i < slots.length; i++) {
            const currentSlot = slots[i];
            if (currentSlot.id === slot.id) {
                return i;
            }
        }
        return -1;
    }

}
