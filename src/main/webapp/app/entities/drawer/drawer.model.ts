import { BaseEntity } from '../../shared';

export class Drawer implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public name?: string,
        public description?: string,
        public barCode?: string,
        public isActive?: boolean,
        public shelf?: any,
        public checked?: boolean,
    ) {
        this.isActive = true;
    }
}
