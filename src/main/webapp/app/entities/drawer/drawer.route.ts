import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { DrawerComponent } from './drawer.component';
import { DrawerDetailComponent } from './drawer-detail.component';
import { DrawerPopupComponent } from './drawer-dialog.component';
import { DrawerDeletePopupComponent } from './drawer-delete-dialog.component'; 
import { SlotDrawerComponent } from './slot-drawer.component';

@Injectable()
export class DrawerResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const drawerRoute: Routes = [
    {
        path: 'drawer',
        component: DrawerComponent,
        resolve: {
            'pagingParams': DrawerResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.drawer.default'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'drawer/:id',
        component: DrawerDetailComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.drawer.default'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'drawer/slots/:id',
        component: SlotDrawerComponent,
        resolve: {
            'pagingParams': DrawerResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.drawer.default'
        },
        canActivate: [UserRouteAccessService]
    },
];

export const drawerPopupRoute: Routes = [
    {
        path: 'drawer-new',
        component: DrawerPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.drawer.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'drawer/:id/edit',
        component: DrawerPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.drawer.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'drawer/:id/delete',
        component: DrawerDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.drawer.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
