import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Drawer } from './drawer.model';
import { createRequestOption } from '../../shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';

@Injectable()
export class DrawerService {

    private resourceUrl = SERVER_API_URL + 'api/drawers';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/drawers';

    constructor(private http: HttpClient) { }

    create(d: Drawer): Observable<Drawer> {
        return this.http.post<Drawer>(this.resourceUrl, d);
    }

    checkCode(code: string): Observable<Drawer> {
        return this.http.get<Drawer>(this.resourceUrl+"/checkcode/?code="+code);
    }

    update(d: Drawer): Observable<Drawer> {
        return this.http.put<Drawer>(this.resourceUrl, d);
    }

    find(id: number): Observable<Drawer> {
        return this.http.get<Drawer>(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<HttpResponse<Drawer[]>> {
        const options = createRequestOption(req);
        return this.http.get<Drawer[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    queryContainsProduct(id: number, req?: any) {
        const options = createRequestOption(req);
        return this.http.get<Drawer[]>(`${this.resourceUrl}/product/${id}`, { params: options, observe: 'response' });
    }
 

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<HttpResponse<Drawer[]>> {
        const options = createRequestOption(req);
        return this.http.get<Drawer[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }
  
     
}
