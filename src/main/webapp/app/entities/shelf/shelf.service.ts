import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Shelf } from './shelf.model';
import { createRequestOption } from '../../shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';

@Injectable()
export class ShelfService {

    private resourceUrl = SERVER_API_URL + 'api/shelves';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/shelves';

    constructor(private http: HttpClient) { }

    create(shelf: Shelf): Observable<Shelf> {
        return this.http.post<Shelf>(this.resourceUrl, shelf);
    }

    checkCode(code: string): Observable<Shelf> {
        return this.http.get<Shelf>(this.resourceUrl+"/checkcode/?code="+code);
    }

    update(shelf: Shelf): Observable<Shelf> {
        return this.http.put<Shelf>(this.resourceUrl, shelf);
    }

    find(id: number): Observable<Shelf> {
        return this.http.get<Shelf>(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<HttpResponse<Shelf[]>> {
        const options = createRequestOption(req);
        return this.http.get<Shelf[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    queryContainsProduct(id: number, req?: any) {
        const options = createRequestOption(req);
        return this.http.get<Shelf[]>(`${this.resourceUrl}/product/${id}`, { params: options, observe: 'response' });
    }

    /*queryContainsProductByShelfUnit(id: number) {
        return this.http.get<Shelf[]>(`${this.resourceUrl}/inventory/31`, { observe: 'response' });
    }*/

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<HttpResponse<Shelf[]>> {
        const options = createRequestOption(req);
        return this.http.get<Shelf[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }
  
    loadShelfsPdf(shelfId:number): Observable<any> {
        return this.http.get<any>(`${this.resourceUrl}/${shelfId}/pdf`);
    }
}
