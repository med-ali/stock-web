import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { Shelf } from './shelf.model';
import { ShelfService } from './shelf.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { Product, ProductService } from '../product';
import { MainService } from '../../shared/utils/main.service';
@Component({
    selector: 'jhi-shelf-contains-products',
    templateUrl: './shelf-contains-products.component.html'
})
export class ShelfContainsProductsComponent implements OnInit, OnDestroy {

    currentAccount: any;
    shelves: any[];
    productId: number;
    product: Product;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    selectedRow: any;
    showDetail : boolean[];
    oldSelectedRow: any;
    j:number;
    jOld:number;
    constructor(
        private mainService: MainService,
        private shelfService: ShelfService,
        private productService: ProductService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
    }
    displayNestedList(item,i) {
        this.shelves[i].listScP.forEach(element => {
            //element.expirationDate= this.mainService.convertDateForInputDate(element.expirationDate)
            if(element.expirationDate){
                element.expirationDate =element.expirationDate.split("T")[0];
            }         
        });
        if (this.jOld == i){
            this.showDetail[i] = !this.showDetail[i];
        }
        else{
            if (this.selectedRow && this.j){
                this.oldSelectedRow = this.selectedRow
                this.jOld = this.j;
            }
            else {
                this.oldSelectedRow = this.shelves[0];
                this.jOld = 0;
            }   
            this.selectedRow = this.shelves[i];
            this.j = i;
            let index = this.shelves.indexOf(this.oldSelectedRow);
            this.showDetail[index] = false;
            this.oldSelectedRow = this.selectedRow;
            this.selectedRow = item;
            if (this.oldSelectedRow.id === this.selectedRow.id) {
                this.showDetail[i] = !this.showDetail[i];
                //this.falseFct(i);
            } else {
                this.showDetail[i] = true;
                //this.falseFct(i);
            }
            this.showDetail[this.jOld] = false;
        }
        
         
    }
    loadContainsProduct() {
        this.shelfService.queryContainsProduct(this.productId, {
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res) => this.onSuccess(res.body, res.headers),
            (res) => this.onError(res.body)
        );
        /*this.shelfService.queryContainsProductByShelfUnit(this.productId).subscribe(
             
            (res) => {this.onSuccess(res.body, res.headers)
            console.log("helo1" + res )
            },
            (res) => {this.onError(res.body)
                console.log("helo2" + res )
            }
        );*/
    }
    loadShelfsPdf(shelfId:number) {
        console.log("data :"+shelfId)
         
        this.shelfService.loadShelfsPdf(shelfId).subscribe((value) => {
            const linkSource = 'data:application/pdf;base64,' + value["res"];
            const downloadLink = document.createElement("a");
            const fileName = "shelfCodeBar.pdf";
            downloadLink.href = linkSource;
            downloadLink.download = fileName;
            // window.open("data:application/pdf;base64, " + value["res"], '', "height=600,width=800");
            //downloadLink.click();
            if (downloadLink.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                //downloadLink.setAttribute("href", window.URL.createObjectURL(linkSource));
                downloadLink.setAttribute("download", fileName);
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
            } else {
                console.log('Pdf export only works in Chrome, Firefox, and Opera.');
                var fileBlob = new Blob([value['res']], {type: 'application/pdf'});
                window.navigator.msSaveBlob(fileBlob, fileName); 
            }
        });
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/shelf/product/' + this.productId], {
            queryParams:
                {
                    page: this.page,
                    size: this.itemsPerPage,
                    sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
                }
        });
        this.loadContainsProduct();
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/shelf/product/' + this.productId, {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadContainsProduct();
    }
    ngOnInit() {
        this.showDetail = []
        this.eventSubscriber = this.activatedRoute.params.subscribe((params) => {
            this.productId = params['productId'];
            this.productService.find(this.productId).subscribe((product) => {
                this.product = product;
            });
            this.loadContainsProduct();
        }); 
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInShelves();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Shelf) {
        return item.id;
    }
    registerChangeInShelves() {
        this.eventSubscriber = this.eventManager.subscribe('shelfListModification',
            (response) => this.loadContainsProduct());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        //this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.shelves = data;
        this.shelves.forEach(element => {
            //element.expirationDate= this.mainService.convertDateForInputDate(element.expirationDate)
            if(element.expirationDate){
                element.expirationDate =element.expirationDate.split("T")[0];
            }         
        });
    }
    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
