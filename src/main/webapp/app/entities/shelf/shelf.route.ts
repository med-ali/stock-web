import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ShelfComponent } from './shelf.component';
import { ShelfDetailComponent } from './shelf-detail.component';
import { ShelfPopupComponent } from './shelf-dialog.component';
import { ShelfDeletePopupComponent } from './shelf-delete-dialog.component';
import { ShelfContainsProductsComponent } from './shelf-contains-products.component';

@Injectable()
export class ShelfResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const shelfRoute: Routes = [
    {
        path: 'shelf',
        component: ShelfComponent,
        resolve: {
            'pagingParams': ShelfResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.shelf.default'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'shelf/:id',
        component: ShelfDetailComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.shelf.default'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'product/shelf/:productId',
        component: ShelfContainsProductsComponent,
        resolve: { 
            'pagingParams': ShelfResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.shelf.default'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const shelfPopupRoute: Routes = [
    {
        path: 'shelf-new',
        component: ShelfPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.shelf.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'shelf/:id/edit',
        component: ShelfPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.shelf.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'shelf/:id/delete',
        component: ShelfDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.shelf.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
