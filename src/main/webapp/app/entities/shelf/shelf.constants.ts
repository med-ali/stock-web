import {
    ShelfService,
    ShelfPopupService,
    ShelfComponent,
    ShelfContainsProductsComponent,
    ShelfDetailComponent,
    ShelfDialogComponent,
    ShelfPopupComponent,
    ShelfDeletePopupComponent,
    ShelfDeleteDialogComponent,
    shelfRoute,
    shelfPopupRoute,
    ShelfResolvePagingParams,
} from './';

export const shelf_declarations = [
    ShelfComponent,
    ShelfContainsProductsComponent,
    ShelfDetailComponent,
    ShelfDialogComponent,
    ShelfDeleteDialogComponent,
    ShelfPopupComponent,
    ShelfDeletePopupComponent,
];

export const shelf_entryComponents = [
    ShelfComponent,
    ShelfContainsProductsComponent,
    ShelfDialogComponent,
    ShelfPopupComponent,
    ShelfDeleteDialogComponent,
    ShelfDeletePopupComponent,
];

export const shelf_providers = [
    ShelfService,
    ShelfPopupService,
    ShelfResolvePagingParams,
];
