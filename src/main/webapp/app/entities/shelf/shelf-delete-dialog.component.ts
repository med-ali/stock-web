import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { Shelf } from './shelf.model';
import { ShelfPopupService } from './shelf-popup.service';
import { ShelfService } from './shelf.service';

@Component({
    selector: 'jhi-shelf-delete-dialog',
    templateUrl: './shelf-delete-dialog.component.html'
})
export class ShelfDeleteDialogComponent {

    shelf: Shelf;
    alerts = [];
    constructor(private alertService: JhiAlertService,
        private shelfService: ShelfService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    addErrorAlert(message, key?, data?) {
        key = (key && key !== null) ? key : message;
        this.alerts.push(
            this.alertService.addAlert(
                {
                    type: 'danger',
                    msg: key,
                    params: data,
                    timeout: 5000,
                    toast: this.alertService.isToast(),
                    scoped: true
                },
                this.alerts
            )
        );
    }

    confirmDelete(id: number) {
        this.shelfService.delete(id).subscribe((response) => {
            if (response["status"] == 200){
                this.eventManager.broadcast({
                    name: 'shelfListModification',
                    content: 'Deleted an shelf'
                });
                this.activeModal.dismiss(true);
            }
            else{
                this.addErrorAlert('Delete not reachable', 'error.entity.delete.reachable');
            } 
        });
    }
}

@Component({
    selector: 'jhi-shelf-delete-popup',
    template: ''
})
export class ShelfDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private shelfPopupService: ShelfPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.shelfPopupService
                .open(ShelfDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
