import { BaseEntity } from '../../shared';

export class Shelf implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public name?: string,
        public description?: string,
        public barCode?: string,
        public isActive?: boolean,
        public shelvingUnit?: any,
        public checked?: boolean,
    ) {
        this.isActive = true;
    }
}
