import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Shelf } from './shelf.model';
import { ShelfPopupService } from './shelf-popup.service';
import { ShelfService } from './shelf.service';
import { ShelvingUnit, ShelvingUnitService } from '../shelving-unit';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-shelf-dialog',
    templateUrl: './shelf-dialog.component.html'
})
export class ShelfDialogComponent implements OnInit {

    shelf: Shelf;
    isSaving: boolean;
    active: any;
    descriptionShelvingUnit:string;
    shelvingunits: ShelvingUnit[];
    shelvingUnitModal: boolean;
    existCode:boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private shelfService: ShelfService,
        private shelvingUnitService: ShelvingUnitService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.active = 'yes';
        this.shelf.isActive = true;
        this.shelvingUnitModal = false;
        if(this.shelf.shelvingUnit){
            this.descriptionShelvingUnit = this.shelf.shelvingUnit.code + "-" + this.shelf.shelvingUnit.description;
        }
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    public activeChange(event:String) {
        console.log(event)
        if (event === 'yes') {
            this.shelf.isActive = true;
        } else if (event === 'no') {
            this.shelf.isActive = false;
        }
        console.log('up' + this.shelf.isActive)
    }

    save() {
        this.shelfService.checkCode(this.shelf.code).subscribe((value) => { 
            console.log(value) 
            if (value==true && this.shelf.id == undefined){
                this.existCode = true;
            }
            else{
                this.existCode = false;
                this.isSaving = true;
                if (this.shelf.id !== undefined) {
                    this.subscribeToSaveResponse(
                        this.shelfService.update(this.shelf));
                } else {
                    this.subscribeToSaveResponse(
                        this.shelfService.create(this.shelf));
                }
                this.clear()
            }
            },
            (error) => { 
            console.log("error"+error)
        })
    }

    private subscribeToSaveResponse(result: Observable<Shelf>) {
        result.subscribe((res: Shelf) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Shelf) {
        this.eventManager.broadcast({ name: 'shelfListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackShelvingUnitById(index: number, item: ShelvingUnit) {
        return item.id;
    }

    openMyModal(event) {
        if (event === 'shelvingUnitModal') {
            this.shelvingUnitModal = true;
        }
        document.querySelector('#' + event).classList.add('md-show');
    }

    closeModal(event) {
        this.shelvingUnitModal = false;
        (document.querySelector('#' + event).parentElement.parentElement).classList.remove('md-show');
    }

    closeMyModalEvent(event) {
        ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
    }

    assignShelvingUnit(shelvingUnit) {
        this.shelf.shelvingUnit = shelvingUnit;
        this.descriptionShelvingUnit = this.shelf.shelvingUnit.code + "-" + this.shelf.shelvingUnit.description;
        this.closeModal('closeShelvingUnitBtn');
    }
}

@Component({
    selector: 'jhi-shelf-popup',
    template: ''
})
export class ShelfPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private shelfPopupService: ShelfPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.shelfPopupService
                    .open(ShelfDialogComponent as Component, params['id']);
            } else {
                this.shelfPopupService
                    .open(ShelfDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
