import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Shelf } from './shelf.model';
import { ShelfService } from './shelf.service';

@Component({
    selector: 'jhi-shelf-detail',
    templateUrl: './shelf-detail.component.html'
})
export class ShelfDetailComponent implements OnInit, OnDestroy {

    shelf: Shelf;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private shelfService: ShelfService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInShelves();
    }

    load(id) {
        this.shelfService.find(id).subscribe((shelf) => {
            this.shelf = shelf;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInShelves() {
        this.eventSubscriber = this.eventManager.subscribe(
            'shelfListModification',
            (response) => this.load(this.shelf.id)
        );
    }
}
