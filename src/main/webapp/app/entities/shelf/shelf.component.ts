import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { Shelf } from './shelf.model';
import { ShelfService } from './shelf.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, PrintService, SpinnerService } from '../../shared';
import { NgxBarcodeComponent } from 'ngx-barcode';

@Component({
    selector: 'jhi-shelf',
    templateUrl: './shelf.component.html'
})
export class ShelfComponent implements OnInit, OnDestroy {

    currentAccount: any;
    shelves: Shelf[];
    selectedShelves: Shelf[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    checkall: boolean;
    @ViewChild('ngxBarCode') ngxBarCode: NgxBarcodeComponent;
    barcodeValue: string;

    constructor(
        private shelfService: ShelfService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private printService: PrintService,
        private spinnerService: SpinnerService,
        private eventManager: JhiEventManager
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        this.checkall = false;
        if (this.currentSearch) {
            this.shelfService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res) => this.onSuccess(res.body, res.headers),
                (res) => this.onError(res.body)
            );
            return;
        }
        this.shelfService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res) => this.onSuccess(res.body, res.headers),
            (res) => this.onError(res.body)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/shelf'], {
            queryParams:
                {
                    page: this.page,
                    size: this.itemsPerPage,
                    search: this.currentSearch,
                    sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
                }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/shelf', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/shelf', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.selectedShelves = [];
        this.checkall = false;
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInShelves();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Shelf) {
        return item.id;
    }
    registerChangeInShelves() {
        this.eventSubscriber = this.eventManager.subscribe('shelfListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        //this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.shelves = data;
    }
    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    selectOrDeselectAllShelves() {
        this.checkall = !this.checkall;
        if (this.checkall) {
            this.selectedShelves = this.shelves;
            for (let i = 0; i < this.shelves.length; i++) {
                this.shelves[i].checked = true;
            }
        } else {
            this.selectedShelves = [];
            for (let i = 0; i < this.shelves.length; i++) {
                this.shelves[i].checked = false;
            }
        }
    }

    updateSelectedShelves(shelf) {
        if (!shelf.checked) {
            this.selectedShelves.splice(this.renderIndex(shelf, this.selectedShelves), 1);
        } else {
            this.selectedShelves.push(shelf);
        }
        if (this.selectedShelves.length === this.shelves.length) {
            this.checkall = true;
        } else {
            this.checkall = false;
        }
    }

    renderIndex(shelf, shelves: Shelf[]): number {
        for (let i = 0; i < shelves.length; i++) {
            const currentShelf = shelves[i];
            if (currentShelf.id === shelf.id) {
                return i;
            }
        }
        return -1;
    }

    printOneBarCode(shelf) {
        const barcodeWindow = window.open('', '_blank', 'width=900,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        this.barcodeValue = shelf.barCode;
        setTimeout(() => {
            this.printService.printBarcode(this.ngxBarCode.bcElement.nativeElement.innerHTML, barcodeWindow);
        });
    }

    async printMultiBarCode() {
        this.spinnerService.show();
        const barcodeWindow = window.open('', '_blank', 'width=900,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no')
        let element = '';
        let shelfToPrint;
        for (let i = 0; i < this.selectedShelves.length; i++) {
            shelfToPrint = this.selectedShelves[i];
            element += '<div style="text-align: center;line-height: 1;">';
            this.barcodeValue = shelfToPrint.barCode;
            element = await this.appendBarCode(element, this.ngxBarCode);
            element += '</div>';
            if (i < this.selectedShelves.length - 1) {
                element += '<div class="page-break"></div>';
            }
        }
        this.spinnerService.hide();
        setTimeout(() => {
            this.printService.printBarcode(element, barcodeWindow);
        });
    }

    async appendBarCode(element, ngx: NgxBarcodeComponent) {
        await this.delay(20);
        element += ngx.bcElement.nativeElement.innerHTML;
        return element;
    }

    delay(ms: number) {
        return new Promise((resolve) => setTimeout(resolve, ms));
    }
}
