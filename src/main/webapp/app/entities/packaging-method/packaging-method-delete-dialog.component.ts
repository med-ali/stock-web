import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { PackagingMethod } from './packaging-method.model';
import { PackagingMethodPopupService } from './packaging-method-popup.service';
import { PackagingMethodService } from './packaging-method.service';

@Component({
    selector: 'jhi-packaging-method-delete-dialog',
    templateUrl: './packaging-method-delete-dialog.component.html'
})
export class PackagingMethodDeleteDialogComponent {

    packagingMethod: PackagingMethod;
    alerts = [];
    constructor(private alertService: JhiAlertService,
        private packagingMethodService: PackagingMethodService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    addErrorAlert(message, key?, data?) {
        key = (key && key !== null) ? key : message;
        this.alerts.push(
            this.alertService.addAlert(
                {
                    type: 'danger',
                    msg: key,
                    params: data,
                    timeout: 5000,
                    toast: this.alertService.isToast(),
                    scoped: true
                },
                this.alerts
            )
        );
    }

    confirmDelete(id: number) {
        this.packagingMethodService.delete(id).subscribe((response) => {
            console.log(response)
            console.log(response["status"])
            if (response["status"] == 200){
                this.eventManager.broadcast({
                    name: 'packagingMethodListModification',
                    content: 'Deleted an packagingMethod'
                });
                this.activeModal.dismiss(true);
            }
            else{
                this.addErrorAlert('Delete not reachable', 'error.entity.delete.reachable');
            } 
        });
    }
}

@Component({
    selector: 'jhi-packaging-method-delete-popup',
    template: ''
})
export class PackagingMethodDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private packagingMethodPopupService: PackagingMethodPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.packagingMethodPopupService
                .open(PackagingMethodDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
