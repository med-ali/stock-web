import {
    PackagingMethodService,
    PackagingMethodPopupService,
    PackagingMethodComponent,
    PackagingMethodDetailComponent,
    PackagingMethodDialogComponent,
    PackagingMethodPopupComponent,
    PackagingMethodDeletePopupComponent,
    PackagingMethodDeleteDialogComponent,
    packagingMethodRoute,
    packagingMethodPopupRoute,
    PackagingMethodResolvePagingParams,
} from './';

export const packagingMethod_declarations = [
    PackagingMethodComponent,
    PackagingMethodDetailComponent,
    PackagingMethodDialogComponent,
    PackagingMethodDeleteDialogComponent,
    PackagingMethodPopupComponent,
    PackagingMethodDeletePopupComponent,
];

export const packagingMethod_entryComponents = [
    PackagingMethodComponent,
    PackagingMethodDialogComponent,
    PackagingMethodPopupComponent,
    PackagingMethodDeleteDialogComponent,
    PackagingMethodDeletePopupComponent,
];

export const packagingMethod_providers = [
    PackagingMethodService,
    PackagingMethodPopupService,
    PackagingMethodResolvePagingParams,
];
