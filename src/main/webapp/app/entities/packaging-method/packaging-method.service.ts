import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { PackagingMethod } from './packaging-method.model';
import { createRequestOption } from '../../shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';

@Injectable()
export class PackagingMethodService {

    private resourceUrl = SERVER_API_URL + 'api/packaging-methods';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/packaging-methods';

    constructor(private http: HttpClient) { }

    create(packagingMethod: PackagingMethod): Observable<PackagingMethod> {
        return this.http.post<PackagingMethod>(this.resourceUrl, packagingMethod);
    }

    update(packagingMethod: PackagingMethod): Observable<PackagingMethod> {
        return this.http.put<PackagingMethod>(this.resourceUrl, packagingMethod);
    } 

    checkCode(code: string): Observable<PackagingMethod> {
        return this.http.get<PackagingMethod>(this.resourceUrl+"/checkcode/?code="+code);
    }

    find(id: number): Observable<PackagingMethod> {
        return this.http.get<PackagingMethod>(`${this.resourceUrl}/${id}`);
    }

    getDefaultPackagingMethod(): Observable<PackagingMethod> {
        return this.http.get<PackagingMethod>(`${this.resourceUrl}/default`);
    }

    query(req?: any): Observable<HttpResponse<PackagingMethod[]>> {
        const options = createRequestOption(req);
        return this.http.get<PackagingMethod[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<HttpResponse<PackagingMethod[]>> {
        const options = createRequestOption(req);
        return this.http.get<PackagingMethod[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }
}
