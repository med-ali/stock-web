import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { PackagingMethodComponent } from './packaging-method.component';
import { PackagingMethodDetailComponent } from './packaging-method-detail.component';
import { PackagingMethodPopupComponent } from './packaging-method-dialog.component';
import { PackagingMethodDeletePopupComponent } from './packaging-method-delete-dialog.component';

@Injectable()
export class PackagingMethodResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const packagingMethodRoute: Routes = [
    {
        path: 'packaging-method',
        component: PackagingMethodComponent,
        resolve: {
            'pagingParams': PackagingMethodResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.packagingMethod.default'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'packaging-method/:id',
        component: PackagingMethodDetailComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.packagingMethod.default'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const packagingMethodPopupRoute: Routes = [
    {
        path: 'packaging-method-new',
        component: PackagingMethodPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.packagingMethod.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'packaging-method/:id/edit',
        component: PackagingMethodPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.packagingMethod.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'packaging-method/:id/delete',
        component: PackagingMethodDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.packagingMethod.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
