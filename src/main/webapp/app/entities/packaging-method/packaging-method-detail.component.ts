import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { PackagingMethod } from './packaging-method.model';
import { PackagingMethodService } from './packaging-method.service';

@Component({
    selector: 'jhi-packaging-method-detail',
    templateUrl: './packaging-method-detail.component.html'
})
export class PackagingMethodDetailComponent implements OnInit, OnDestroy {

    packagingMethod: PackagingMethod;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private packagingMethodService: PackagingMethodService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPackagingMethods();
    }

    load(id) {
        this.packagingMethodService.find(id).subscribe((packagingMethod) => {
            this.packagingMethod = packagingMethod;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPackagingMethods() {
        this.eventSubscriber = this.eventManager.subscribe(
            'packagingMethodListModification',
            (response) => this.load(this.packagingMethod.id)
        );
    }
}
