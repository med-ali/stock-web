import { BaseEntity } from '../../shared';

export class PackagingMethod implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public description?: string,
        public isDefault?: boolean,
    ) {
        this.isDefault = false;
    }
}
