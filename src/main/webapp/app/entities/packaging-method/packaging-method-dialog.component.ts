import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PackagingMethod } from './packaging-method.model';
import { PackagingMethodPopupService } from './packaging-method-popup.service';
import { PackagingMethodService } from './packaging-method.service';

@Component({
    selector: 'jhi-packaging-method-dialog',
    templateUrl: './packaging-method-dialog.component.html'
})
export class PackagingMethodDialogComponent implements OnInit {

    packagingMethod: PackagingMethod;
    isSaving: boolean;
    default: any;
    existCode:boolean;
    constructor(
        public activeModal: NgbActiveModal,
        private packagingMethodService: PackagingMethodService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        
        if (this.packagingMethod.isDefault == true){
            this.default = 'yes';
        }else {
            this.default = 'no';
        }
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    public defaultChange(event) {
        if (event === 'yes') {
            this.packagingMethod.isDefault = true;
        } else if (event === 'no') {
            this.packagingMethod.isDefault = false;
        }
    }

    save() {
        console.log(this.packagingMethod)
        this.packagingMethodService.checkCode(this.packagingMethod.code).subscribe((value) => { 
            console.log(value) 
            if (value==true && this.packagingMethod.id == undefined){
                this.existCode = true;
            }
            else{
                this.existCode = false;
                this.isSaving = true;
                if (this.packagingMethod.id !== undefined) {
                    this.subscribeToSaveResponse(
                        this.packagingMethodService.update(this.packagingMethod));
                } else {
                    this.subscribeToSaveResponse(
                        this.packagingMethodService.create(this.packagingMethod));
                }
                this.clear()
            }
            },
            (error) => { 
            console.log("error"+error)
        })  
    }

    private subscribeToSaveResponse(result: Observable<PackagingMethod>) {
        result.subscribe((res: PackagingMethod) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: PackagingMethod) {
        this.eventManager.broadcast({ name: 'packagingMethodListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-packaging-method-popup',
    template: ''
})
export class PackagingMethodPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private packagingMethodPopupService: PackagingMethodPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.packagingMethodPopupService
                    .open(PackagingMethodDialogComponent as Component, params['id']);
            } else {
                this.packagingMethodPopupService
                    .open(PackagingMethodDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
