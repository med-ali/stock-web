import {
    ClientService,
    ClientPopupService,
    ClientComponent,
    ClientDetailComponent,
    ClientDialogComponent,
    ClientPopupComponent,
    ClientDeletePopupComponent,
    ClientDeleteDialogComponent,
    clientRoute,
    clientPopupRoute,
    ClientResolvePagingParams,
} from './';

export const client_declarations = [
    ClientComponent,
    ClientDetailComponent,
    ClientDialogComponent,
    ClientDeleteDialogComponent,
    ClientPopupComponent,
    ClientDeletePopupComponent,
];

export const client_entryComponents = [
    ClientComponent,
    ClientDialogComponent,
    ClientPopupComponent,
    ClientDeleteDialogComponent,
    ClientDeletePopupComponent,
];

export const client_providers = [
    ClientService,
    ClientPopupService,
    ClientResolvePagingParams,
];
