import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Client } from './client.model';
import { createRequestOption } from '../../shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { JhiDateUtils } from 'ng-jhipster';

@Injectable()
export class ClientService {

    private resourceUrl = SERVER_API_URL + 'api/clients';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/clients';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(client: Client): Observable<Client> {
        this.convertDate(client);
        return this.http.post<Client>(this.resourceUrl, client);
    }

    update(client: Client): Observable<Client> {
        this.convertDate(client);
        return this.http.put<Client>(this.resourceUrl, client);
    }

    checkEmail(email: string): Observable<Client> {
        return this.http.get<Client>(this.resourceUrl+"/checkemail/?email="+email);
    }
    
    checkPhone(phone: string): Observable<Client> {
        return this.http.get<Client>(this.resourceUrl+"/checkphone/?phone="+phone);
    }

    find(id: number): Observable<Client> {
        return this.http.get<Client>(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<HttpResponse<Client[]>> {
        const options = createRequestOption(req);
        return this.http.get<Client[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<HttpResponse<Client[]>> {
        const options = createRequestOption(req);
        return this.http.get<Client[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }

    convertDate(client: Client) {
        client.startValidation = this.dateUtils.toDate(client.startValidation);
        client.endValidation = this.dateUtils.toDate(client.endValidation);
    }
}
