import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Client } from './client.model';
import { ClientPopupService } from './client-popup.service';
import { ClientService } from './client.service';
import { TvaCode, TvaCodeService } from '../tva-code';
import { Payment, PaymentService } from '../payment';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-client-dialog',
    templateUrl: './client-dialog.component.html'
})
export class ClientDialogComponent implements OnInit {

    client: Client;
    isSaving: boolean;
    paymentModal: boolean;
    tvaCodeModal: boolean;
    step = 1;
    descriptionTvaCode:string;
    descriptionPayment:string;
    existEmail:boolean;
    existPhone:boolean;
    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private clientService: ClientService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.paymentModal = this.tvaCodeModal = false;
        if (this.client.address === undefined) {
            this.client.address = {};
        }

        if(this.client.tvaCode){
            this.descriptionTvaCode = this.client.tvaCode.code + "-" + this.client.tvaCode.description;  
        }
        if(this.client.payment){
            this.descriptionPayment = this.client.payment.code + "-" + this.client.payment.description;  
        }
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        //this.isSaving = true;
        this.clientService.checkEmail(this.client.email).subscribe((value) => { 
            console.log(value) 
            if (value==true && this.client.id == undefined){
                this.existEmail = true;   
            }
            else{
                this.existEmail = false;
                //this.clear()             
                this.clientService.checkPhone(this.client.phoneNumber).subscribe((value) => { 
                    console.log(value) 
                    if (value==true && this.client.id == undefined){
                        this.existPhone = true;
                    }
                    else{
                        this.existPhone = false;
                        if (this.client.id !== undefined) {
                            this.subscribeToSaveResponse(
                                this.clientService.update(this.client));
                        } else {
                            this.subscribeToSaveResponse(
                                this.clientService.create(this.client));
                        }
                        this.isSaving = true;
                        this.clear()
                    }
                    },
                    (error) => { 
                    console.log("error"+error)
                })
            }
            },
            (error) => { 
            console.log("error"+error)
        })
    }
    
    private subscribeToSaveResponse(result: Observable<Client>) {
        result.subscribe((res: Client) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: Client) {
        this.eventManager.broadcast({ name: 'clientListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error: any) {
        this.isSaving = false;
        this.jhiAlertService.error(error.message, null, null);
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    openMyModal(event) {
        if (event === 'paymentModal') {
            this.paymentModal = true;
        } else if (event === 'tvaCodeModal') {
            this.tvaCodeModal = true;
        }
        document.querySelector('#' + event).classList.add('md-show');
    }

    closeModal(event) {
        this.paymentModal = this.tvaCodeModal = false;
        (document.querySelector('#' + event).parentElement.parentElement).classList.remove('md-show');
    }

    closeMyModalEvent(event) {
        ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
    }

    assignTvaCode(tvaCode) {
        this.client.tvaCode = tvaCode;
        this.descriptionTvaCode = this.client.tvaCode.code + "-" + this.client.tvaCode.description;
        this.closeModal('closeTvaCodeBtn'); 
        
    }

    assignPayment(payment) {
        this.client.payment = payment;
        this.descriptionPayment = this.client.payment.code + "-" + this.client.payment.description;
        this.closeModal('closePaymentBtn');
    }

}

@Component({
    selector: 'jhi-client-popup',
    template: ''
})
export class ClientPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientPopupService: ClientPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.clientPopupService
                    .open(ClientDialogComponent as Component, params['id']);
            } else {
                this.clientPopupService
                    .open(ClientDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
