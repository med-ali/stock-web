import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Company } from './company.model';
import { createRequestOption } from '../../shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';

@Injectable()
export class CompanyService {

    private resourceUrl = SERVER_API_URL + 'api/companies';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/companies';

    constructor(private http: HttpClient) { }

    create(company: Company): Observable<Company> {
        return this.http.post<Company>(this.resourceUrl, company);
    }

    checkCode(code: string): Observable<Company> {
        return this.http.get<Company>(this.resourceUrl+"/checkcode/?code="+code);
    }

    update(company: Company): Observable<Company> {
        return this.http.put<Company>(this.resourceUrl, company);
    }

    find(id: number): Observable<Company> {
        return this.http.get<Company>(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<HttpResponse<Company[]>> {
        const options = createRequestOption(req);
        return this.http.get<Company[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<HttpResponse<Company[]>> {
        const options = createRequestOption(req);
        return this.http.get<Company[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }
}
