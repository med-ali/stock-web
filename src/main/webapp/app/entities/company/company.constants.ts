import {
    CompanyService,
    CompanyPopupService,
    CompanyComponent,
    CompanyDetailComponent,
    CompanyDialogComponent,
    CompanyPopupComponent,
    CompanyDeletePopupComponent,
    CompanyDeleteDialogComponent,
    companyRoute,
    companyPopupRoute,
    CompanyResolvePagingParams,
} from './';

export const company_declarations = [
    CompanyComponent,
    CompanyDetailComponent,
    CompanyDialogComponent,
    CompanyDeleteDialogComponent,
    CompanyPopupComponent,
    CompanyDeletePopupComponent,
];

export const company_entryComponents = [
    CompanyComponent,
    CompanyDialogComponent,
    CompanyPopupComponent,
    CompanyDeleteDialogComponent,
    CompanyDeletePopupComponent,
];

export const company_providers = [
    CompanyService,
    CompanyPopupService,
    CompanyResolvePagingParams,
];
