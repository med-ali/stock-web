import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { createRequestOption } from '../../shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { JhiDateUtils } from 'ng-jhipster';
import { DeliveredEquipement } from '../delivered-equipement';
import { StockMovement } from '../stock-movement';

@Injectable()
export class AnalysisService {

    private resourceUrl = SERVER_API_URL + 'api/analysis';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/analysis';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    getArticlesListToBeExpiry(expirationDate: any): Observable<HttpResponse<any[]>> {
        
        if (!expirationDate) {
            // expirationDate = this.dateUtils.toDate(expirationDate);
            expirationDate = '2020-12-31T00:00:00.123Z';
        }
        else{
            expirationDate = expirationDate + "T00:00:00.123Z";
        }
        return this.http.get<any[]>
            (`${this.resourceUrl}/getArticlesListToBeExpiry?expirationDate=${expirationDate}`, { observe: 'response' });
    }

    filterByNullQuantity(type: string): Observable<HttpResponse<any[]>> {
        return this.http.get<any[]>
            (`${this.resourceUrl}/filterByNullQuantity?type=${type}`, { observe: 'response' });
    }

    updateInventory(stockMovement: StockMovement): any {
        return this.http.post<any>
            (`${this.resourceUrl}/updateInventorySession`, stockMovement, { observe: 'response' });
    }

    saveAll(): Observable<HttpResponse<any[]>> {
        return this.http.get<any[]>(`${this.resourceUrl}/saveAll`, { observe: 'response' });
    }

    createAdvancedSearchOption = (req?: any): HttpParams => {
        let Params = new HttpParams();
        if (req) {
            // Begin assigning parameters
            if (req.page) {
                Params = Params.append('page', req.page)
            }
            if (req.size) {
                Params = Params.append('size', req.size)
            }
            if (req.query) {
                Params = Params.append('q', req.query)
            }
            if (req.filter) {
                Params = Params.append('filter', req.filter)
            }
            if (req.sort) {
                for (const s of req.sort) {
                    Params = Params.append('sort', s);
                }
            }
            if (req.status) {
                Params = Params.append('status', req.status)
            }
            if (req.fromMovement) {
                Params = Params.append('fromMov', req.fromMovement)
            }
            if (req.toMovement) {
                Params = Params.append('toMov', req.toMovement)
            }
            if (req.annulled) {
                Params = Params.append('annulled', req.annulled)
            }
            if (req.client) {
                Params = Params.append('client', req.client)
            }
        }
        return Params;
    };

    convertDate(stockMovement: StockMovement) {
        stockMovement.movementDate = this.dateUtils.toDate(stockMovement.movementDate);
    }

    printReport(expirationDate:any,lang:string): Observable<any> {
        if (!expirationDate) {
            // expirationDate = this.dateUtils.toDate(expirationDate);
            expirationDate = '2020-12-31T00:00:00.123Z';
        }
        else{
            expirationDate = expirationDate + "T00:00:00.123Z";
        }
        return this.http.get<any[]>
            (`${this.resourceUrl}/getArticlesListToBeExpiry/print/?expirationDate=${expirationDate}&lang=${lang}`);
    }
}
