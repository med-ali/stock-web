import {
    AnalysisService,
    AnalysisComponent,
    AnalysisResolvePagingParams
} from './';

export const analysis_declarations = [
    AnalysisComponent,
];

export const analysis_entryComponents = [
    AnalysisComponent,
];

export const analysis_providers = [
    AnalysisService,
    AnalysisResolvePagingParams,
];
