import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService, JhiLanguageService } from 'ng-jhipster';
 
import { AnalysisService } from './analysis.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, PrintService, MainService } from '../../shared';
import { NgxBarcodeComponent } from 'ngx-barcode';
import { Client } from '../client';
import swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
 
@Component({
    selector: 'jhi-analysis',
    templateUrl: './analysis.component.html'
})
export class AnalysisComponent implements OnInit, OnDestroy {
 
    stockMovements: any[];
    selectedRow: any;
    showDetail = false;
    update = false;
    today = new Date();
    endDate: any;
    nullqt: string;
 
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    criteria: any;
    previousPage: any;
    reverse: any;
    /*
    ** Inventaire **
    */
    execute: boolean;
    isLoading = false;
    curPage: number;
 
    shelfModal = false;
    shelvingUnitModal = false;
    headquarterModal = false;
    selectedShelf = null;
    selectedShelvingUnit = null;
    selectedHeadquarter = null;
    shelfCode = null;
    shelvingUnitCode = null;
    headquarterCode = null;
    oldSelectedRow: any;
    display = 'all';
    @ViewChild('inventoryReport') inventoryReport: ElementRef;
 
    constructor(
        private languageService: JhiLanguageService,
        private parseLinks: JhiParseLinks,
        private analysisService: AnalysisService,
        private jhiAlertService: JhiAlertService,
        private printService: PrintService,
        private mainService: MainService,
        private translateService: TranslateService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = true;
            this.predicate = 'description';
            this.criteria = this.sort();
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }
 
    ngOnInit() {
        this.nullqt = 'all';
        this.stockMovements = [];
        //this.endDate = this.mainService.convertDateForInputDateLocal(this.mainService.getLastDateInCurrentYear(new Date()));
        // this.selectedHeadquarter = { id: 5, code: '001', name: 'Laboratorio Bergamo', description: 'Laboratorio Bergamo Via Borgo Palazzo - Bergamo', isActive: true };
        // this.selectedShelvingUnit = { id: 12, code: 'BG002', name: 'Acc. Campioni', description: 'Acc. Campioni (Bg)', isActive: true };
        // this.selectedShelf = { id: 12, code: 'BG002', name: 'Acc. Campioni', description: 'Acc. Campioni (Bg)', isActive: true };
    }
    executeArticlesAnalysis() {
        /*if (!this.endDate){
            this.endDate = this.mainService.convertDateForInputDateLocal(this.mainService.getLastDateInCurrentYear(new Date()));
        }*/
        this.analysisService.getArticlesListToBeExpiry(this.endDate).subscribe((data) => {
            this.stockMovements = data.body;
            this.totalItems = this.stockMovements.length;
            this.queryCount = this.totalItems;
        });
    }
    loadArticlesPdf() {
        this.analysisService.printReport(this.endDate,this.languageService.currentLang).subscribe((value) => {
            //this.product = product;
            //console.log('hererere'+value['res'])
            const linkSource = 'data:application/pdf;base64,' + value['res'];
            const downloadLink = document.createElement("a");
            const fileName = "analysis.pdf";
            downloadLink.href = linkSource;
            downloadLink.download = fileName;
            //window.open("data:application/pdf;base64, " + value['res'], '', "height=600,width=800");
            //downloadLink.click();
            if (downloadLink.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                //downloadLink.setAttribute("href", window.URL.createObjectURL(linkSource));
                downloadLink.setAttribute("download", fileName);
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
            } else {
                console.log('Pdf export only works in Chrome, Firefox, and Opera.');
                var fileBlob = new Blob([value['res']], {type: 'application/pdf'});
                window.navigator.msSaveBlob(fileBlob, fileName);   
            }
        });
    }
    dayDiff(date) {
        const year = new Date().getFullYear();
        const month = new Date().getMonth() + 1;
        const day = new Date().getDate();
        const oneDay = 24 * 60 * 60 * 1000;
        date = date.slice(0, 19);
        const res0 = date.split('T');
        const res1 = res0[0].split('-');
        const res2 = res0[1].split(':');
        
        const diffTime = new Date(res1[0], res1[1], res1[2], res2[0], res2[1], res2[2]).getTime() - new Date(year, month, day).getTime();
        return Math.floor(diffTime / oneDay);
    }
 
    timeStampFormat(date) {
        return date.split('.')[0];
    }
 
    /*sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }*/
    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        if(this.reverse == true){
            this.sortInvDesc(this.predicate)
        }
        else{
            this.sortInvAsc(this.predicate)  
        }
        return result;
    }
    sortInvAsc(predicate:string) {
        predicate = predicate.replace("it.","");
        if(this.stockMovements && this.stockMovements.length > 0){
            console.log("this.reversedescfilter"+this.reverse)
            console.log("this.reversedescfilter"+predicate)
        this.stockMovements.sort(function(a, b) {
             
             if (predicate == "numberOfDaysToExpired" ){
                
                const year = new Date().getFullYear();
                const month = new Date().getMonth() + 1;
                const day = new Date().getDate();
                const oneDay = 24 * 60 * 60 * 1000;
                let expirationDate = a.deliveredEquipement.expirationDate.slice(0, 19);
                const res0 = expirationDate.split('T');
                const res1 = res0[0].split('-');
                const res2 = res0[1].split(':');
                
                const diffTime = new Date(res1[0], res1[1], res1[2], res2[0], res2[1], res2[2]).getTime() - new Date(year, month, day).getTime();
                 var nameAN =  Math.floor(diffTime / oneDay);
                const yearb = new Date().getFullYear();
                const monthb = new Date().getMonth() + 1;
                const dayb = new Date().getDate();
                const oneDayb = 24 * 60 * 60 * 1000;
                let expirationDateb = b.deliveredEquipement.expirationDate.slice(0, 19);
                const res0b = expirationDateb.split('T');
                const res1b = res0b[0].split('-');
                const res2b = res0b[1].split(':');
                
                const diffTimeb = new Date(res1b[0], res1b[1], res1b[2], res2b[0], res2b[1], res2b[2]).getTime() - new Date(yearb, monthb, dayb).getTime();
                var nameBN =  Math.floor(diffTimeb / oneDayb);
                //var nameA = this.dayDiff(a.productFr.expirationDate)-1 ; // ignore upper and lowercase
                //var nameB = this.dayDiff(b.productFr.expirationDate)-1; // ignore upper and lowercase
            }else if (predicate == "expirationDate"){
                var nameA =  a.deliveredEquipement.expirationDate ;
                var nameB = b.deliveredEquipement.expirationDate;
            }
            else if(predicate === "shelf.id"){
                 var nameA = a["shelf"].id ; // ignore upper and lowercase
                 var nameB = b["shelf"].id ; // ignore upper and lowercase
            }
            else if(predicate === "shelf.shelvingUnit.id"){
                var nameA = a["shelf"].shelvingUnit.id ; // ignore upper and lowercase
                var nameB = b["shelf"].shelvingUnit.id ; // ignore upper and lowercase
            }
            else if(predicate === "shelf.shelvingUnit.headquarter.id"){
                var nameA = a["shelf"].shelvingUnit.headquarter.id ; // ignore upper and lowercase
                var nameB = b["shelf"].shelvingUnit.headquarter.id ; // ignore upper and lowercase
            }
            else if(predicate === "shelf.shelvingUnit.headquarter.company.id"){
                var nameA = a["shelf"].shelvingUnit.headquarter.company.id ; // ignore upper and lowercase
                var nameB = b["shelf"].shelvingUnit.headquarter.company.id ; // ignore upper and lowercase
            }
            else if (predicate === "productionBatch" && a[predicate] != null) {
                var nameA = a.deliveredEquipement.productionBatch.toUpperCase(); // ignore upper and lowercase
                var nameB = b.deliveredEquipement.productionBatch.toUpperCase(); // ignore upper and lowercase
            }
            else if (predicate === "code" && a[predicate] != null) {
                var nameA = a.product.code ; // ignore upper and lowercase
                var nameB = b.product.code ; // ignore upper and lowercase
            }
            else if (predicate === "description" && a[predicate] != null) {
                if (a.kit){
                   var nameA = a.product.description.toUpperCase(); // ignore upper and lowercase
                }else{
                    var nameB = b.product.description.toUpperCase(); // ignore upper and lowercase
                }
          }
            if (nameA < nameB || nameAN < nameBN) {
              return -1;
            }
            if (nameA > nameB || nameAN < nameBN) {
              return 1;
            }
          
            // names must be equal
            return 0;
          });
        }
    }

    sortInvDesc(predicate:string) {
        predicate = predicate.replace("it.","");
        if(this.stockMovements && this.stockMovements.length > 0){
            console.log("this.reversedescfilter"+this.reverse)
            console.log("this.reversedescfilter"+predicate)
        this.stockMovements.sort(function(a, b) {
             if (predicate == "numberOfDaysToExpired" ){
               
                const year = new Date().getFullYear();
                const month = new Date().getMonth() + 1;
                const day = new Date().getDate();
                const oneDay = 24 * 60 * 60 * 1000;
                let expirationDate = a.deliveredEquipement.expirationDate.slice(0, 19);
                const res0 = expirationDate.split('T');
                const res1 = res0[0].split('-');
                const res2 = res0[1].split(':');
                
                const diffTime = new Date(res1[0], res1[1], res1[2], res2[0], res2[1], res2[2]).getTime() - new Date(year, month, day).getTime();
                 var nameAN =  Math.floor(diffTime / oneDay);
                const yearb = new Date().getFullYear();
                const monthb = new Date().getMonth() + 1;
                const dayb = new Date().getDate();
                const oneDayb = 24 * 60 * 60 * 1000;
                let expirationDateb = b.deliveredEquipement.expirationDate.slice(0, 19);
                const res0b = expirationDateb.split('T');
                const res1b = res0b[0].split('-');
                const res2b = res0b[1].split(':');
                
                const diffTimeb = new Date(res1b[0], res1b[1], res1b[2], res2b[0], res2b[1], res2b[2]).getTime() - new Date(yearb, monthb, dayb).getTime();
                var nameBN =  Math.floor(diffTimeb / oneDayb);
                //var nameA = this.dayDiff(a.productFr.expirationDate)-1 ; // ignore upper and lowercase
                //var nameB = this.dayDiff(b.productFr.expirationDate)-1; // ignore upper and lowercase
            }else if (predicate == "expirationDate"){
                var nameA =  a.deliveredEquipement.expirationDate ;
                var nameB = b.deliveredEquipement.expirationDate;
            }
            else if(predicate === "shelf.id"){
                 var nameA = a["shelf"].id ; // ignore upper and lowercase
                 var nameB = b["shelf"].id ; // ignore upper and lowercase
            }
            else if(predicate === "shelf.shelvingUnit.id"){
                var nameA = a["shelf"].shelvingUnit.id ; // ignore upper and lowercase
                var nameB = b["shelf"].shelvingUnit.id ; // ignore upper and lowercase
            }
            else if(predicate === "shelf.shelvingUnit.headquarter.id"){
                var nameA = a["shelf"].shelvingUnit.headquarter.id ; // ignore upper and lowercase
                var nameB = b["shelf"].shelvingUnit.headquarter.id ; // ignore upper and lowercase
            }
            else if(predicate === "shelf.shelvingUnit.headquarter.company.id"){
                var nameA = a["shelf"].shelvingUnit.headquarter.company.id ; // ignore upper and lowercase
                var nameB = b["shelf"].shelvingUnit.headquarter.company.id ; // ignore upper and lowercase
            }
            else if (predicate === "productionBatch" && a[predicate] != null) {
                var nameA = a.deliveredEquipement.productionBatch.toUpperCase(); // ignore upper and lowercase
                var nameB = b.deliveredEquipement.productionBatch.toUpperCase(); // ignore upper and lowercase
            }
            else if (predicate === "code" && a[predicate] != null) {
                var nameA = a.product.code ; // ignore upper and lowercase
                var nameB = b.product.code ; // ignore upper and lowercase
            }
            else if (predicate === "description" && a[predicate] != null) {
                if (a.kit){
                   var nameA = a.product.description.toUpperCase(); // ignore upper and lowercase
                }else{
                    var nameB = b.product.description.toUpperCase(); // ignore upper and lowercase
                }
          }
            if (nameA < nameB || nameAN < nameBN) {
              return 1;
            }
            if (nameA > nameB || nameAN < nameBN) {
              return -1;
            }
          
            // names must be equal
            return 0;
          });
        }
    }
    ngOnDestroy() {
    }
}
