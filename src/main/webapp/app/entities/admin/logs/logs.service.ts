import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../../app.constants';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { Log } from './log.model';

@Injectable()
export class LogsService {
    constructor(private http: HttpClient) { }

    changeLevel(log: Log): Observable<HttpResponse<any>> {
        return this.http.put<any>(SERVER_API_URL + 'management/logs', log);
    }

    findAll(): Observable<Log[]> {
        return this.http.get<Log[]>(SERVER_API_URL + 'management/logs');
    }
}
