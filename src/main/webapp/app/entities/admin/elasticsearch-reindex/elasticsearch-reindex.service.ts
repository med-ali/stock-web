import { SERVER_API_URL } from './../../../app.constants';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpResponse } from '@angular/common/http';

@Injectable()
export class ElasticsearchReindexService {

    constructor(
        private http: HttpClient
    ) { }

    reindex(): Observable<HttpResponse<any>> {
        return this.http.post<any>(SERVER_API_URL + 'api/elasticsearch/index', {});
    }
}
