import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../../app.constants';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Audit } from './audit.model';

@Injectable()
export class AuditsService  {
    constructor(private http: HttpClient) { }

    query(req: any): Observable<HttpResponse<Audit[]>> {
        const options = new HttpParams();
        options.set('fromDate', req.fromDate);
        options.set('toDate', req.toDate);
        options.set('page', req.page);
        options.set('size', req.size);
        options.set('sort', req.sort);

        return this.http.get<Audit[]>(SERVER_API_URL + 'management/audits', { params: options, observe: 'response' });
    }
}
