import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../../app.constants';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class JhiMetricsService {

    constructor(private http: HttpClient) {}

    getMetrics(): Observable<any> {
        return this.http.get<any>(SERVER_API_URL + 'management/metrics')
    }

    threadDump(): Observable<any> {
        return this.http.get<any>(SERVER_API_URL + 'management/dump');
    }
}
