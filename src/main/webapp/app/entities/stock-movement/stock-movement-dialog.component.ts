import { Component, OnInit, OnDestroy, Output, EventEmitter, ViewChild, LOCALE_ID } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { MainService } from '../../shared/utils/main.service';
import { StockMovement, SMState } from './stock-movement.model';
import { StockMovementPopupService } from './stock-movement-popup.service';
import { StockMovementService } from './stock-movement.service';
import { Supplier, SupplierService } from '../supplier';
import { PackagingMethod, PackagingMethodService } from '../packaging-method';
import { Article, ArticleService } from '../article';
import { Kit, KitService } from '../kit';
import { OrderDetails, OrderDetailsService } from '../order-details';
import { Product, ProductService } from '../product';
import { Commande, CommandeService } from '../commande';
import { ResponseWrapper } from '../../shared';
import swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'jhi-stock-movement-dialog',
    templateUrl: './stock-movement-dialog.component.html',
    styleUrls: [
        '../../../../../../node_modules/sweetalert2/src/sweetalert2.scss'
    ]
})
export class StockMovementDialogComponent implements OnInit {

    stockMovement: StockMovement;
    selectedCommande: Commande;
    isSaving: boolean;
    step = 1;
    item: any;
    maxQuantity: number;
    QuantitySelected: number;
    notAvailable: boolean;
    stockAvailabilityCommands: any[];
    status: string;
    kit: boolean;
    itemCode: any;
    shelfModal: boolean;
    destinationShelfModal: boolean;
    clientModal: boolean;
    deliveredEquipementModal: boolean;

    smState = SMState;
    descriptionStockMovement:string;
    descriptionShelf:string;
    descriptionDestinationShelf:string;
    descriptionDeliveredEquipement:string;
    descriptionProduct:string;
    stockNotFound:boolean;
    dateTab:any[];
    constructor(
        private mainService: MainService,
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private stockMovementService: StockMovementService,
        private supplierService: SupplierService,
        private commandeService: CommandeService,
        private packagingMethodService: PackagingMethodService,
        private articleService: ArticleService,
        private kitService: KitService,
        private orderDetailsService: OrderDetailsService,
        private productService: ProductService,
        private eventManager: JhiEventManager,
        private translateService: TranslateService
    ) {
    }
    setClickedRow(i){
        this.maxQuantity = this.stockAvailabilityCommands[i].maxQuantity;
        this.QuantitySelected = i ;
        this.stockMovement.deliveredEquipement = this.stockAvailabilityCommands[i].deliveredEquipement;
    }
    ngOnInit() {
        this.isSaving = false;
        this.kit = false;
        this.shelfModal = this.destinationShelfModal = this.clientModal = this.deliveredEquipementModal = false;
        if (this.stockMovement.id !== undefined) {
            this.status = this.stockMovement.movementType.toString();
            this.itemCode = this.stockMovement.code;
            this.maxQuantity = 0
            this.QuantitySelected = 0;
            if (this.stockMovement.movementType !== "LO"){
                this.stockMovementService.getStockAvailability(this.stockMovement.code, this.stockMovement.shelf.code).subscribe((res) => {
                    this.stockAvailabilityCommands = res.body;
                    this.dateTab = []
                    this.maxQuantity = this.stockAvailabilityCommands[0].maxQuantity;
                    this.QuantitySelected = 0;
                    this.stockAvailabilityCommands.forEach(element => {
                        if (element.deliveredEquipement.expirationDate){
                            this.dateTab.push(element.deliveredEquipement.expirationDate.split("T")[0]); 
                        }
                        //element.deliveredEquipement.expirationDate= this.mainService.convertDateForInputDate(element.deliveredEquipement.expirationDate)
                       
                    });
                    this.stockMovement.deliveredEquipement = this.stockAvailabilityCommands[0].deliveredEquipement;
                    if (this.stockAvailabilityCommands.length > 1) {
                        this.maxQuantity = this.stockAvailabilityCommands[0].maxQuantity;
                        this.QuantitySelected = 0;
                    } else if (this.stockAvailabilityCommands.length === 1) {
                        this.maxQuantity = this.stockAvailabilityCommands[0].maxQuantity;
                        this.QuantitySelected = 0;
                    } else {
                        this.maxQuantity = 0;
                    }
                    if (this.maxQuantity <= 0) {
                        this.notAvailable = true;
                    }
                });
            }
            else{
                this.getItemByCode(this.stockMovement.code);
            }
           
            if (this.stockMovement.kit) {
                this.kit = true;
            } else if (this.stockMovement.article) {
                this.kit = false;
            }
        } else {
            this.status = this.smState.loading;
            this.stockMovement.movementType = this.smState.loading;
        }

        if(this.stockMovement.client){
            this.descriptionShelf = this.stockMovement.client.companyName + "-" + this.stockMovement.client.email;  
        }

        if(this.stockMovement.shelf){
            this.descriptionShelf = this.stockMovement.shelf.code + "-" + this.stockMovement.shelf.description;  
        }

        if(this.stockMovement.destinationShelf){
            this.descriptionDestinationShelf = this.stockMovement.destinationShelf.code + "-" + this.stockMovement.destinationShelf.description;  
        }

        if(!this.kit && this.stockMovement.article){
            this.descriptionDeliveredEquipement = this.stockMovement.article.code + "-" + this.stockMovement.article.description;  
        }
        else if (this.kit && this.stockMovement.kit){
            this.descriptionDeliveredEquipement = this.stockMovement.kit.code + "-" + this.stockMovement.kit.description;
        }
        
        if(this.stockMovement.deliveredEquipement){
            this.descriptionProduct = this.stockMovement.deliveredEquipement.product.code + "-" + this.stockMovement.deliveredEquipement.product.description;  
        }
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.stockMovement.id !== undefined) {
            this.subscribeToSaveResponse(
                this.stockMovementService.update(this.stockMovement));
        } else {
            this.subscribeToSaveResponse(
                this.stockMovementService.create(this.stockMovement));
        }
        //this.clear()
    }

    private findProductAssignedWithArticle(id) {
        this.productService.findAssignedWithArticle(id).subscribe((product) => {
            this.stockMovement.product = product.body;
        }); 
    }

    private subscribeToSaveResponse(result: Observable<StockMovement>) {
        result.subscribe((res: StockMovement) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: StockMovement) {
        this.eventManager.broadcast({ name: 'stockMovementListModification', content: 'OK' });
        this.isSaving = false;
        if (result.movementType === this.smState.cons_unloading && result.client) {
            this.eventManager.broadcast({ name: 'stockMovementLoadEtiquette', content: result });
        }
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    public statusChange(event) {
        if (event  !== this.status) {
            switch (event) {
                case 'LO': this.stockMovement.movementType = this.smState.loading;
                    break;
                case 'CUN': this.stockMovement.movementType = this.smState.cons_unloading;
                    break;
                case 'EUN': this.stockMovement.movementType = this.smState.exp_unloading;
                    break;
                case 'TR': this.stockMovement.movementType = this.smState.transfert;
                    break;
                case 'IN': this.stockMovement.movementType = this.smState.inventaire;
                    break;
            }
            this.stockMovement.itemCode = '';
            this.stockMovement.destinationShelf = null;
            //this.loadItem(this.itemCode);
            this.stockMovement.product = null;
            this.stockMovement.kit = null;
            this.stockMovement.article = null;
            this.stockMovement.destinationShelf = null;
            this.stockMovement.quantity = 0;
            this.maxQuantity = 0;
            this.loadItem(this.itemCode);
        }
    }

    loadItem(code) {
        if (code != null) {
            if (code.length === 16) {
                if (this.stockMovement.movementType === this.smState.loading) {
                    this.getItemByCode(code);
                    //this.descriptionProduct = this.stockMovement.deliveredEquipement.product.code + "-" + this.stockMovement.deliveredEquipement.product.description;
                }
                if (this.stockMovement.movementType === this.smState.cons_unloading
                    || this.stockMovement.movementType === this.smState.exp_unloading
                    || this.stockMovement.movementType === this.smState.transfert) {
                    this.getStockAvailability(code);
                    //this.getItemByCode(code);
                }
            }
        }
    }

    getItemByCode(code) { 
        this.stockMovementService.getItemByCode(code).subscribe((data) => {
            this.item = data.body;
            this.stockMovement.code = code;
            this.stockMovement.deliveredEquipement = this.item;
            if (this.item.orderDetail) {
                this.stockMovement.kit = this.item.orderDetail.kit;
                this.stockMovement.article = this.item.orderDetail.article;
                this.stockMovement.product = this.item.product;
                this.descriptionProduct = this.stockMovement.deliveredEquipement.product.code + "-" + this.stockMovement.deliveredEquipement.product.description;
            } else {
                this.stockMovement.kit = this.item.kit;
                this.stockMovement.article = this.item.article;
                this.stockMovement.product = this.item.product;
                this.descriptionProduct = this.stockMovement.deliveredEquipement.product.code + "-" + this.stockMovement.deliveredEquipement.product.description;
            }
            if (this.stockMovement.kit) {
                this.kit = true;
            } else if (this.stockMovement.article) {
                this.kit = false;
            }
            this.stockMovement.quantity = this.item.deliveredQuantity - this.item.stockedQuantity;
            this.maxQuantity = this.item.deliveredQuantity - this.item.stockedQuantity;
            if (this.maxQuantity <= 0) {
                this.notAvailable = true;
            }
            this.descriptionProduct = this.stockMovement.deliveredEquipement.product.code + "-" + this.stockMovement.deliveredEquipement.product.description;
            if(!this.kit && this.stockMovement.article){
                this.descriptionDeliveredEquipement = this.stockMovement.article.code + "-" + this.stockMovement.article.description;  
            }
            else if (this.kit && this.stockMovement.kit){
                this.descriptionDeliveredEquipement = this.stockMovement.kit.code + "-" + this.stockMovement.kit.description;
            }
            
            if(this.stockMovement.deliveredEquipement){
                this.descriptionProduct = this.stockMovement.deliveredEquipement.product.code + "-" + this.stockMovement.deliveredEquipement.product.description;  
            }
        },(error) => {
            this.stockNotFound = true
            setTimeout(()=>{ this.stockNotFound = false }, 3000)
        }
        
        );

    }

    getStockAvailability(code) {
        this.stockMovementService.getStockMovementByCode(code, this.stockMovement.shelf.code).subscribe((data) => {
            this.item = data.body;
            this.stockMovement.code = this.item.code;
            this.stockMovement.kit = this.item.kit;
            this.stockMovement.article = this.item.article;
            this.stockMovement.product = this.item.product;
            // his.stockMovement.shelf = this.item.shelf;
           
            if (this.stockMovement.kit) {
                this.kit = true;
            } else if (this.stockMovement.article) {
                this.kit = false;
            }
            this.stockMovement.quantity = 1;
            this.maxQuantity = 0;
             
            this.stockMovementService.getStockAvailability(code, this.stockMovement.shelf.code).subscribe((res) => {
                this.stockAvailabilityCommands = res.body;
                this.dateTab = []
                this.setClickedRow(0);
                this.stockMovement.deliveredEquipement = this.stockAvailabilityCommands[0].deliveredEquipement;
                this.stockAvailabilityCommands.forEach(element => { //.split("T")[0]element.deliveredEquipement.expirationDate= this.mainService.convertDateForInputDate(element.deliveredEquipement.expirationDate)
                    //element.deliveredEquipement.expirationDate= element.deliveredEquipement.expirationDate;
                    if (element.deliveredEquipement.expirationDate != null){
                        this.dateTab.push(element.deliveredEquipement.expirationDate.split("T")[0]);
                    }
                    
                });
                
                if (this.stockAvailabilityCommands.length > 1) {
                    this.maxQuantity = this.stockAvailabilityCommands[0].maxQuantity;
                } else if (this.stockAvailabilityCommands.length === 1) {
                    this.maxQuantity = this.stockAvailabilityCommands[0].maxQuantity;
                } else {
                    this.maxQuantity = 0;
                    this.stockNotFound = true
                    setTimeout(()=>{ this.stockNotFound = false }, 3000)
                }
                if (this.maxQuantity <= 0) {
                    this.notAvailable = true;
                    this.stockNotFound = true
                    setTimeout(()=>{ this.stockNotFound =false}, 3000)
                }
                this.descriptionProduct = this.stockMovement.deliveredEquipement.product.code + "-" + this.stockMovement.deliveredEquipement.product.description;
                if(!this.kit && this.stockMovement.article){
                    this.descriptionDeliveredEquipement = this.stockMovement.article.code + "-" + this.stockMovement.article.description;  
                }
                else if (this.kit && this.stockMovement.kit){
                    this.descriptionDeliveredEquipement = this.stockMovement.kit.code + "-" + this.stockMovement.kit.description;
                }
                
                if(this.stockMovement.deliveredEquipement){
                    this.descriptionProduct = this.stockMovement.deliveredEquipement.product.code + "-" + this.stockMovement.deliveredEquipement.product.description;  
                }
            });
            
        },
        (error) => {
            this.stockNotFound = true
            setTimeout(()=>{ this.stockNotFound = false }, 3000)
        }
        
        );
    }

    public initSubs(entity) {
        if (entity === 'deliveredEquipement') {
            this.stockMovement.deliveredEquipement = null;
            this.stockMovement.quantity = null;
            this.descriptionDeliveredEquipement = ''
        }
        if (entity === 'shelf') {
            this.stockMovement.shelf = null;
            this.stockMovement.client = null;
            this.stockMovement.destinationShelf = null;
            this.stockMovement.deliveredEquipement = null;
            this.stockMovement.quantity = null;
            this.stockMovement.note = null;
            this.descriptionShelf = ''
        }
    }

    openMyModal(event) {
        if (event === 'shelfModal') {
            this.shelfModal = true;
        } else if (event === 'destinationShelfModal') {
            this.destinationShelfModal = true;
        } else if (event === 'clientModal') {
            this.clientModal = true;
        } else if (event === 'deliveredEquipementModal') {
            this.deliveredEquipementModal = true;
        }
        document.querySelector('#' + event).classList.add('md-show');
    }

    closeModal(event) {
        this.shelfModal = this.destinationShelfModal = this.clientModal = this.deliveredEquipementModal = false;
        (document.querySelector('#' + event).parentElement.parentElement).classList.remove('md-show');
    }

    closeMyModalEvent(event) {
        ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
    }

    assignDeliveredEquipement(deliveredEquipement) {
        this.stockMovement.deliveredEquipement = deliveredEquipement;
        this.stockMovement.quantity = this.stockMovement.deliveredEquipement.deliveredQuantity -
        this.stockMovement.deliveredEquipement.stockedQuantity;
        if(!this.kit){
            this.descriptionDeliveredEquipement = this.stockMovement.article.code + "-" + this.stockMovement.article.description;  
        }
        else if (this.kit){
            this.descriptionDeliveredEquipement = this.stockMovement.kit.code + "-" + this.stockMovement.kit.description;
        }
        this.closeModal('closeDeliveredEquipementBtn');
    }

    assignShelf(shelf) {
        this.stockMovement.shelf = shelf;
        this.stockMovement.client = null;
        this.stockMovement.destinationShelf = null;
        this.stockMovement.deliveredEquipement = null;
        this.stockMovement.quantity = null;
        this.stockMovement.note = null;
        this.descriptionShelf = this.stockMovement.shelf.code + "-" + this.stockMovement.shelf.description;
        this.closeModal('closeShelfBtn');
    }

    assignDestinationShelf(shelf) {
        this.stockMovement.destinationShelf = shelf;
        this.descriptionDestinationShelf = this.stockMovement.destinationShelf.code + "-" + this.stockMovement.destinationShelf.description;
        this.closeModal('closeDestinationShelfBtn');
    }

    assignClient(client) {
        this.stockMovement.client = client;
        this.descriptionStockMovement = this.stockMovement.client.companyName + "-" + this.stockMovement.client.email;
        this.closeModal('closeClientBtn');
    }
}

@Component({
    selector: 'jhi-delivered-equipement-popup',
    template: ''
})
export class StockMovementPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private stockMovementPopupService: StockMovementPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.stockMovementPopupService
                    .open(StockMovementDialogComponent as Component, params['id']);
            } else {
                this.stockMovementPopupService
                    .open(StockMovementDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
