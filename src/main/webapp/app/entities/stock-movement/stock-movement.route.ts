import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { StockMovementComponent } from './stock-movement.component';
import { StockMovementDetailComponent } from './stock-movement-detail.component';
import { StockMovementPopupComponent } from './stock-movement-dialog.component';
import { StockMovementDeletePopupComponent } from './stock-movement-delete-dialog.component';

@Injectable()
export class StockMovementResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const stockMovementRoute: Routes = [
    {
        path: 'stock-movement',
        component: StockMovementComponent,
        resolve: {
            'pagingParams': StockMovementResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.stockMovement.default'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'stock-movement/:id',
        component: StockMovementDetailComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.stockMovement.default'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const stockMovementPopupRoute: Routes = [
    {
        path: 'stock-movement-new',
        component: StockMovementPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.stockMovement.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'stock-movement/:id/edit',
        component: StockMovementPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.stockMovement.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'stock-movement/:id/delete',
        component: StockMovementDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.stockMovement.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
