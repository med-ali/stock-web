import {
    StockMovementService,
    StockMovementPopupService,
    StockMovementComponent,
    StockMovementDetailComponent,
    StockMovementDialogComponent,
    StockMovementPopupComponent,
    StockMovementDeletePopupComponent,
    StockMovementDeleteDialogComponent,
    stockMovementRoute,
    stockMovementPopupRoute,
    StockMovementResolvePagingParams,
} from './';

export const stockMovement_declarations = [
    StockMovementComponent,
    StockMovementDetailComponent,
    StockMovementDialogComponent,
    StockMovementDeleteDialogComponent,
    StockMovementPopupComponent,
    StockMovementDeletePopupComponent,
];

export const stockMovement_entryComponents = [
    StockMovementComponent,
    StockMovementDialogComponent,
    StockMovementPopupComponent,
    StockMovementDeleteDialogComponent,
    StockMovementDeletePopupComponent,
];

export const stockMovement_providers = [
    StockMovementService,
    StockMovementPopupService,
    StockMovementResolvePagingParams,
];
