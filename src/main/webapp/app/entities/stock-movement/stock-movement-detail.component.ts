import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { StockMovement } from './stock-movement.model';
import { StockMovementService } from './stock-movement.service';

@Component({
    selector: 'jhi-stock-movement-detail',
    templateUrl: './stock-movement-detail.component.html'
})
export class StockMovementDetailComponent implements OnInit, OnDestroy {

    stockMovement: StockMovement;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private stockMovementService: StockMovementService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInStockMovements();
    }

    load(id) {
        this.stockMovementService.find(id).subscribe((stockMovement) => {
            this.stockMovement = stockMovement;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInStockMovements() {
        this.eventSubscriber = this.eventManager.subscribe(
            'stockMovementListModification',
            (response) => this.load(this.stockMovement.id)
        );
    }
}
