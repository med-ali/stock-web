import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { StockMovement } from './stock-movement.model';
import { StockMovementPopupService } from './stock-movement-popup.service';
import { StockMovementService } from './stock-movement.service';

@Component({
    selector: 'jhi-stock-movement-delete-dialog',
    templateUrl: './stock-movement-delete-dialog.component.html'
})
export class StockMovementDeleteDialogComponent {

    stockMovement: StockMovement;
    alerts = [];
    constructor(private alertService: JhiAlertService,
        private stockMovementService: StockMovementService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }
    addErrorAlert(message, key?, data?) {
        key = (key && key !== null) ? key : message;
        this.alerts.push(
            this.alertService.addAlert(
                {
                    type: 'danger',
                    msg: key,
                    params: data,
                    timeout: 5000,
                    toast: this.alertService.isToast(),
                    scoped: true
                },
                this.alerts
            )
        );
    }
    confirmDelete(id: number) {
        this.stockMovementService.delete(id).subscribe((response) => {
            if (response["status"] == 200){
                this.eventManager.broadcast({
                    name: 'stockMovementListModification',
                    content: 'Deleted an StockMovement'
                });
                this.activeModal.dismiss(true);
            }
            else{
                this.addErrorAlert('Delete not reachable', 'error.entity.delete.reachable');
            } 
        });
    }
}

@Component({
    selector: 'jhi-stock-movement-delete-popup',
    template: ''
})
export class StockMovementDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private stockMovementPopupService: StockMovementPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.stockMovementPopupService
                .open(StockMovementDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
