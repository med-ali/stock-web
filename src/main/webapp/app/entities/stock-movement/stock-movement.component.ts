import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService, JhiLanguageService } from 'ng-jhipster';

import { StockMovement, SMState } from './stock-movement.model';
import { StockMovementService } from './stock-movement.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, PrintService, SpinnerService } from '../../shared';
import { NgxBarcodeComponent } from 'ngx-barcode';
import { Client } from '../client';
import swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'jhi-stock-movement',
    templateUrl: './stock-movement.component.html'
})
export class StockMovementComponent implements OnInit, OnDestroy {

    currentAccount: any;
    stockMovements: StockMovement[];
    selectedStockMovements: StockMovement[];
    etiquettes: any[];
    etiquetteToPrint: any;
    listToPrint = [];
    smState = SMState;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    annulled: boolean;
    fromMovement: any;
    toMovement: any;
    status: string;
    selectedClient: Client;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    showFilter = false;
    etiquetteModal = false;
    checkall: boolean;
    @ViewChild('wholeBarCode') wholeBarCode: ElementRef;
    @ViewChild('ngxBarCode') ngxBarCode: NgxBarcodeComponent;
    barcodeValue: string;

    constructor(
        private languageService: JhiLanguageService,
        private stockMovementService: StockMovementService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private printService: PrintService,
        private translateService: TranslateService,
        private spinnerService: SpinnerService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = false;
            this.predicate = 'movementDate';
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    ngOnInit() {
        this.status = 'All';
        this.checkall = false;
        this.loadAll();
        this.selectedStockMovements = [];
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInStockMovements();
    }

    displayAdvanced() {
        this.showFilter = !this.showFilter;
    }

    advancedSearch() {
        const params: any = {
            page: this.page - 1,
            query: this.currentSearch,
            fromMovement: this.fromMovement,
            toMovement: this.toMovement,
            annulled: this.annulled,
            size: this.itemsPerPage,
            sort: this.sort()
        };
        if (this.status !== 'All') {
            params['status'] = this.status;
        }
        if (this.selectedClient) {
            params['client'] = this.selectedClient.id;
        }
        this.stockMovementService.search(params).subscribe(
            (res) => this.onSuccess(res.body, res.headers),
            (res) => this.onError(res.body)
        );
    }
    loadAll() {
        this.checkall = false;
        if (this.currentSearch || this.fromMovement || this.toMovement || this.annulled || this.status !== 'All' || this.selectedClient) {
            this.advancedSearch();
            return;
        }
        this.stockMovementService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res) => this.onSuccess(res.body, res.headers),
            (res) => this.onError(res.body)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/stock-movement'], {
            queryParams:
                {
                    page: this.page,
                    size: this.itemsPerPage,
                    search: this.currentSearch,
                    sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
                }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/stock-movement', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query && (this.status === 'All') && !this.fromMovement && !this.toMovement && !this.annulled && !this.selectedClient) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        const params: any = {
            search: this.currentSearch,
            fromMovement: this.fromMovement,
            toMovement: this.toMovement,
            annulled: this.annulled,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        };
        if (this.status !== 'All') {
            params['status'] = this.status;
        }
        if (this.selectedClient) {
            params['client'] = this.selectedClient.id;
        }
        this.router.navigate(['/stock-movement', params]);
        this.advancedSearch();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: StockMovement) {
        return item.id;
    }
    registerChangeInStockMovements() {
        this.eventSubscriber = this.eventManager.subscribe('stockMovementListModification', (response) => this.loadAll());
        this.eventSubscriber = this.eventManager.subscribe('stockMovementLoadEtiquette', (response) => {this.loadEtiquette(response['content'].id)
          
        });
    }
    loadEtiquette(idStock: any) {
        this.stockMovementService.getStockMovementEtiquette(idStock).subscribe((data) => {
            this.etiquettes = data.body;
            this.listToPrint = [];
            if (this.etiquettes.length > 0) {
                this.openMyModal('etiquetteModal');
            } else {
                this.openBasicSwal('etiquette');
            }
        });
    }
    loadEtiquettePdf(idStock: any) {
        this.stockMovementService.getStockMovementEtiquettePdf(idStock,this.languageService.currentLang).subscribe((value) => {
            const linkSource = 'data:application/pdf;base64,' + value["res"];
            const downloadLink = document.createElement("a");
            const fileName = "ticket.pdf";
            downloadLink.href = linkSource;
            downloadLink.download = fileName;
           window.open("data:application/pdf;base64, " + value["res"], '', "height=600,width=800");
           // downloadLink.click();
            if (downloadLink.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                downloadLink.setAttribute("href", window.URL.createObjectURL(linkSource));
                downloadLink.setAttribute("download", fileName);
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
            } else {
                console.log('Pdf export only works in Chrome, Firefox, and Opera.');
                var fileBlob = new Blob([value['res']], {type: 'application/pdf'});
                window.navigator.msSaveBlob(fileBlob, fileName); 
            }
        });
    }

    openBasicSwal(entity) {
        const title = this.translateService.instant('entity.' + entity + '.swal.title');
        const text = this.translateService.instant('entity.' + entity + '.swal.text');
        swal({
            'title': title,
            'text': text,
            'type': 'error'

        }).catch(swal.noop);
    }

    openMyModal(event) {
        if (event === 'etiquetteModal') {
            this.etiquetteModal = true;
        }
        document.querySelector('#' + event).classList.add('md-show');
    }

    closeModal(event) {
        this.etiquetteModal = false;
        (document.querySelector('#' + event).parentElement.parentElement).classList.remove('md-show');
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        //this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.stockMovements = data;
    }
    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    toggleSelection(item) {
        console.log('dededede'+item.id)
        const idx = this.listToPrint.indexOf(item);
        if (idx > -1) {
            this.listToPrint.splice(idx, 1);
        } else {
            this.listToPrint.push(item);
        }
    }

    toggleCheckAll() {
        this.listToPrint = this.etiquettes;
    }

    toggleUncheckAll() {
        this.listToPrint = [];
    }

    selectOrDeselectAllStockMovements() {
        this.checkall = !this.checkall;
        if (this.checkall) {
            this.selectedStockMovements = this.stockMovements;
            for (let i = 0; i < this.stockMovements.length; i++) {
                this.stockMovements[i].checked = true;
            }
        } else {
            this.selectedStockMovements = [];
            for (let i = 0; i < this.stockMovements.length; i++) {
                this.stockMovements[i].checked = false;
            }
        }
    }

    updateSelectedStockMovements(stockMovement) {
        if (!stockMovement.checked) {
            this.selectedStockMovements.splice(this.renderIndex(stockMovement, this.selectedStockMovements), 1);
        } else {
            this.selectedStockMovements.push(stockMovement);
        }
        if (this.selectedStockMovements.length === this.stockMovements.length) {
            this.checkall = true;
        } else {
            this.checkall = false;
        }
    }

    renderIndex(stockMovement, stockMovements: StockMovement[]): number {
        for (let i = 0; i < stockMovements.length; i++) {
            const currentStockMovement = stockMovements[i];
            if (currentStockMovement.id === stockMovement.id) {
                return i;
            }
        }
        return -1;
    }

    async printSelectedEtiquettes() {
        this.spinnerService.show();
        const barcodeWindow = window.open('', '_blank', 'width=900,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no')
        let element = '';
        for (let i = 0; i < this.listToPrint.length; i++) {
            this.etiquetteToPrint = this.listToPrint[i];
            element += '<div style="text-align: center;line-height: 1;">';
            this.barcodeValue = this.etiquetteToPrint.code;
            element = await this.appendBarCode(element, this.ngxBarCode);
            element += '<p> <strong>' + this.etiquetteToPrint.stockMovement.product.code + '</strong> </p>' +
                '<p>' + this.etiquetteToPrint.stockMovement.description + '</p>' +
                '<p>' + this.etiquetteToPrint.stockMovement.client.companyName + '</p>';
            element += '</div>';
            if (i < this.listToPrint.length - 1) {
                element += '<div class="page-break"></div>';
            }
        }
        this.spinnerService.hide();
        setTimeout(() => {
            this.printService.print(element, barcodeWindow);
        });
    }
    exist(tab:any,el:any){
        console.log(el+"tab :"+tab)
        tab.forEach(e => {
            if(e == el){
                return true;
            }
        });
        return false;
    }
    printSelectedEtiquettePdf() {
        console.log("data :"+this.listToPrint[0].id)
        let listToPrintId = [];
        this.listToPrint.forEach(element => {
            console.log("data id:"+element.id)
            if(!this.exist(listToPrintId,element.id)){
                listToPrintId.push(element.id)
            }
        });
        this.stockMovementService.getSelectedEtiquettesPdf(listToPrintId,this.languageService.currentLang).subscribe((value) => {
            const linkSource = 'data:application/pdf;base64,' + value["res"];
            const downloadLink = document.createElement("a");
            const fileName = "ticket.pdf";
            console.log("data :"+value["res"])
            downloadLink.href = linkSource;
            downloadLink.download = fileName;
           // window.open("data:application/pdf;base64, " + value["res"], '', "height=600,width=800");
            //downloadLink.click();
            if (downloadLink.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                //downloadLink.setAttribute("href", window.URL.createObjectURL(linkSource));
                downloadLink.setAttribute("download", fileName);
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
            } else {
                console.log('Pdf export only works in Chrome, Firefox, and Opera.');
                var fileBlob = new Blob([value['res']], {type: 'application/pdf'});
                window.navigator.msSaveBlob(fileBlob, fileName); 
            }
        });
    }
    loadMvtPdf() {
        const params: any = {
            page: this.page - 1,
            query: this.currentSearch,
            fromMovement: this.fromMovement,
            toMovement: this.toMovement,
            annulled: this.annulled,
            size: this.itemsPerPage,
            sort: this.sort()
        };
        if (this.status !== 'All') {
            params['status'] = this.status;
        }
        this.stockMovementService.printReportMvts(this.stockMovements,params).subscribe((value) => {
            //this.product = product;
            const linkSource = 'data:application/pdf;base64,' + value['res'];
            const downloadLink = document.createElement("a");
            const fileName = "stockMovements.pdf";

            downloadLink.href = linkSource;
            downloadLink.download = fileName;
            //window.open("data:application/pdf;base64, " + value['res'], '', "height=600,width=800");
            //downloadLink.click();
            if (downloadLink.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                //downloadLink.setAttribute("href", window.URL.createObjectURL(linkSource));
                downloadLink.setAttribute("download", fileName);
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
            } else {
                console.log('Pdf export only works in Chrome, Firefox, and Opera.');
                var fileBlob = new Blob([value['res']], {type: 'application/pdf'});
                window.navigator.msSaveBlob(fileBlob, fileName); 
            }
        });
    }
    async appendBarCode(element, ngx: NgxBarcodeComponent) {
        await this.delay(20);
        element += ngx.bcElement.nativeElement.innerHTML;
        return element;
    }

    delay(ms: number) {
        return new Promise((resolve) => setTimeout(resolve, ms));
    }

    showWindowPrintForAll() {
        this.spinnerService.show();
        const barcodeWindow = window.open('', '_blank', 'width=900,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no')
        for (let i = 0; i < this.selectedStockMovements.length; i++) {
            let element = '<div style="text-align: center;line-height: 1;">';
            this.etiquetteToPrint = this.listToPrint[i];
            this.barcodeValue = this.etiquetteToPrint.code;
            element += this.ngxBarCode.bcElement.nativeElement.innerHTML;
            element += this.wholeBarCode.nativeElement.innerHTML;
            element += '</div>';
            setTimeout(() => {
                this.spinnerService.hide();
                this.printService.print(element, barcodeWindow);
            }, 100);
        }
    }

    showWindowPrint(stockMovement) {
        this.barcodeValue = stockMovement.code;
        const barcodeWindow = window.open('', '_blank', 'width=900,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no')
        setTimeout(() => {
            this.printService.print(this.wholeBarCode.nativeElement.innerHTML, barcodeWindow);
        });
    }
}
