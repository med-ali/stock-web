import { BaseEntity } from './../../shared';
import { OrderDetails } from '../order-details';
import { Article } from '../article';
import { Product } from '../product';

export enum SMState {
    loading = 'LO',
    cons_unloading = 'CUN',
    exp_unloading = 'EUN',
    transfert = 'TR',
    inventaire = 'INV'
}

export class StockMovement implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public description?: string,
        public itemCode?: string,
        public quantity?: number,
        public quantityInventory?: number,
        public movementDate?: any,
        public effective?: number,
        public presume?: number,
        public movementType?: SMState,
        public annulled?: boolean,
        public inventoryAnnulled?: boolean,
        public lastAction?: boolean,
        public note?: string,
        public product?: any,
        public shelf?: any,
        public destinationShelf?: any,
        public article?: any,
        public kit?: any,
        public deliveredEquipement?: any,
        public stockMovement?: any,
        public client?: any,
        public checked?: boolean,
    ) {
        this.inventoryAnnulled = false;
        this.annulled = false;
        this.lastAction = false;
    }
}
