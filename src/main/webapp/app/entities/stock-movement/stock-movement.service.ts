import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { StockMovement } from './stock-movement.model';
import { createRequestOption } from '../../shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { JhiDateUtils } from 'ng-jhipster';
import { DeliveredEquipement } from '../delivered-equipement';

@Injectable()
export class StockMovementService {

    private resourceUrl = SERVER_API_URL + 'api/stock-movements';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/stock-movements';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(stockMovement: StockMovement): Observable<StockMovement> {
        this.convertDate(stockMovement);
        return this.http.post<StockMovement>(this.resourceUrl, stockMovement);
    }

    update(stockMovement: StockMovement): Observable<StockMovement> {
        this.convertDate(stockMovement);
        return this.http.put<StockMovement>(this.resourceUrl, stockMovement);
    }

    find(id: number): Observable<StockMovement> {
        return this.http.get<StockMovement>(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<HttpResponse<StockMovement[]>> {
        const options = createRequestOption(req);
        return this.http.get<StockMovement[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    getItemByCode(code: string): Observable<HttpResponse<DeliveredEquipement>> {
        return this.http.get<DeliveredEquipement>(`${this.resourceUrl}/getItemByCode?code=${code}`, { observe: 'response' });
    }

    getStockMovementByCode(code: string, shelfCode: string): Observable<HttpResponse<StockMovement>> {
        return this.http.get<StockMovement>(`${this.resourceUrl}/getStockMovementByCode?code=${code}&shelfCode=${shelfCode}`, { observe: 'response' });
    }

    getStockMovementEtiquette(idStock: number): Observable<HttpResponse<any[]>> {
        return this.http.get<any[]>(`${this.resourceUrl}/${idStock}/etiquettes`, { observe: 'response' });
    }
    getStockMovementEtiquettePdf(idStock: number,lang:string): Observable<any> {
        return this.http.get<any>(`${this.resourceUrl}/${idStock}/etiquettes/pdf?lang=`+lang);
    }

    getEtiquettePdf(id: number,lang:string): Observable<any> {
        return this.http.get<any>(`${this.resourceUrl}/${id}/etiquette/pdf?lang=`+lang);
    }
    getSelectedEtiquettesPdf(idTicketTab: number[],lang:string): Observable<any> {
        return this.http.post<any>(`${this.resourceUrl}/selected/etiquettes/pdf?lang=`+lang,idTicketTab);
    }
    getStockAvailability(code: string, shelfCode: string): Observable<HttpResponse<any[]>> {
        return this.http.get<any[]>(`${this.resourceUrl}/getStockAvailability?code=${code}&shelfCode=${shelfCode}`, { observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<HttpResponse<StockMovement[]>> {
        const options = this.createAdvancedSearchOption(req);
        return this.http.get<StockMovement[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }

    createAdvancedSearchOption = (req?: any): HttpParams => {
        let Params = new HttpParams();
        if (req) {
            // Begin assigning parameters
            if (req.page) {
                Params = Params.append('page', req.page)
            }
            if (req.size) {
                Params = Params.append('size', req.size)
            }
            if (req.query) {
                Params = Params.append('q', req.query)
            }
            if (req.filter) {
                Params = Params.append('filter', req.filter)
            }
            if (req.sort) {
                for (const s of req.sort) {
                    Params = Params.append('sort', s);
                }
            }
            if (req.status) {
                Params = Params.append('status', req.status)
            }
            if (req.fromMovement) {
                Params = Params.append('fromMov', req.fromMovement)
            }
            if (req.toMovement) {
                Params = Params.append('toMov', req.toMovement)
            }
            if (req.annulled) {
                Params = Params.append('annulled', req.annulled)
            }
            if (req.client) {
                Params = Params.append('client', req.client)
            }
        }
        return Params;
    };

    convertDate(stockMovement: StockMovement) {
        stockMovement.movementDate = this.dateUtils.toDate(stockMovement.movementDate);
    }
    
    printReportMvts(stockMovements: any[],req?: any): Observable<any> {
        const options = this.createAdvancedSearchOption(req);
        console.log(options)
        return this.http.post<any>
            (`${this.resourceUrl}/print`, stockMovements, { params: options });
    }
}
