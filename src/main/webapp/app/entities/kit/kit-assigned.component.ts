import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { Kit } from './kit.model';
import { KitService } from './kit.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, BaseEntity } from '../../shared';
import { Supplier, SupplierService } from '../supplier';
import { Product, ProductService } from '../product';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { KitAssignedProductComponent } from './kit-assigned-product.component';
@Component({
    selector: 'jhi-kit-assigned',
    templateUrl: './kit-assigned.component.html'
})
export class KitAssignedComponent implements OnInit, OnDestroy {

    currentAccount: any;
    kits: any;
    productId: number;
    product: Product;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    constructor(private modalService: NgbModal,
        private kitService: KitService,
        private productService: ProductService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
    }

    loadAssignedToProduct() {
        this.kitService.queryAssignedToProduct(this.productId, {
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res) => this.onSuccess(res.body, res.headers),
            (res) => this.onError(res.body)
            );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/kit/product/' + this.productId], {
            queryParams:
                {
                    page: this.page,
                    size: this.itemsPerPage,
                    sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
                }
        });
        this.loadAssignedToProduct();
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/kit/product/' + this.productId, {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAssignedToProduct();
    }
    assignedFuction(product:Product) {
        const modalRef = this.modalService.open(KitAssignedProductComponent, {windowClass:  'fadeStyle'});
        modalRef.componentInstance.product = product; 
      }
    ngOnInit() {
        this.eventSubscriber = this.activatedRoute.params.subscribe((params) => {
            this.productId = params['productId'];
            this.productService.find(this.productId).subscribe((product) => {
                this.product = product;
            });
            this.loadAssignedToProduct();
        });
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInShelves();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Kit) {
        return item.id;
    }
    registerChangeInShelves() {
        this.eventSubscriber = this.eventManager.subscribe('kitListModification',
            (response) => this.loadAssignedToProduct());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        //this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.kits = data;
    }
    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
