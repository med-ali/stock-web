import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Kit } from './kit.model';
import { KitService } from './kit.service';

@Component({
    selector: 'jhi-kit-detail',
    templateUrl: './kit-detail.component.html'
})
export class KitDetailComponent implements OnInit, OnDestroy {

    kit: Kit;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private kitService: KitService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInKits();
    }

    load(id) {
        this.kitService.find(id).subscribe((kit) => {
            this.kit = kit;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInKits() {
        this.eventSubscriber = this.eventManager.subscribe(
            'kitListModification',
            (response) => this.load(this.kit.id)
        );
    }
}
