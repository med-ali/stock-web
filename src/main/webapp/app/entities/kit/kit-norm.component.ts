import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiParseLinks } from 'ng-jhipster';

import { KitNormalisation } from './kit-normalisation.model';
import { Kit } from './kit.model';
import { KitService } from './kit.service';
import { KitPopupService } from './kit-popup.service';
import { Product, ProductService } from '../product';
import { ResponseWrapper, ITEMS_PER_FILTER_PAGE } from '../../shared';
import swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs/Subscription';
import { NgForm } from '@angular/forms';

@Component({
    selector: 'jhi-kit-norm',
    templateUrl: './kit-norm.component.html',
    styleUrls: ['../../../content/scss/kitnorm.scss']
})
export class KitNormComponent implements OnInit {

    kit: Kit;
    kitNormalisations: KitNormalisation[];
    kitNormalisation: KitNormalisation;
    eventSubscriber: Subscription;

    products: Product[];
    productSearch: string;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any; 
    page: any;
    predicate: any;
    previousPage: any;
    links: any;
    reverse: any;
    pager: any = {};

    edit: boolean;
    productModal: boolean;
    hasSameType: boolean;

    @ViewChild('editForm')
    editForm: NgForm;

    constructor(
        public activeModal: NgbActiveModal,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private kitService: KitService,
        private productService: ProductService,
        private eventManager: JhiEventManager,
        private route: ActivatedRoute,
        private translateService: TranslateService
    ) {
        this.itemsPerPage = ITEMS_PER_FILTER_PAGE;
        this.page = 1;
        this.previousPage = 1;
        this.reverse = true;
        this.predicate = 'description';
    }

    ngOnInit() {
        this.productModal = false;
        this.kitNormalisation = {};
        this.kitNormalisations = [];
        this.edit = false;
        if (this.kit.isNormalised) {
            this.load(this.kit.id);
        }
    }

    load(id) {
        this.kitService.queryNormByKit(id).subscribe(
            (res) => this.onSuccess(res.body, res.headers),
            (res) => this.onError(res.body)
        );
    }

    clear() {
        this.activeModal.dismiss();
    }

    showForm(kitNormalisation) {
        if (kitNormalisation) {
            this.edit = true;
            this.kitNormalisation = kitNormalisation;
        } else {
            this.kitNormalisation = {};
        }
    }

    save() {
        if (this.kitNormalisation.id !== undefined) {
            this.subscribeToSaveResponse(
                this.kitService.updateNorm(this.kitNormalisation));
        } else {
            this.kitNormalisation.kit = this.kit;
            this.kitNormalisation.isActive = true;
            this.subscribeToSaveResponse(
                this.kitService.createNorm(this.kitNormalisation));
        }
        this.edit = false;
    }

    confirmDelete(id: number) {
        this.kitService.deleteNorm(id).subscribe((response) => {
            this.load(this.kit.id);
            this.eventManager.broadcast({ name: 'kitListModification', content: 'Deleted a kit' });
        });
    }

    private subscribeToSaveResponse(result: Observable<KitNormalisation>) {
        result.subscribe((res: KitNormalisation) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: KitNormalisation) {
        this.kitNormalisation = {};
        this.load(this.kit.id);
        this.eventManager.broadcast({ name: 'kitListModification', content: 'OK' });
        this.editForm.form.markAsUntouched();
        this.editForm.form.markAsPristine();
    }

    private onSaveError() {
        this.activeModal.close();
    }

    private onSuccess(data, headers) {
        this.kitNormalisations = data;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackKitById(index: number, item: Kit) {
        return item.id;
    }

    trackProductById(index: number, item: Product) {
        return item.id;
    }

    openConfirmsSwal(id) {
        const title = this.translateService.instant('action.delete.swal.title');
        const text = this.translateService.instant('action.delete.swal.text');
        const btnconfirm = this.translateService.instant('action.delete.swal.btnconfirm');
        const btnclose = this.translateService.instant('action.delete.swal.btnclose');
        const msg = this.translateService.instant('action.delete.swal.msg');
        const success = this.translateService.instant('action.delete.swal.success');
        swal({
            'title': title,
            'text': text,
            'type': 'warning',
            'showCancelButton': true,
            'confirmButtonColor': '#3085d6',
            'cancelButtonColor': '#d33',
            'cancelButtonText': btnclose,
            'confirmButtonText': btnconfirm
        }).then(function(result) {
            if (result.value) {
            this.confirmDelete(id);
            swal(
                msg,
                success,
                'success'
            );
            }
        }.bind(this)).catch(swal.noop);
    }

    openMyModal(event) {
        if (event === 'productModal') {
            this.productModal = true;
            this.loadProducts();
        }
        document.querySelector('#' + event).classList.add('md-show');
    }

    loadProducts() {
        const params: any = {
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }
        if (this.productSearch) {
            params['query'] = this.productSearch;
        }
        this.productService.listProductsNotAssociatedToKitsBySupplier(this.kit.supplier.id, params)
            .subscribe(
                (res) => this.onProductSuccess(res.body, res.headers),
                (res) => this.onError(res.body)
            );
    }

    private onProductSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.products = data;
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.loadProducts();
        }
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    closeModal(event) {
        this.resetParameters();
        this.productModal = false;
        (document.querySelector('#' + event).parentElement.parentElement).classList.remove('md-show');
    }

    resetParameters() {
        this.page = 1;
        this.predicate = 'description';
        this.reverse = true;
        this.productSearch = '';
    }

    closeMyModalEvent(event) {
        ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
    }

    assignProduct(product) {
        if (this.isYetNormalised(product, this.kitNormalisations)) {
            this.kitNormalisation.product = null;
            this.jhiAlertService.error('error.kitnorm');
        } else {
            this.kitNormalisation.product = product;
        }
        this.closeModal('closeProductBtn');
    }

    isYetNormalised(obj: Product, array: KitNormalisation[]) {
        if (array.length > 0) {
            for (const norm of array) {
                if (norm.product.id === obj.id) {
                    return true;
                }
            }
        }
        return false;
    }
}

@Component({
    selector: 'jhi-kit-norm-popup',
    template: ''
})
export class KitNormPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private kitPopupService: KitPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.kitPopupService
                .open(KitNormComponent as Component, params['id'], true);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
