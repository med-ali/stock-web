import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Kit } from './kit.model';
import { KitService } from './kit.service';

@Injectable()
export class KitPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private kitService: KitService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any, norm?: boolean): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.kitService.find(id).subscribe((kit) => {
                    if (!norm) {
                        kit.expirationDate = this.datePipe
                            .transform(kit.expirationDate, 'yyyy-MM-ddTHH:mm:ss');
                        kit.startValidation = this.datePipe
                            .transform(kit.startValidation, 'yyyy-MM-ddTHH:mm:ss');
                        kit.endValidation = this.datePipe
                            .transform(kit.endValidation, 'yyyy-MM-ddTHH:mm:ss');
                    }
                    this.ngbModalRef = this.kitModalRef(component, kit, norm);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    const kit: Kit = new Kit();
                    kit.startValidation = this.datePipe
                        .transform(new Date(), 'yyyy-MM-ddTHH:mm:ss');
                    kit.endValidation = this.datePipe
                        .transform('2028-12-31T23:59:59+01:00', 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.kitModalRef(component, kit, norm);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    kitModalRef(component: Component, kit: Kit, norm: boolean): NgbModalRef {
        let modalRef = null;
        if (norm) {
            modalRef = this.modalService.open(component, { windowClass: 'modal-norm', backdrop: 'static' });
        } else {
            modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static' });
        }
        modalRef.componentInstance.kit = kit;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
