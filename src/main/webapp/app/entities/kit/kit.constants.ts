import {
    KitService,
    KitPopupService,
    KitComponent,
    KitAssignedComponent,
    KitDetailComponent,
    KitDialogComponent,
    KitPopupComponent,
    KitDeletePopupComponent,
    KitDeleteDialogComponent,
    kitRoute,
    kitPopupRoute,
    KitResolvePagingParams,
    KitNormComponent,
    KitNormPopupComponent,
    KitAssignedProductComponent
} from './';

export const kit_declarations = [
    KitComponent,
    KitAssignedComponent,
    KitNormComponent,
    KitDetailComponent,
    KitDialogComponent,
    KitDeleteDialogComponent,
    KitPopupComponent,
    KitDeletePopupComponent,
    KitNormPopupComponent,
    KitAssignedProductComponent
];

export const kit_entryComponents = [
    KitComponent,
    KitAssignedComponent,
    KitNormComponent,
    KitDialogComponent,
    KitPopupComponent,
    KitDeleteDialogComponent,
    KitDeletePopupComponent,
    KitNormPopupComponent,
    KitAssignedProductComponent
];

export const kit_providers = [
    KitService,
    KitPopupService,
    KitResolvePagingParams,
];
