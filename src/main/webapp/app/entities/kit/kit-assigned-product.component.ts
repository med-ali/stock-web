import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { Observable } from 'rxjs/Rx';
import { Kit } from './kit.model';
import { KitService } from './kit.service';
import { ITEMS_PER_PAGE, Principal, BaseEntity } from '../../shared';
import { Supplier, SupplierService } from '../supplier';
import { Product, ProductService } from '../product';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { KitNormalisation } from './kit-normalisation.model';
import { ResponseWrapper, ITEMS_PER_FILTER_PAGE, PagerService } from '../../shared';
import { StockMovement, SMState } from '../stock-movement/stock-movement.model';
import { StockMovementPopupService } from '../stock-movement/stock-movement-popup.service';
import { StockMovementService } from '../stock-movement/stock-movement.service';
import { Shelf } from '../shelf/shelf.model';
import { ShelfService } from '../shelf/shelf.service';
@Component({
    selector: 'jhi-article-assigned',
    templateUrl: './kit-assigned-product.component.html'
})
export class KitAssignedProductComponent implements OnInit, OnDestroy {

    currentAccount: any;
    //articles: any;
   
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    
    
    ///:::::://///
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    productId: number;
    product: Product;
    kit: Kit;
    kitNormalisation: KitNormalisation;
    isSaving: boolean;
    kitModal: boolean;
    kitSearch: string;
    kits: Kit[];
    hasSameType: boolean;
    shelf:Shelf;
    stockMovement:StockMovement;
    shelfModal: boolean;
    quantity:number;
    stockMovements:StockMovement[];
    constructor( public activeModal: NgbActiveModal,
        private modalService: NgbModal,
        private kitService: KitService,
        private productService: ProductService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager
    ) {
        this.itemsPerPage = ITEMS_PER_FILTER_PAGE;
        this.page = 1;
        this.previousPage = 1;
        this.reverse = true;
        this.predicate = 'description';
    }

    

    clear() {
        this.activeModal.dismiss('cancel');
    }

    openMyModal(event) {
        if (event === 'shelfModal') {
            this.shelfModal = true;
        }
        else if (event === 'kitModal') {
            this.kitModal = true;
            this.loadKits();
        }
        document.querySelector('#' + event).classList.add('md-show');
    }
    assignShelf(shelf) {
        this.shelf = shelf;
        this.stockMovement.shelf = shelf; 
        this.stockMovement.quantity = null;
        this.closeModal('closeShelfBtn');
    }
    loadKits() {
        const params: any = {
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }
        if (this.kitSearch) {
            params['query'] = this.kitSearch;
        }
        this.kitService.query(params)
            .subscribe(
                (res) => this.onSuccess(res.body, res.headers),
                (res) => this.onError(res.body)
            );
    }
    loadkitsAll() {
        const params: any = {
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }
        if (this.kitSearch) {
            params['query'] = this.kitSearch;
        }
        this.kitService.query(params)
            .subscribe(
                (res) => this.onSuccess(res.body, res.headers),
                (res) => this.onError(res.body)
            );
    }
    loadKitSearchs() {
        const params: any = {
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }
        if (this.kitSearch) {
            params['query'] = this.kitSearch;
        }
        this.kitService.search(params)
            .subscribe(
                (res) => this.onSuccess(res.body, res.headers),
                (res) => this.onError(res.body)
            );
    }
    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        
        return result;
    }

    private onSaveSuccess(result: KitNormalisation) {
        this.eventManager.broadcast({ name: 'kitListModification', content: 'OK' });
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.activeModal.close();
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
    private onSuccess(data, headers) {
        //this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.kits = data;
    }
    closeModal(event) {
        this.resetParameters();
        this.kitModal = false;
        (document.querySelector('#' + event).parentElement.parentElement).classList.remove('md-show');
    }
    resetParameters() {
        this.page = 1;
        this.predicate = 'description';
        this.reverse = true;
        this.kitSearch = '';
    }
    ngOnInit() {
        this.stockMovements = [];
        this.isSaving = false;
        this.kitModal = false;
        this.kitNormalisation = {};
        this.stockMovement = {};
        this.shelf = {};
        console.log(this.product)
       
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        }); 
    }
    assignkit(kit) {
        this.kit = kit;
        this.kitNormalisation.product = this.product;
        this.kitNormalisation.kit = kit;
        if (this.kit.measureUnit.type !== this.product.measureUnit.type) {
            this.hasSameType = true;
        } else { 
            this.hasSameType = false;
            this.kitNormalisation.conversionUnit = 0;
        }
        this.closeModal('closeKitBtn');
    }
    save() {
        const params: any = {
            qty: this.quantity
        }
        this.isSaving = true;
        this.kitNormalisation.product = this.product; 
        this.kitNormalisation.isActive = true;
        //this.kitNormalisation.conversionUnit = 0;
        console.log(this.kitNormalisation)
       this.subscribeToSaveResponse(
           this.kitService.createNormInventory(this.kitNormalisation,this.shelf.id,this.quantity));
        
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        const params_query: any = {
            page: this.page,
            size: this.itemsPerPage,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }
        if (this.kitSearch) {
            params_query['query'] = this.kitSearch;
        }
        this.loadKits()
    }
   /* 
    ngOnInit() {
        this.stockMovements = [];
        this.isSaving = false;
        this.articleModal = false;
        this.articleNormalisation = {};
        this.stockMovement = {};
        this.shelf = {};
        console.log(this.product)
       
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        }); 
    }
    save() {
        const params: any = {
            qty: this.quantity
        }
        this.isSaving = true;
        this.articleNormalisation.product = this.product; 
        this.articleNormalisation.isActive = true;
        this.articleNormalisation.conversionUnit = 0;
        console.log(this.articleNormalisation)
        this.stockMovement.shelf = this.shelf;
        this.stockMovement.article = this.article;
        this.stockMovement.effective = this.quantity;
        this.stockMovement.presume = 0;
        this.stockMovements.push(this.stockMovement)
        console.log(this.stockMovements)
       this.subscribeToSaveResponse(
           this.articleService.createNormInventory(this.articleNormalisation,this.shelf.id,this.quantity));
        
    }
    */
   private subscribeToSaveResponse(result: Observable<KitNormalisation>) {
    result.subscribe((res: KitNormalisation) =>
        this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }
    ngOnDestroy() {
         
    }

     
}
