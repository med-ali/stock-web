import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { KitComponent } from './kit.component';
import { KitDetailComponent } from './kit-detail.component';
import { KitPopupComponent } from './kit-dialog.component';
import { KitDeletePopupComponent } from './kit-delete-dialog.component';
import { KitAssignedComponent } from './kit-assigned.component';
import { KitNormPopupComponent } from './kit-norm.component';

@Injectable()
export class KitResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const kitRoute: Routes = [
    {
        path: 'kit',
        component: KitComponent,
        resolve: {
            'pagingParams': KitResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'entity.kit.default'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'kit/:id',
        component: KitDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'entity.kit.default'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'product/kit/:productId',
        component: KitAssignedComponent,
        resolve: {
            'pagingParams': KitResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'entity.kit.default'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const kitPopupRoute: Routes = [
    {
        path: 'kit-new',
        component: KitPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.kit.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'kit/:id/norm',
        component: KitNormPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.kit.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'kit/:id/edit',
        component: KitPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.kit.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'kit/:id/delete',
        component: KitDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.kit.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
