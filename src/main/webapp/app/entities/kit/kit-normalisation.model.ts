import { BaseEntity } from '../../shared';

export class KitNormalisation implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public normalisationUnit?: number,
        public conversionUnit?: number,
        public isActive?: boolean,
        public kit?: any,
        public product?: any,
    ) {
        this.isActive = false;
        this.normalisationUnit = 1;
        this.conversionUnit = 0;
    }
}
