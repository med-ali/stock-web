import { BaseEntity } from './../../shared';

export class Kit implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public description?: string,
        public minPackagingOrder?: number,
        public discount?: number,
        public quantityPerPackage?: number,
        public unitPrice?: number,
        public isExpired?: boolean,
        public numberOfDaysToExpired?: number,
        public expirationDate?: any,
        public startValidation?: any,
        public endValidation?: any,
        public isNew?: boolean,
        public note?: string,
        public isNormalised?: boolean,
        public packagingMethod?: any,
        public measureUnit?: any,
        public supplier?: any,
    ) {
        this.isExpired = false;
        this.isNew = false;
        this.isNormalised = false;
        this.minPackagingOrder = 1;
        this.quantityPerPackage = 1;
        this.numberOfDaysToExpired = 0;
    }
}
