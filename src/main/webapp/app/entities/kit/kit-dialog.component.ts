import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Kit } from './kit.model';
import { KitPopupService } from './kit-popup.service';
import { KitService } from './kit.service';
import { PackagingMethod, PackagingMethodService } from '../packaging-method';
import { MeasureUnit, MeasureUnitService } from '../measure-unit';
import { Supplier, SupplierService } from '../supplier';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-kit-dialog',
    templateUrl: './kit-dialog.component.html'
})
export class KitDialogComponent implements OnInit {

    kit: Kit;
    isSaving: boolean;
    expired: any;
    defaultPackagingMethod: PackagingMethod;
    supplierModal: boolean;
    packagingMethodModal: boolean;
    measureUnitModal: boolean;
    step = 1;
    descriptionSupplier:string;
    descriptionPackagingMethod:string;
    descriptionMeasureUnit:string;
    existCode: boolean;
    constructor(
        public activeModal: NgbActiveModal,
        private packagingMethodService: PackagingMethodService,
        private jhiAlertService: JhiAlertService,
        private kitService: KitService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.expired = 'no';
        this.supplierModal = this.packagingMethodModal = this.measureUnitModal = false;
        //this.loadDefaultPackagingMethod();
        if(this.kit.supplier){
            this.descriptionSupplier = this.kit.supplier.companyName + "-" + this.kit.supplier.email;
        }
        
        if(this.kit.packagingMethod){
            this.descriptionPackagingMethod = this.kit.packagingMethod.code + "-" + this.kit.packagingMethod.description;
        }
        if(this.kit.measureUnit){
            this.descriptionMeasureUnit = this.kit.measureUnit.code + "-" + this.kit.measureUnit.description;
        }
    } 
    loadDefaultPackagingMethod() {
        this.packagingMethodService.getDefaultPackagingMethod().subscribe((data) => {
            if (data) {
                this.defaultPackagingMethod = data;
                this.kit.packagingMethod = this.defaultPackagingMethod;
            }
        });

    };

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() { 
        this.kitService.checkCode(this.kit.code).subscribe((value) => { 
            
            if (value==true && this.kit.id == undefined){
                this.existCode = true;
            }
            else{
                this.existCode = false;
                this.isSaving = true;
                if (this.kit.id !== undefined) {
                    this.subscribeToSaveResponse(
                        this.kitService.update(this.kit));
                } else {
                    this.subscribeToSaveResponse(
                        this.kitService.create(this.kit));
                }
                this.clear()
            }
            },
            (error) => { 
            console.log("error"+error)
        })     
    }

    public expiredChange(event:string) {
        if (event === 'yes') {
            this.kit.isExpired = true;
        } else if (event === 'no') {
            this.kit.isExpired = false;
            this.kit.numberOfDaysToExpired = null;
        }
    }

    private subscribeToSaveResponse(result: Observable<Kit>) {
        result.subscribe((res: Kit) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Kit) {
        this.eventManager.broadcast({ name: 'kitListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    openMyModal(event) {
        if (event === 'packagingMethodModal') {
            this.packagingMethodModal = true;
        } else if (event === 'measureUnitModal') {
            this.measureUnitModal = true;
        } else if (event === 'supplier2Modal') {
            this.supplierModal = true;
        }
        document.querySelector('#' + event).classList.add('md-show');
    }

    closeModal(event) {
        this.supplierModal = this.packagingMethodModal = this.measureUnitModal = false;
        (document.querySelector('#' + event).parentElement.parentElement).classList.remove('md-show');
    }

    closeMyModalEvent(event) {
        ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
    }

    assignSupplier(supplier) {
        this.kit.supplier = supplier;
        this.descriptionSupplier = this.kit.supplier.companyName + "-" + this.kit.supplier.email;
        this.closeModal('closeSupplier2Btn');
    }

    assignPackagingMethod(packagingMethod) {
        this.kit.packagingMethod = packagingMethod;
        this.descriptionPackagingMethod = this.kit.packagingMethod.code + "-" + this.kit.packagingMethod.description;
        this.closeModal('closePackagingMethodBtn');
    }

    assignMeasureUnit(measureunit) {
        this.kit.measureUnit = measureunit;
        this.descriptionMeasureUnit = this.kit.measureUnit.code + "-" + this.kit.measureUnit.description;
        this.closeModal('closeMeasureUnitBtn');
    }
}

@Component({
    selector: 'jhi-kit-popup',
    template: ''
})
export class KitPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private kitPopupService: KitPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.kitPopupService
                    .open(KitDialogComponent as Component, params['id']);
            } else {
                this.kitPopupService
                    .open(KitDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
