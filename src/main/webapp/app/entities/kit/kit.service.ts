import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Kit } from './kit.model';
import { createRequestOption } from '../../shared';
import { KitNormalisation } from './kit-normalisation.model';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { JhiDateUtils } from 'ng-jhipster';

@Injectable()
export class KitService {

    private resourceUrl = SERVER_API_URL + 'api/kits';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/kits';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(kit: Kit): Observable<Kit> {
        this.convertDate(kit);
        return this.http.post<Kit>(this.resourceUrl, kit);
    }

    checkCode(code: string): Observable<Kit> {
        return this.http.get<Kit>(this.resourceUrl+"/checkcode/?code="+code);
    }

    update(kit: Kit): Observable<Kit> {
        this.convertDate(kit);
        return this.http.put<Kit>(this.resourceUrl, kit);
    }

    find(id: number): Observable<Kit> {
        return this.http.get<Kit>(`${this.resourceUrl}/${id}`);
    }

    createNormInventory(kitNormalisation: KitNormalisation,idShelf:number,qty: number): Observable<KitNormalisation> {
        return this.http.post<KitNormalisation>(`${this.resourceUrl}/norm/inventory/`+idShelf+"/?qty="+qty, kitNormalisation);
    }

    query(req?: any): Observable<HttpResponse<Kit[]>> {
        const options = createRequestOption(req);
        return this.http.get<Kit[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    queryAssignedToProduct(id: number, req?: any): Observable<HttpResponse<Kit[]>> {
        const options = createRequestOption(req);
        return this.http.get<Kit[]>(`${this.resourceUrl}/product/${id}`, { params: options, observe: 'response' });
    }

    queryAssignedProductsKitByStatusOfSupplier(idSup: number, idKit: number, idDetail: number, req?: any): Observable<HttpResponse<Kit[]>> {
        const options = createRequestOption(req);
        return this.http.get<Kit[]>(`${this.resourceUrl}/norm/getAssignedProductsKitByStatusOfSupplier?idSup=${idSup}&idKit=${idKit}&idDetail=${idDetail}`,
            { params: options, observe: 'response' });
    }

    queryAssignedProductsKitOfSupplier(idSup: number, idKit: number, req?: any): Observable<HttpResponse<Kit[]>> {
        const options = createRequestOption(req);
        return this.http.get<Kit[]>(`${this.resourceUrl}/norm/getAssignedProductsKitOfSupplier?idSup=${idSup}&idKit=${idKit}`, { params: options, observe: 'response' });
    }

    queryListKitNormalisations(kitNormalisationCommand: any, req?: any): Observable<HttpResponse<KitNormalisation[]>> {
        const options = createRequestOption(req);
        return this.http.post<KitNormalisation[]>(`${this.resourceUrl}/norm/loadListKitNormalisations`, kitNormalisationCommand, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<HttpResponse<Kit[]>> {
        const options = this.createAdvancedSearchOption(req);
        return this.http.get<Kit[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }

    createAdvancedSearchOption = (req?: any): HttpParams => {
        let Params = new HttpParams();
        if (req) {
            // Begin assigning parameters
            if (req.page) {
                Params = Params.append('page', req.page)
            }
            if (req.size) {
                Params = Params.append('size', req.size)
            }
            if (req.query) {
                Params = Params.append('q', req.query)
            }
            if (req.filter) {
                Params = Params.append('filter', req.filter)
            }
            if (req.sort) {
                for (const s of req.sort) {
                    Params = Params.append('sort', s);
                }
            }
            if (req.supplier) {
                Params = Params.append('supplier', req.supplier)
            }

        }
        return Params;
    };

    queryNormByKit(id: number): Observable<HttpResponse<KitNormalisation>> {
        return this.http.get<KitNormalisation>(`${this.resourceUrl}/${id}/norm`, { observe: 'response' })
    }

    createNorm(kitNormalisation: KitNormalisation): Observable<KitNormalisation> {
        return this.http.post<KitNormalisation>(`${this.resourceUrl}/norm`, kitNormalisation);
    }

    updateNorm(kitNormalisation: KitNormalisation): Observable<KitNormalisation> {
        return this.http.put<KitNormalisation>(`${this.resourceUrl}/norm`, kitNormalisation);
    }

    deleteNorm(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/norm/${id}`, { observe: 'response' });
    }

    convertDate(kit: Kit) {
        kit.expirationDate = this.dateUtils.toDate(kit.expirationDate);
        kit.startValidation = this.dateUtils.toDate(kit.startValidation);
        kit.endValidation = this.dateUtils.toDate(kit.endValidation);
    }
}
