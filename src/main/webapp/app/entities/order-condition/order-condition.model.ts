import { BaseEntity } from '../../shared';

export class OrderCondition implements BaseEntity {
    constructor(
        public id?: number,
        public deliveryAddress?: string,
        public deliveryTerm?: string,
        public indicationForDelivery?: string,
        public installation?: string,
        public methodOfPayment?: string,
        public guaranty?: string,
    ) {
    }
}
