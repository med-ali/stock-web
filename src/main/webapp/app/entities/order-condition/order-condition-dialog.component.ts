import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { OrderCondition } from './order-condition.model';
import { OrderConditionPopupService } from './order-condition-popup.service';
import { OrderConditionService } from './order-condition.service';

@Component({
    selector: 'jhi-order-condition-dialog',
    templateUrl: './order-condition-dialog.component.html'
})
export class OrderConditionDialogComponent implements OnInit {

    orderCondition: OrderCondition;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private orderConditionService: OrderConditionService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.orderCondition.id !== undefined) {
            this.subscribeToSaveResponse(
                this.orderConditionService.update(this.orderCondition));
        } else {
            this.subscribeToSaveResponse(
                this.orderConditionService.create(this.orderCondition));
        }
        console.log(" i am here 2")
    }

    private subscribeToSaveResponse(result: Observable<OrderCondition>) {
        result.subscribe((res: OrderCondition) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: OrderCondition) {
        this.eventManager.broadcast({ name: 'orderConditionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-order-condition-popup',
    template: ''
})
export class OrderConditionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private orderConditionPopupService: OrderConditionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.orderConditionPopupService
                    .open(OrderConditionDialogComponent as Component, params['id']);
            } else {
                this.orderConditionPopupService
                    .open(OrderConditionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
