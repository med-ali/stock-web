import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { OrderCondition } from './order-condition.model';
import { createRequestOption } from '../../shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';

@Injectable()
export class OrderConditionService {

    private resourceUrl = SERVER_API_URL + 'api/order-conditions';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/order-conditions';

    constructor(private http: HttpClient) { }

    create(orderCondition: OrderCondition): Observable<OrderCondition> {
        return this.http.post<OrderCondition>(this.resourceUrl, orderCondition);
    }

    update(orderCondition: OrderCondition): Observable<OrderCondition> {
        return this.http.put<OrderCondition>(this.resourceUrl, orderCondition);
    }

    find(id: number): Observable<OrderCondition> {
        return this.http.get<OrderCondition>(`${this.resourceUrl}/${id}`);
    }

    findLast(): Observable<OrderCondition> {
        return this.http.get<OrderCondition>(`${this.resourceUrl}/last`);
    }

    query(req?: any): Observable<HttpResponse<OrderCondition[]>> {
        const options = createRequestOption(req);
        return this.http.get<OrderCondition[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<HttpResponse<OrderCondition[]>> {
        const options = createRequestOption(req);
        return this.http.get<OrderCondition[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }
}
