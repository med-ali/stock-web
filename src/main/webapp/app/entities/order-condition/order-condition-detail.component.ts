import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { OrderCondition } from './order-condition.model';
import { OrderConditionService } from './order-condition.service';

@Component({
    selector: 'jhi-order-condition-detail',
    templateUrl: './order-condition-detail.component.html'
})
export class OrderConditionDetailComponent implements OnInit, OnDestroy {

    orderCondition: OrderCondition;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private orderConditionService: OrderConditionService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInOrderConditions();
    }

    load(id) {
        this.orderConditionService.find(id).subscribe((orderCondition) => {
            this.orderCondition = orderCondition;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInOrderConditions() {
        this.eventSubscriber = this.eventManager.subscribe(
            'orderConditionListModification',
            (response) => this.load(this.orderCondition.id)
        );
    }
}
