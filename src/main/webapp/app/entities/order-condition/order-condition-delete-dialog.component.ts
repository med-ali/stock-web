import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { OrderCondition } from './order-condition.model';
import { OrderConditionPopupService } from './order-condition-popup.service';
import { OrderConditionService } from './order-condition.service';

@Component({
    selector: 'jhi-order-condition-delete-dialog',
    templateUrl: './order-condition-delete-dialog.component.html'
})
export class OrderConditionDeleteDialogComponent {

    orderCondition: OrderCondition;

    constructor(
        private orderConditionService: OrderConditionService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.orderConditionService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'orderConditionListModification',
                content: 'Deleted an orderCondition'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-order-condition-delete-popup',
    template: ''
})
export class OrderConditionDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private orderConditionPopupService: OrderConditionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.orderConditionPopupService
                .open(OrderConditionDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
