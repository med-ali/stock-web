import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { OrderConditionComponent } from './order-condition.component';
import { OrderConditionDetailComponent } from './order-condition-detail.component';
import { OrderConditionPopupComponent } from './order-condition-dialog.component';
import { OrderConditionDeletePopupComponent } from './order-condition-delete-dialog.component';

@Injectable()
export class OrderConditionResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const orderConditionRoute: Routes = [
    {
        path: 'order-condition',
        component: OrderConditionComponent,
        resolve: {
            'pagingParams': OrderConditionResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'stockApp.orderCondition.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'order-condition/:id',
        component: OrderConditionDetailComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'stockApp.orderCondition.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const orderConditionPopupRoute: Routes = [
    {
        path: 'order-condition-new',
        component: OrderConditionPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'stockApp.orderCondition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'order-condition/:id/edit',
        component: OrderConditionPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'stockApp.orderCondition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'order-condition/:id/delete',
        component: OrderConditionDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'stockApp.orderCondition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
