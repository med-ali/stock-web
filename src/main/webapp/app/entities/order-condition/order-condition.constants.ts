import {
    OrderConditionService,
    OrderConditionPopupService,
    OrderConditionComponent,
    OrderConditionDetailComponent,
    OrderConditionDialogComponent,
    OrderConditionPopupComponent,
    OrderConditionDeletePopupComponent,
    OrderConditionDeleteDialogComponent,
    orderConditionRoute,
    orderConditionPopupRoute,
    OrderConditionResolvePagingParams,
} from './';

export const orderCondition_declarations = [
    OrderConditionComponent,
    OrderConditionDetailComponent,
    OrderConditionDialogComponent,
    OrderConditionDeleteDialogComponent,
    OrderConditionPopupComponent,
    OrderConditionDeletePopupComponent,
];

export const orderCondition_entryComponents = [
    OrderConditionComponent,
    OrderConditionDialogComponent,
    OrderConditionPopupComponent,
    OrderConditionDeleteDialogComponent,
    OrderConditionDeletePopupComponent,
];

export const orderCondition_providers = [
    OrderConditionService,
    OrderConditionPopupService,
    OrderConditionResolvePagingParams,
];
