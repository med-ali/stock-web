import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { TvaCode } from './tva-code.model';
import { createRequestOption } from '../../shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';

@Injectable()
export class TvaCodeService {

    private resourceUrl = SERVER_API_URL + 'api/tva-codes';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/tva-codes';

    constructor(private http: HttpClient) { }

    create(tvaCode: TvaCode): Observable<TvaCode> {
        return this.http.post<TvaCode>(this.resourceUrl, tvaCode);
    }

    checkCode(code: string): Observable<TvaCode> {
        return this.http.get<TvaCode>(this.resourceUrl+"/checkcode/?code="+code);
    }

    update(tvaCode: TvaCode): Observable<TvaCode> {
        return this.http.put<TvaCode>(this.resourceUrl, tvaCode);
    }

    find(id: number): Observable<TvaCode> {
        return this.http.get<TvaCode>(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<HttpResponse<TvaCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<TvaCode[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<HttpResponse<TvaCode[]>> {
        const options = createRequestOption(req);
        return this.http.get<TvaCode[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }
}
