import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TvaCode } from './tva-code.model';
import { TvaCodeService } from './tva-code.service';

@Component({
    selector: 'jhi-tva-code-detail',
    templateUrl: './tva-code-detail.component.html'
})
export class TvaCodeDetailComponent implements OnInit, OnDestroy {

    tvaCode: TvaCode;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private tvaCodeService: TvaCodeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTvaCodes();
    }

    load(id) {
        this.tvaCodeService.find(id).subscribe((tvaCode) => {
            this.tvaCode = tvaCode;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTvaCodes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'tvaCodeListModification',
            (response) => this.load(this.tvaCode.id)
        );
    }
}
