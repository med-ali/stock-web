import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TvaCode } from './tva-code.model';
import { TvaCodePopupService } from './tva-code-popup.service';
import { TvaCodeService } from './tva-code.service';

@Component({
    selector: 'jhi-tva-code-dialog',
    templateUrl: './tva-code-dialog.component.html'
})
export class TvaCodeDialogComponent implements OnInit {

    tvaCode: TvaCode;
    isSaving: boolean;
    existCode:boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private tvaCodeService: TvaCodeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.tvaCodeService.checkCode(this.tvaCode.code).subscribe((value) => { 
            console.log(value) 
            if (value==true && this.tvaCode.id == undefined){
                this.existCode = true;
            }
            else{
                this.existCode = false;
                this.isSaving = true;
                if (this.tvaCode.id !== undefined) {
                    this.subscribeToSaveResponse(
                        this.tvaCodeService.update(this.tvaCode));
                } else {
                    this.subscribeToSaveResponse(
                        this.tvaCodeService.create(this.tvaCode));
                }
                this.clear()
            }
            },
            (error) => { 
            console.log("error"+error)
        })
    }

    private subscribeToSaveResponse(result: Observable<TvaCode>) {
        result.subscribe((res: TvaCode) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: TvaCode) {
        this.eventManager.broadcast({ name: 'tvaCodeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-tva-code-popup',
    template: ''
})
export class TvaCodePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tvaCodePopupService: TvaCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.tvaCodePopupService
                    .open(TvaCodeDialogComponent as Component, params['id']);
            } else {
                this.tvaCodePopupService
                    .open(TvaCodeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
