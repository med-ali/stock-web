import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { TvaCode } from './tva-code.model';
import { TvaCodePopupService } from './tva-code-popup.service';
import { TvaCodeService } from './tva-code.service';

@Component({
    selector: 'jhi-tva-code-delete-dialog',
    templateUrl: './tva-code-delete-dialog.component.html'
})
export class TvaCodeDeleteDialogComponent {

    tvaCode: TvaCode;
    alerts = []; 
    constructor(private alertService: JhiAlertService,
        private tvaCodeService: TvaCodeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    addErrorAlert(message, key?, data?) {
        key = (key && key !== null) ? key : message;
        this.alerts.push(
            this.alertService.addAlert(
                {
                    type: 'danger',
                    msg: key,
                    params: data,
                    timeout: 5000,
                    toast: this.alertService.isToast(),
                    scoped: true
                },
                this.alerts
            )
        );
    }
    confirmDelete(id: number) {
        this.tvaCodeService.delete(id).subscribe((response) => {
            if (response["status"] == 200 || response["status"] == 204){
                this.eventManager.broadcast({
                    name: 'tvaListModification',
                    content: 'Deleted an tvaCode'
                });
                this.activeModal.dismiss(true);
            }
            else{
                this.addErrorAlert('Delete not reachable', 'error.entity.delete.reachable');
            } 
        });
    }
}

@Component({
    selector: 'jhi-tva-code-delete-popup',
    template: ''
})
export class TvaCodeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tvaCodePopupService: TvaCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.tvaCodePopupService
                .open(TvaCodeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
