import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { TvaCodeComponent } from './tva-code.component';
import { TvaCodeDetailComponent } from './tva-code-detail.component';
import { TvaCodePopupComponent } from './tva-code-dialog.component';
import { TvaCodeDeletePopupComponent } from './tva-code-delete-dialog.component';

@Injectable()
export class TvaCodeResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const tvaCodeRoute: Routes = [
    {
        path: 'tva-code',
        component: TvaCodeComponent,
        resolve: {
            'pagingParams': TvaCodeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.tvaCode.default'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tva-code/:id',
        component: TvaCodeDetailComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.tvaCode.default'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const tvaCodePopupRoute: Routes = [
    {
        path: 'tva-code-new',
        component: TvaCodePopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.tvaCode.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'tva-code/:id/edit',
        component: TvaCodePopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.tvaCode.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'tva-code/:id/delete',
        component: TvaCodeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.tvaCode.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
