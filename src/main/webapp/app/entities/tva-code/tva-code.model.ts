import { BaseEntity } from '../../shared';

export class TvaCode implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public description?: string,
        public value?: number,
    ) {
    }
}
