import {
    TvaCodeService,
    TvaCodePopupService,
    TvaCodeComponent,
    TvaCodeDetailComponent,
    TvaCodeDialogComponent,
    TvaCodePopupComponent,
    TvaCodeDeletePopupComponent,
    TvaCodeDeleteDialogComponent,
    tvaCodeRoute,
    tvaCodePopupRoute,
    TvaCodeResolvePagingParams,
} from './';

export const tvaCode_declarations = [
    TvaCodeComponent,
    TvaCodeDetailComponent,
    TvaCodeDialogComponent,
    TvaCodeDeleteDialogComponent,
    TvaCodePopupComponent,
    TvaCodeDeletePopupComponent,
];

export const tvaCode_entryComponents = [
    TvaCodeComponent,
    TvaCodeDialogComponent,
    TvaCodePopupComponent,
    TvaCodeDeleteDialogComponent,
    TvaCodeDeletePopupComponent,
];

export const tvaCode_providers = [
    TvaCodeService,
    TvaCodePopupService,
    TvaCodeResolvePagingParams,
];
