import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Headquarter } from './headquarter.model';
import { HeadquarterPopupService } from './headquarter-popup.service';
import { HeadquarterService } from './headquarter.service';
import { Company, CompanyService } from '../company';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-headquarter-dialog',
    templateUrl: './headquarter-dialog.component.html'
})
export class HeadquarterDialogComponent implements OnInit {

    headquarter: Headquarter;
    isSaving: boolean;
    existCode:boolean;
    companies: Company[];
    companyModal: boolean;
    descriptionCompany:string;
    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private headquarterService: HeadquarterService,
        private companyService: CompanyService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.companyModal = false;
        if(this.headquarter.company){
            this.descriptionCompany = this.headquarter.company.code + "-" + this.headquarter.company.description;  
        }
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.headquarterService.checkCode(this.headquarter.code).subscribe((value) => { 
            console.log(value) 
            if (value==true && this.headquarter.id == undefined){
                this.existCode = true;
            }
            else{
                this.existCode = false;
                this.isSaving = true;
                if (this.headquarter.id !== undefined) {
                    this.subscribeToSaveResponse(
                        this.headquarterService.update(this.headquarter));
                } else {
                    this.subscribeToSaveResponse(
                        this.headquarterService.create(this.headquarter));
                }
                this.clear()
            }
            },
            (error) => { 
            console.log("error"+error)
        })
    }

    private subscribeToSaveResponse(result: Observable<Headquarter>) {
        result.subscribe((res: Headquarter) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Headquarter) {
        this.eventManager.broadcast({ name: 'headquarterListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackCompanyById(index: number, item: Company) {
        return item.id;
    }

    openMyModal(event) {
        if (event === 'companyModal') {
            this.companyModal = true;
        }
        document.querySelector('#' + event).classList.add('md-show');
    }

    closeModal(event) {
        this.companyModal = false;
        (document.querySelector('#' + event).parentElement.parentElement).classList.remove('md-show');
    }

    closeMyModalEvent(event) {
        ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
    }

    assignCompany(company) {
        this.headquarter.company = company;
        this.descriptionCompany = this.headquarter.company.code + "-" + this.headquarter.company.description;
        this.closeModal('closeCompanyBtn');
    }
}

@Component({
    selector: 'jhi-headquarter-popup',
    template: ''
})
export class HeadquarterPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private headquarterPopupService: HeadquarterPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.headquarterPopupService
                    .open(HeadquarterDialogComponent as Component, params['id']);
            } else {
                this.headquarterPopupService
                    .open(HeadquarterDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
