import { BaseEntity } from '../../shared';

export class Headquarter implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public name?: string,
        public description?: string,
        public isActive?: boolean,
        public company?: any,
    ) {
        this.isActive = true;
    }
}
