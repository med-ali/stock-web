import {
    HeadquarterService,
    HeadquarterPopupService,
    HeadquarterComponent,
    HeadquarterDetailComponent,
    HeadquarterDialogComponent,
    HeadquarterPopupComponent,
    HeadquarterDeletePopupComponent,
    HeadquarterDeleteDialogComponent,
    headquarterRoute,
    headquarterPopupRoute,
    HeadquarterResolvePagingParams,
} from './';

export const headquarter_declarations = [
    HeadquarterComponent,
    HeadquarterDetailComponent,
    HeadquarterDialogComponent,
    HeadquarterDeleteDialogComponent,
    HeadquarterPopupComponent,
    HeadquarterDeletePopupComponent,
];

export const headquarter_entryComponents = [
    HeadquarterComponent,
    HeadquarterDialogComponent,
    HeadquarterPopupComponent,
    HeadquarterDeleteDialogComponent,
    HeadquarterDeletePopupComponent,
];

export const headquarter_providers = [
    HeadquarterService,
    HeadquarterPopupService,
    HeadquarterResolvePagingParams,
];
