import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Headquarter } from './headquarter.model';
import { HeadquarterService } from './headquarter.service';

@Component({
    selector: 'jhi-headquarter-detail',
    templateUrl: './headquarter-detail.component.html'
})
export class HeadquarterDetailComponent implements OnInit, OnDestroy {

    headquarter: Headquarter;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private headquarterService: HeadquarterService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInHeadquarters();
    }

    load(id) {
        this.headquarterService.find(id).subscribe((headquarter) => {
            this.headquarter = headquarter;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInHeadquarters() {
        this.eventSubscriber = this.eventManager.subscribe(
            'headquarterListModification',
            (response) => this.load(this.headquarter.id)
        );
    }
}
