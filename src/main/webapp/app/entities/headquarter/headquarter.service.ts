import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Headquarter } from './headquarter.model';
import { createRequestOption } from '../../shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';

@Injectable()
export class HeadquarterService {

    private resourceUrl = SERVER_API_URL + 'api/headquarters';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/headquarters';

    constructor(private http: HttpClient) { }

    create(headquarter: Headquarter): Observable<Headquarter> {
        return this.http.post<Headquarter>(this.resourceUrl, headquarter);
    }

    checkCode(code: string): Observable<Headquarter> {
        return this.http.get<Headquarter>(this.resourceUrl+"/checkcode/?code="+code);
    }

    update(headquarter: Headquarter): Observable<Headquarter> {
        return this.http.put<Headquarter>(this.resourceUrl, headquarter);
    }

    find(id: number): Observable<Headquarter> {
        return this.http.get<Headquarter>(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<HttpResponse<Headquarter[]>> {
        const options = createRequestOption(req);
        return this.http.get<Headquarter[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<HttpResponse<Headquarter[]>> {
        const options = createRequestOption(req);
        return this.http.get<Headquarter[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }
}
