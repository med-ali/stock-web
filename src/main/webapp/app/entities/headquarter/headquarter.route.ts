import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { HeadquarterComponent } from './headquarter.component';
import { HeadquarterDetailComponent } from './headquarter-detail.component';
import { HeadquarterPopupComponent } from './headquarter-dialog.component';
import { HeadquarterDeletePopupComponent } from './headquarter-delete-dialog.component';

@Injectable()
export class HeadquarterResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const headquarterRoute: Routes = [
    {
        path: 'headquarter',
        component: HeadquarterComponent,
        resolve: {
            'pagingParams': HeadquarterResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.headquarter.default'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'headquarter/:id',
        component: HeadquarterDetailComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.headquarter.default'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const headquarterPopupRoute: Routes = [
    {
        path: 'headquarter-new',
        component: HeadquarterPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.headquarter.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'headquarter/:id/edit',
        component: HeadquarterPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.headquarter.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'headquarter/:id/delete',
        component: HeadquarterDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.headquarter.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
