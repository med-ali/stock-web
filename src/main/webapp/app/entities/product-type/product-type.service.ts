import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { ProductType } from './product-type.model';
import { createRequestOption } from '../../shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';

@Injectable()
export class ProductTypeService {

    private resourceUrl = SERVER_API_URL + 'api/product-types';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/product-types';

    constructor(private http: HttpClient) { }

    create(productType: ProductType): Observable<ProductType> {
        return this.http.post<ProductType>(this.resourceUrl, productType);
    }

    checkCode(code: string): Observable<ProductType> {
        return this.http.get<ProductType>(this.resourceUrl+"/checkcode/?code="+code);
    }

    update(productType: ProductType): Observable<ProductType> {
        return this.http.put<ProductType>(this.resourceUrl, productType);
    }

    find(id: number): Observable<ProductType> {
        return this.http.get<ProductType>(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<HttpResponse<ProductType[]>> {
        const options = createRequestOption(req);
        return this.http.get<ProductType[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<HttpResponse<ProductType[]>> {
        const options = createRequestOption(req);
        return this.http.get<ProductType[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }
}
