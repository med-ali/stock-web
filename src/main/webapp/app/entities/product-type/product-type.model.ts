import { BaseEntity } from '../../shared';

export class ProductType implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public description?: string,
    ) {
    }
}
