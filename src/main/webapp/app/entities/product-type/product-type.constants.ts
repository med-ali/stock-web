import {
    ProductTypeService,
    ProductTypePopupService,
    ProductTypeComponent,
    ProductTypeDetailComponent,
    ProductTypeDialogComponent,
    ProductTypePopupComponent,
    ProductTypeDeletePopupComponent,
    ProductTypeDeleteDialogComponent,
    productTypeRoute,
    productTypePopupRoute,
    ProductTypeResolvePagingParams,
} from './';

export const productType_declarations = [
    ProductTypeComponent,
    ProductTypeDetailComponent,
    ProductTypeDialogComponent,
    ProductTypeDeleteDialogComponent,
    ProductTypePopupComponent,
    ProductTypeDeletePopupComponent,
];

export const productType_entryComponents = [
    ProductTypeComponent,
    ProductTypeDialogComponent,
    ProductTypePopupComponent,
    ProductTypeDeleteDialogComponent,
    ProductTypeDeletePopupComponent,
];

export const productType_providers = [
    ProductTypeService,
    ProductTypePopupService,
    ProductTypeResolvePagingParams,
];
