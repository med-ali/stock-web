import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { ShelvingUnit } from './shelving-unit.model';
import { ShelvingUnitPopupService } from './shelving-unit-popup.service';
import { ShelvingUnitService } from './shelving-unit.service';

@Component({
    selector: 'jhi-shelving-unit-delete-dialog',
    templateUrl: './shelving-unit-delete-dialog.component.html'
})
export class ShelvingUnitDeleteDialogComponent {

    shelvingUnit: ShelvingUnit;
    alerts = [];
    constructor(private alertService: JhiAlertService,
        private shelvingUnitService: ShelvingUnitService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    addErrorAlert(message, key?, data?) {
        key = (key && key !== null) ? key : message;
        this.alerts.push(
            this.alertService.addAlert(
                {
                    type: 'danger',
                    msg: key,
                    params: data,
                    timeout: 5000,
                    toast: this.alertService.isToast(),
                    scoped: true
                },
                this.alerts
            )
        );
    }

    confirmDelete(id: number) {
        this.shelvingUnitService.delete(id).subscribe((response) => {
            if (response["status"] == 200){
                this.eventManager.broadcast({
                    name: 'shelvingUnitListModification',
                    content: 'Deleted an shelvingUnit'
                });
                this.activeModal.dismiss(true);
            }
            else{
                this.addErrorAlert('Delete not reachable', 'error.entity.delete.reachable');
            } 
        });
    }
}

@Component({
    selector: 'jhi-shelving-unit-delete-popup',
    template: ''
})
export class ShelvingUnitDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private shelvingUnitPopupService: ShelvingUnitPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.shelvingUnitPopupService
                .open(ShelvingUnitDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
