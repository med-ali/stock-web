import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { ShelvingUnit } from './shelving-unit.model';
import { ShelvingUnitService } from './shelving-unit.service';

@Component({
    selector: 'jhi-shelving-unit-detail',
    templateUrl: './shelving-unit-detail.component.html'
})
export class ShelvingUnitDetailComponent implements OnInit, OnDestroy {

    shelvingUnit: ShelvingUnit;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private shelvingUnitService: ShelvingUnitService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInShelvingUnits();
    }

    load(id) {
        this.shelvingUnitService.find(id).subscribe((shelvingUnit) => {
            this.shelvingUnit = shelvingUnit;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInShelvingUnits() {
        this.eventSubscriber = this.eventManager.subscribe(
            'shelvingUnitListModification',
            (response) => this.load(this.shelvingUnit.id)
        );
    }
}
