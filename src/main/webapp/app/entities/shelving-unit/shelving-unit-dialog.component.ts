import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ShelvingUnit } from './shelving-unit.model';
import { ShelvingUnitPopupService } from './shelving-unit-popup.service';
import { ShelvingUnitService } from './shelving-unit.service';
import { Headquarter, HeadquarterService } from '../headquarter';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-shelving-unit-dialog',
    templateUrl: './shelving-unit-dialog.component.html'
})
export class ShelvingUnitDialogComponent implements OnInit {

    shelvingUnit: ShelvingUnit;
    isSaving: boolean;
    active: any;
    headquarters: Headquarter[];
    descriptionHeadquarter:string;
    headquarterModal: boolean;
    existCode:boolean;
    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private shelvingUnitService: ShelvingUnitService,
        private headquarterService: HeadquarterService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.headquarterModal = false;
        if(this.shelvingUnit.headquarter){
            this.descriptionHeadquarter = this.shelvingUnit.headquarter.code + "-" + this.shelvingUnit.headquarter.description;
        }
        if (this.shelvingUnit.isActive==false){
            this.active = 'no';
        }
        else{
            this.active = 'yes';
        }
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    public activeChange(event) {
        if (event === 'yes') {
            this.shelvingUnit.isActive = true;
        } else if (event === 'no') {
            this.shelvingUnit.isActive = false;
        }
    }

    save() {
        this.shelvingUnitService.checkCode(this.shelvingUnit.code).subscribe((value) => { 
            console.log(value) 
            if (value==true && this.shelvingUnit.id == undefined){
                this.existCode = true;
            }
            else{
                this.existCode = false;
                this.isSaving = true;
                if (this.shelvingUnit.id !== undefined) {
                    this.subscribeToSaveResponse(
                        this.shelvingUnitService.update(this.shelvingUnit));
                } else {
                    this.subscribeToSaveResponse(
                        this.shelvingUnitService.create(this.shelvingUnit));
                }
                this.clear()
            }
            },
            (error) => { 
            console.log("error"+error)
        })
    }

    private subscribeToSaveResponse(result: Observable<ShelvingUnit>) {
        result.subscribe((res: ShelvingUnit) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: ShelvingUnit) {
        this.eventManager.broadcast({ name: 'shelvingUnitListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackHeadquarterById(index: number, item: Headquarter) {
        return item.id;
    }

    openMyModal(event) {
        if (event === 'headquarterModal') {
            this.headquarterModal = true;
        }
        document.querySelector('#' + event).classList.add('md-show');
    }

    closeModal(event) {
        this.headquarterModal = false;
        (document.querySelector('#' + event).parentElement.parentElement).classList.remove('md-show');
    }

    closeMyModalEvent(event) {
        ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
    }

    assignHeadquarter(headquarter) {
        this.shelvingUnit.headquarter = headquarter;
        this.descriptionHeadquarter = this.shelvingUnit.headquarter.code + "-" + this.shelvingUnit.headquarter.description;
        this.closeModal('closeHeadquarterBtn');
    }
}

@Component({
    selector: 'jhi-shelving-unit-popup',
    template: ''
})
export class ShelvingUnitPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private shelvingUnitPopupService: ShelvingUnitPopupService) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.shelvingUnitPopupService
                    .open(ShelvingUnitDialogComponent as Component, params['id']);
            } else {
                this.shelvingUnitPopupService
                    .open(ShelvingUnitDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
