import {
    ShelvingUnitService,
    ShelvingUnitPopupService,
    ShelvingUnitComponent,
    ShelvingUnitDetailComponent,
    ShelvingUnitDialogComponent,
    ShelvingUnitPopupComponent,
    ShelvingUnitDeletePopupComponent,
    ShelvingUnitDeleteDialogComponent,
    shelvingUnitRoute,
    shelvingUnitPopupRoute,
    ShelvingUnitResolvePagingParams,
} from './';

export const shelvingUnit_declarations = [
    ShelvingUnitComponent,
    ShelvingUnitDetailComponent,
    ShelvingUnitDialogComponent,
    ShelvingUnitDeleteDialogComponent,
    ShelvingUnitPopupComponent,
    ShelvingUnitDeletePopupComponent,
];

export const shelvingUnit_entryComponents = [
    ShelvingUnitComponent,
    ShelvingUnitDialogComponent,
    ShelvingUnitPopupComponent,
    ShelvingUnitDeleteDialogComponent,
    ShelvingUnitDeletePopupComponent,
];

export const shelvingUnit_providers = [
    ShelvingUnitService,
    ShelvingUnitPopupService,
    ShelvingUnitResolvePagingParams,
];
