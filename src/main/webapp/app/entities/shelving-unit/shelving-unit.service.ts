import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { ShelvingUnit } from './shelving-unit.model';
import { createRequestOption } from '../../shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';

@Injectable()
export class ShelvingUnitService {

    private resourceUrl = SERVER_API_URL + 'api/shelving-units';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/shelving-units';

    constructor(private http: HttpClient) { }

    create(shelvingUnit: ShelvingUnit): Observable<ShelvingUnit> {
        return this.http.post<ShelvingUnit>(this.resourceUrl, shelvingUnit);
    }

    update(shelvingUnit: ShelvingUnit): Observable<ShelvingUnit> {
        return this.http.put<ShelvingUnit>(this.resourceUrl, shelvingUnit);
    }

    checkCode(code: string): Observable<ShelvingUnit> {
        return this.http.get<ShelvingUnit>(this.resourceUrl+"/checkcode/?code="+code);
    }

    find(id: number): Observable<ShelvingUnit> {
        return this.http.get<ShelvingUnit>(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<HttpResponse<ShelvingUnit[]>> {
        const options = createRequestOption(req);
        return this.http.get<ShelvingUnit[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    queryByHeadquarter(headquarterId:number,req?: any): Observable<HttpResponse<ShelvingUnit[]>> {
        const options = createRequestOption(req);
        return this.http.get<ShelvingUnit[]>(this.resourceUrl+ "/filter/"+headquarterId, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<HttpResponse<ShelvingUnit[]>> {
        const options = createRequestOption(req);
        return this.http.get<ShelvingUnit[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }
}
