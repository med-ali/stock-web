import { BaseEntity } from './../../shared';

export class ShelvingUnit implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public name?: string,
        public description?: string,
        public isActive?: boolean,
        public headquarter?: any,
    ) {
        this.isActive = true;
    }
}
