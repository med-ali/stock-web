import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ShelvingUnitComponent } from './shelving-unit.component';
import { ShelvingUnitDetailComponent } from './shelving-unit-detail.component';
import { ShelvingUnitPopupComponent } from './shelving-unit-dialog.component';
import { ShelvingUnitDeletePopupComponent } from './shelving-unit-delete-dialog.component';

@Injectable()
export class ShelvingUnitResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const shelvingUnitRoute: Routes = [
    {
        path: 'shelving-unit',
        component: ShelvingUnitComponent,
        resolve: {
            'pagingParams': ShelvingUnitResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.shelvingUnit.default'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'shelving-unit/:id',
        component: ShelvingUnitDetailComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.shelvingUnit.default'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const shelvingUnitPopupRoute: Routes = [
    {
        path: 'shelving-unit-new',
        component: ShelvingUnitPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.shelvingUnit.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'shelving-unit/:id/edit',
        component: ShelvingUnitPopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.shelvingUnit.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'shelving-unit/:id/delete',
        component: ShelvingUnitDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.shelvingUnit.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
