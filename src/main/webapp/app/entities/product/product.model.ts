import { BaseEntity } from './../../shared';

export class Product implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public description?: string,
        public lastAnalyse?: any,
        public actualQuantity?: number,
        public minimumLevel?: number,
        public isActive?: boolean,
        public measureUnit?: any,
        public productType?: any,
    ) {
        this.isActive = true;
        this.minimumLevel = 5;
        this.actualQuantity = 0;
    }
}
