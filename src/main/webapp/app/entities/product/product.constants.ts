
import {
    ProductService,
    ProductPopupService,
    ProductComponent,
    ProductDetailComponent,
    ProductDialogComponent,
    ProductPopupComponent,
    ProductDeletePopupComponent,
    ProductDeleteDialogComponent,
    productRoute,
    productPopupRoute,
    ProductResolvePagingParams,
} from './';

export const product_declarations = [
    ProductComponent,
    ProductDetailComponent,
    ProductDialogComponent,
    ProductDeleteDialogComponent,
    ProductPopupComponent,
    ProductDeletePopupComponent,
];

export const product_entryComponents = [
    ProductComponent,
    ProductDialogComponent,
    ProductPopupComponent,
    ProductDeleteDialogComponent,
    ProductDeletePopupComponent,
];

export const product_providers = [
    ProductService,
    ProductPopupService,
    ProductResolvePagingParams,
];
