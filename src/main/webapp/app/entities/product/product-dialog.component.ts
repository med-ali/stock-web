import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Product } from './product.model';
import { ProductPopupService } from './product-popup.service';
import { ProductService } from './product.service';
import { MeasureUnit, MeasureUnitService } from '../measure-unit';
import { ProductType, ProductTypeService } from '../product-type';
import { ResponseWrapper } from '../../shared';

@Component({ 
    selector: 'jhi-product-dialog',
    templateUrl: './product-dialog.component.html',
    styleUrls: ['./product-dialog.component.scss']
})
export class ProductDialogComponent implements OnInit {

    product: Product;
    isSaving: boolean;
    measureUnitModal: boolean;
    productTypeModal: boolean;
    existCode:boolean;
    descriptionType:string;
    descriptionUnit:string;
    codeShow:boolean
    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private productService: ProductService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.existCode = false;
        this.isSaving = false;
        this.measureUnitModal = this.productTypeModal = false;
        
        if(this.product.measureUnit && this.product.productType){
            this.descriptionType = this.product.productType.code + "-" + this.product.productType.description;
            this.descriptionUnit = this.product.measureUnit.code + "-"+ this.product.measureUnit.description;
        }
        if(this.product.code){
            this.codeShow = true;
        }  
    }
    onNullType(){
        this.product.productType =  null
        this.descriptionType = ""
    }
    onNullMeasureUnit(){
        this.product.measureUnit =  null
        this.descriptionUnit = ""
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }

    /*save() {   
        this.productService.checkCode(this.product.code).subscribe((value) => { 
        console.log(value) 
        if (value==true && this.product.id == undefined){
            this.existCode = true;
            this.isSaving = false;
        }
        else{
            this.existCode = false;
            this.isSaving = true;
            if (this.product.id !== undefined) {
                this.subscribeToSaveResponse(
                    this.productService.update(this.product));
            } else {
                this.product.actualQuantity = 0;
                this.subscribeToSaveResponse(
                    
                    this.productService.create(this.product));
            }
            this.clear()
        }
        },
        (error) => {  
        })   
    }*/
    save() { 
            this.isSaving = true;
            if (this.product.id !== undefined) {
                this.subscribeToSaveResponse(
                    this.productService.update(this.product));
            } else {
                this.product.actualQuantity = 0;
                this.subscribeToSaveResponse(
                    
                    this.productService.create(this.product));
            }
            this.clear()
    }
    private subscribeToSaveResponse(result: Observable<Product>) {
        result.subscribe((res: Product) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }
    
    private onSaveSuccess(result: Product) {
        this.eventManager.broadcast({ name: 'productListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    openMyModal(event) {
        if (event === 'measureUnitModal') {
            this.measureUnitModal = true;
        } else if (event === 'productTypeModal') {
            this.productTypeModal = true;
        }
         
        document.querySelector('#' + event).classList.add('md-show');
    }

    closeModal(event) {
        this.measureUnitModal = this.productTypeModal = false;
        (document.querySelector('#' + event).parentElement.parentElement).classList.remove('md-show');
    }

    closeMyModalEvent(event) {
        ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
    }

    assignMeasureUnit(measureUnit) {
        this.product.measureUnit = measureUnit;
        this.descriptionUnit = this.product.measureUnit.code + "-" + this.product.measureUnit.description
        this.closeModal('closeMeasureUnitBtn');
    }

    assignProductType(productType) {
        this.product.productType = productType;
        this.descriptionType = this.product.productType.code + "-" + this.product.productType.description;
        this.closeModal('closeProductTypeBtn');
    }
}

@Component({
    selector: 'jhi-product-popup',
    template: ''
})
export class ProductPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private productPopupService: ProductPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.productPopupService
                    .open(ProductDialogComponent as Component, params['id']);
            } else {
                this.productPopupService
                    .open(ProductDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
