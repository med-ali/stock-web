import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Product } from './product.model';
import { createRequestOption } from '../../shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { JhiDateUtils } from 'ng-jhipster';

@Injectable()
export class ProductService {

    private resourceUrl = SERVER_API_URL + 'api/products';
    private analysisUrl = SERVER_API_URL + 'api/analysis';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/products';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(product: Product): Observable<Product> {
        this.convertDate(product);
        return this.http.post<Product>(this.resourceUrl, product);
    }

    checkCode(code: string): Observable<Product> {
        return this.http.get<Product>(this.resourceUrl+"/checkcode/?code="+code);
    }

    update(product: Product): Observable<Product> {
        this.convertDate(product);
        return this.http.put<Product>(this.resourceUrl, product);
    }

    find(id: number): Observable<Product> {
        return this.http.get<Product>(`${this.resourceUrl}/${id}`);
    }
    printReport(type:string,lang:string): Observable<any> {
        return this.http.get<any>(this.resourceUrl+"/print/?type="+type+"&lang="+lang);
    }
    findAssignedWithArticle(id: number, req?: any): Observable<HttpResponse<Product[]>> {
        const options = createRequestOption(req);
        return this.http.get<Product[]>(`${this.resourceUrl}/article/${id}`, { params: options, observe: 'response' });
    }

    getAllAssignedProductArticles(product: any,idOrder:number, req?: any): Observable<HttpResponse<Product[]>> {
        console.log('idOreder'+idOrder)
        const options = createRequestOption(req);
        return this.http.post<Product[]>(`${this.resourceUrl}/getAllAssignedArticles/${idOrder}`, product, { params: options, observe: 'response' });
    }

    findAssignedWithkit(id: number, req?: any): Observable<HttpResponse<Product[]>> {
        const options = createRequestOption(req);
        return this.http.get<Product[]>(`${this.resourceUrl}/kit/${id}`, { params: options, observe: 'response' });
    }

    getAllAssignedProductKits(product: any,idOrder:number, req?: any): Observable<HttpResponse<Product[]>> {
        const options = createRequestOption(req);
        return this.http.post<Product[]>(`${this.resourceUrl}/getAllAssignedKits/${idOrder}`, product, { params: options, observe: 'response' });
    }

    query(req?: any): Observable<HttpResponse<Product[]>> {
        const options = createRequestOption(req);
        return this.http.get<Product[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    listProductsBySupplier(product: any, req?: any): Observable<HttpResponse<Product[]>> {
        const options = this.createAdvancedSearchOption(req);
        return this.http.post<Product[]>(`${this.resourceUrl}/listProductsBySupplier`, product, { params: options, observe: 'response' });
    }

    listProductsNotAssociatedBySupplier(idSupplier: any, req?: any): Observable<HttpResponse<Product[]>> {
        const options = this.createAdvancedSearchOption(req);
        return this.http.get<Product[]>(`${this.resourceUrl}/listProductsNotAssociatedBySupplier?idSupplier=${idSupplier}`, { params: options, observe: 'response' });
    }

    listProductsNotAssociatedToKitsBySupplier(idSupplier: any, req?: any): Observable<HttpResponse<Product[]>> {
        const options = this.createAdvancedSearchOption(req);
        return this.http.get<Product[]>(`${this.resourceUrl}/listProductsNotAssociatedKitBySupplier?idSupplier=${idSupplier}`, { params: options, observe: 'response' });
    }

    executeUnderSupply(req?: any): Observable<HttpResponse<Product[]>> {
        const options = createRequestOption(req);
        return this.http.get<Product[]>(`${this.analysisUrl}/executeUnderSupply`, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<HttpResponse<Product[]>> {
        const options = this.createAdvancedSearchOption(req);
        return this.http.get<Product[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }

    createAdvancedSearchOption = (req?: any): HttpParams => {
        let Params = new HttpParams();
        if (req) {
            // Begin assigning parameters
            if (req.page) {
                Params = Params.append('page', req.page)
            }
            if (req.size) {
                Params = Params.append('size', req.size)
            }
            if (req.query) {
                Params = Params.append('q', req.query)
            }
            if (req.filter) {
                Params = Params.append('filter', req.filter)
            }
            if (req.sort) {
                for (const s of req.sort) {
                    Params = Params.append('sort', s);
                }
            }
            if (req.under) {
                Params = Params.append('under', req.under)
            }

        }
        return Params;
    };

    convertDate(product: Product) {
        product.lastAnalyse = this.dateUtils.toDate(product.lastAnalyse);
    }
    generateEt(): Observable<any> {
        let code = "190000000000019455"
        let publicId ="uyGHW6SJYTnfr2NX1464946487743"
        return this.http.get<any>
            ("http://localhost:8088/api/shared/getEtiquetteByCode/?code="+code+"&publicId="+publicId);
    }
}
