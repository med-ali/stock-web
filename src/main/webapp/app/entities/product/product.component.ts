import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService, JhiLanguageService } from 'ng-jhipster';

import { Product } from './product.model';
import { ProductService } from './product.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { SERVER_API_URL } from '../../app.constants';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FileUploadComponent } from '../../shared/file-upload/file-upload.component';

@Component({
    selector: 'jhi-product',
    templateUrl: './product.component.html'
})
export class ProductComponent implements OnInit, OnDestroy {

    currentAccount: any;
    products: Product[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    supply: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    showFilter = false;
    uploadProductUrl = SERVER_API_URL + 'api/products/upload';

    constructor(
        private languageService: JhiLanguageService,
        private productService: ProductService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private modalService: NgbModal
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = 'code';
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }
    ngOnInit() {
        this.supply = 'All';
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInProducts();
    }
    open() {
        const modalRef = this.modalService.open(FileUploadComponent);
        modalRef.componentInstance.url = this.uploadProductUrl;
        modalRef.componentInstance.title = "product";
        modalRef.componentInstance.search = this.loadAll();
      }

    loadAll() {
        if (this.currentSearch || (this.supply && (this.supply === 'US'))) {
            this.advancedSearch();
            return;
        }
        this.productService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res) => this.onSuccess(res.body, res.headers, false),
            (res) => this.onError(res.body)
        );
    }
    search(query) {
        if (!query && (!this.supply || (this.supply === 'All'))) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        const params: any = {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        };
        if (this.supply === 'US') {
            params['under'] = true;
        }
        this.router.navigate(['/product', params]);
        this.advancedSearch();
    }

    advancedSearch() {
        const params: any = {
            page: this.page - 1,
            query: this.currentSearch,
            size: this.itemsPerPage,
            sort: this.sort()
        };
        if (this.supply === 'US') {
            params['under'] = true;
        }
        this.productService.search(params).subscribe(
            (res) => this.onSuccess(res.body, res.headers, true),
            (res) => this.onError(res.body)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transitiod() {
        const params: any = {
            page: this.page,
            size: this.itemsPerPage,
            search: this.currentSearch,
            sort: this.sort()
        }
        if (this.supply === 'US') {
            params['under'] = true;
        }//voila
        this.router.navigate(['/product'], { queryParams: params });
        this.loadAll();
    }

    transition() {
        this.router.navigate(['/product'], {
            queryParams:
                {
                    page: this.page,
                    size: this.itemsPerPage,
                    search: this.currentSearch,
                    sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
                }
        });
        this.loadAll();
    }
    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/product', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    base64toBlob(byteString) {
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
          ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ia], { type: "octet/stream" });
    }
    saveData = (function () {
        var a = document.createElement("a");
        document.body.appendChild(a);
        // a.style = "display: none";
        return function (data, fileName) {
          var json = atob(data),
            blob = this.base64toBlob(json),
            url = window.URL.createObjectURL(blob);
          a.href = url;
          a.download = fileName;
          a.click();
          window.URL.revokeObjectURL(url);
        };
    }());

    loadProductsWord(type:string) {
        this.productService.printReport(type,this.languageService.currentLang).subscribe((value) => {
            //this.product = product;msword
            this.saveData(value['res'],"products.docx");
            //this.loadEtTest();
      });
    }

    loadEtTest() {
        this.productService.generateEt().subscribe((value) => {
            //this.product = product;msword
        });
    }
    loadProductsPdf(type:string) {
        this.productService.printReport(type,this.languageService.currentLang).subscribe((value) => {
            //this.product = product;
            const linkSource = 'data:application/pdf;base64,' + value['res'];
            const downloadLink = document.createElement("a");
            const fileName = "products.pdf";
            downloadLink.href = linkSource;
            downloadLink.download = fileName;
            if (downloadLink.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                //downloadLink.setAttribute("href", window.URL.createObjectURL(linkSource));
                downloadLink.setAttribute("download", fileName);
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
            } else {
                var fileBlob = new Blob([value['res']], {type: 'application/pdf'});
                window.navigator.msSaveBlob(fileBlob, fileName); 
            }
        });
    }
    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Product) {
        return item.id;
    }
    registerChangeInProducts() {
        this.eventSubscriber = this.eventManager.subscribe('productListModification', (response) => this.loadAll());
    }

    sort() { 
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers, supply) {
        //this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.products = data;
    }
    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    executeUnderSupply() {
        this.supply = 'US';
        this.productService.executeUnderSupply({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res) => this.onSuccess(res.body, res.headers, true),
            (res) => this.onError(res.body)
        );
    }
}
