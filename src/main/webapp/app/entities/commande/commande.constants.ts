import {
    CommandeService,
    CommandePopupService,
    CommandeComponent,
    CommandeByProductComponent,
    CommandeDetailComponent,
    CommandeDialogComponent,
    CommandePopupComponent,
    CommandeDeletePopupComponent,
    CommandeDeleteDialogComponent,
    commandeRoute,
    commandePopupRoute,
    CommandeResolvePagingParams,
} from './';

export const commande_declarations = [
    CommandeComponent,
    CommandeByProductComponent,
    CommandeDetailComponent,
    CommandeDialogComponent,
    CommandeDeleteDialogComponent,
    CommandePopupComponent,
    CommandeDeletePopupComponent,
];

export const commande_entryComponents = [
    CommandeComponent,
    CommandeByProductComponent,
    CommandeDialogComponent,
    CommandePopupComponent,
    CommandeDeleteDialogComponent,
    CommandeDeletePopupComponent,
];

export const commande_providers = [
    CommandeService,
    CommandePopupService,
    CommandeResolvePagingParams,
];
