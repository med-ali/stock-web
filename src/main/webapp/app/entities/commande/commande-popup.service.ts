import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Commande } from './commande.model';
import { CommandeService } from './commande.service';
import { OrderConditionService } from '../order-condition';
import { Article } from '../article';

@Injectable()
export class CommandePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private commandeService: CommandeService,
        private orderConditionService: OrderConditionService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.commandeService.find(id).subscribe((commande) => {
                    commande.orderDate = this.datePipe
                        .transform(commande.orderDate, 'yyyy-MM-ddTHH:mm:ss');
                    commande.requiredDeliveryDate = this.datePipe
                        .transform(commande.requiredDeliveryDate, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.commandeModalRef(component, commande);
                    resolve(this.ngbModalRef);
                });
            } else {
                const commande: Commande = new Commande();
                setTimeout(() => {
                    this.ngbModalRef = this.commandeModalRef(component, commande);
                    resolve(this.ngbModalRef);
                }, 0);
                /*this.orderConditionService.findLast().subscribe((orderCondition) => {
                    const commande: Commande = new Commande();
                    //commande.orderDate = new Date();
                    commande.deliveryAddress = orderCondition.deliveryAddress;
                    commande.deliveryTerm = orderCondition.deliveryTerm;
                    commande.indicationForDelivery = orderCondition.indicationForDelivery;
                    commande.methodOfPayment = orderCondition.methodOfPayment;
                    commande.installation = orderCondition.installation;
                    commande.guaranty = orderCondition.guaranty;
                    // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                    setTimeout(() => {
                        this.ngbModalRef = this.commandeModalRef(component, commande);
                        resolve(this.ngbModalRef);
                    }, 0);
                });*/

            }
        });
    }

    commandeModalRef(component: Component, commande: Commande): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.commande = commande;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
