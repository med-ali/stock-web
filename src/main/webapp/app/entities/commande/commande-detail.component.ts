import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Commande } from './commande.model';
import { CommandeService } from './commande.service';

@Component({
    selector: 'jhi-commande-detail',
    templateUrl: './commande-detail.component.html'
})
export class CommandeDetailComponent implements OnInit, OnDestroy {

    commande: Commande;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private commandeService: CommandeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCommandes();
    }

    load(id) {
        this.commandeService.find(id).subscribe((commande) => {
            this.commande = commande;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCommandes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'commandeListModification',
            (response) => this.load(this.commande.id)
        );
    }
}
