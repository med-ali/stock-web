import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService, JhiLanguageService } from 'ng-jhipster';

import { Commande } from './commande.model';
import { CommandeService } from './commande.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, BaseEntity, PrintService } from '../../shared';
import { Supplier, SupplierService } from '../supplier';
import { OrderDetails, OrderDetailsService } from '../order-details';
import { companyPopupRoute } from '../company';
import { TranslateService } from '@ngx-translate/core';

declare var $: any;
@Component({
    selector: 'jhi-commande',
    templateUrl: './commande.component.html'
})
export class CommandeComponent implements OnInit, OnDestroy {

    currentAccount: any;
    commandes: Commande[];
    suppliers: Supplier[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    fromOrder: any;
    toOrder: any;
    status: string;
    selectedSupplier: any;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    showFilter = false;
    selectedOrder: Commande;
    @ViewChild('printOrderReport') printOrderReport: ElementRef;

    constructor(
        private languageService: JhiLanguageService,
        private commandeService: CommandeService,
        private supplierService: SupplierService,
        private orderDetailsService: OrderDetailsService,
        private printService: PrintService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = false;
            this.predicate = 'orderDate';
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }
    ngOnInit() {
        this.status = 'All';
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInCommandes();
    }
    base64toBlob(byteString) {
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
          ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ia], { type: "octet/stream" });
      }
      saveData = (function () {
        console.log("Here")
        var a = document.createElement("a");
        document.body.appendChild(a);
        // a.style = "display: none";
        return function (data, fileName) {
          console.log(data);
          var json = atob(data),
            blob = this.base64toBlob(json),
            url = window.URL.createObjectURL(blob);
          a.href = url;
          a.download = fileName;
          a.click();
          window.URL.revokeObjectURL(url);
        };
      }());
      loadRepportsWord(commande,type) {
        this.selectedOrder = commande; 
        /*this.orderDetailsService.queryByOrder(commande.id).subscribe((response) => {
            this.selectedOrder.orderDetails = response.body;
        });*/
        /*setTimeout(() => {
            this.printService.printWholePaper(this.printOrderReport.nativeElement.innerHTML);
        }, 1000);*/
        this.commandeService.loadOrders(commande,type,this.languageService.currentLang).subscribe((value) => {
            this.saveData(value['res'],"commande.docx");
    });
    }
    showWindowPrint(commande,type) {
        this.selectedOrder = commande;
        //this.translateService.currentLang
        /*this.orderDetailsService.queryByOrder(commande.id).subscribe((response) => {
            this.selectedOrder.orderDetails = response.body;
        });*/
        /*setTimeout(() => {
            this.printService.printWholePaper(this.printOrderReport.nativeElement.innerHTML);
        }, 1000);*/
        this.commandeService.loadOrders(commande,type,this.languageService.currentLang).subscribe((value) => {
            const linkSource = 'data:application/pdf;base64,' + value['res'];
            const downloadLink = document.createElement("a");
            const fileName = commande.code+".pdf";
            downloadLink.href = linkSource;
            downloadLink.download = fileName;
            //window.open("data:application/pdf;base64, " + value['res'], '', "height=600,width=800");
            //downloadLink.click();
            if (downloadLink.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                //downloadLink.setAttribute("href", window.URL.createObjectURL(linkSource));
                downloadLink.setAttribute("download", fileName);
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
            } else {
                console.log('Pdf export only works in Chrome, Firefox, and Opera.');
                var fileBlob = new Blob([value['res']], {type: 'application/pdf'});
                window.navigator.msSaveBlob(fileBlob, fileName); 
            }
    });
    }
    loadAll() {
        if (this.currentSearch || this.status !== 'All' || this.fromOrder || this.toOrder ||
            (this.selectedSupplier !== null && this.selectedSupplier !== undefined)) {
            this.advancedSearch();
            return;
        }
        this.commandeService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res) => this.onSuccess(res.body, res.headers),
            (res) => this.onError(res.body)
        );
    }

    displayAdvanced() {
        this.showFilter = !this.showFilter;
    }

    advancedSearch() {
        const params: any = {
            page: this.page - 1,
            query: this.currentSearch,
            fromOrder: this.fromOrder,
            toOrder: this.toOrder,
            size: this.itemsPerPage,
            sort: this.sort()
        };
        if (this.status !== 'All') {
            params['status'] = this.status;
        }
        if (this.selectedSupplier !== undefined && this.selectedSupplier !== null) {
            params['supplier'] = this.selectedSupplier.id;
        }
        this.commandeService.search(params).subscribe(
            (res) => this.onSuccess(res.body, res.headers),
            (res) => this.onError(res.body)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/commande'], {
            queryParams:
                {
                    page: this.page,
                    size: this.itemsPerPage,
                    search: this.currentSearch,
                    sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
                }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.status = 'All';
        this.selectedSupplier = null;
        this.fromOrder = null;
        this.toOrder = null;
        this.router.navigate(['/commande', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query && (this.status === 'All') && !this.fromOrder && !this.toOrder
            && !this.selectedSupplier) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        const params: any = {
            search: this.currentSearch,
            fromDelivery: this.fromOrder,
            toDelivery: this.toOrder,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        };
        if (this.status !== 'All') {
            params['status'] = this.status;
        }
        if (this.selectedSupplier !== undefined && this.selectedSupplier !== null) {
            params['supplier'] = this.selectedSupplier.id;
        }
        this.router.navigate(['/commande', params]);
        this.advancedSearch();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Commande) {
        return item.id;
    }
    registerChangeInCommandes() {
        this.eventSubscriber = this.eventManager.subscribe('commandeListModification', (response) => this.loadAll());
    }

    

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        //this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.commandes = data;
    }
    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    openMyModal(event) {
        document.querySelector('#' + event).classList.add('md-show');
    }

    closeMyModal() {
        (document.querySelector('#closeSupplierBtn').parentElement.parentElement).classList.remove('md-show');
    }

    assignSupplier(supplier) {
        this.selectedSupplier = supplier;
        this.closeMyModal();
        this.search(this.currentSearch);
    }
}
