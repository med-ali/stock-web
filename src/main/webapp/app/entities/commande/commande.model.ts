import { BaseEntity } from './../../shared';

export const enum OrderState {
    'OP',
    'PDL',
    'CL'
}

export class Commande implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public description?: string,
        public deliveryAddress?: string,
        public deliveryTerm?: string,
        public indicationForDelivery?: string,
        public installation?: string,
        public methodOfPayment?: string,
        public guaranty?: string,
        public totalCost?: number,
        public discount?: number,
        public totalDiscount?: number,
        public year?: number,
        public orderDate?: any,
        public requiredDeliveryDate?: any,
        public billingRequirement?: string,
        public extraCosts?: number,
        public orderState?: OrderState,
        public paymentRequiremnt?: any,
        public supplier?: any,
        public orderDetails?: any[],
        public header?: string
    ) {
    }
}
