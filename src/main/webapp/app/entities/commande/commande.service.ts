import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Commande } from './commande.model';
import { createRequestOption } from '../../shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { JhiDateUtils, JhiLanguageService } from 'ng-jhipster';
import { DatePipe } from '@angular/common';

@Injectable()
export class CommandeService {

    private resourceUrl = SERVER_API_URL + 'api/commandes';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/commandes';
    lang: string;

    constructor(private languageService: JhiLanguageService,private http: HttpClient, private datePipe: DatePipe, private dateUtils: JhiDateUtils, ) {
        this.languageService.getCurrent().then((lang)=>{
            console.log('dddddddddddddddddddddddddd'+lang)
            this.lang = lang;
         }) 
     }

    create(commande: Commande): Observable<Commande> {
        commande.orderDate = this.datePipe.transform(new Date(), 'yyyy-MM-ddTHH:mm:ss');
        this.convertDate(commande);
        return this.http.post<Commande>(this.resourceUrl, commande);
    }

    update(commande: Commande): Observable<Commande> {
        this.convertDate(commande);
        return this.http.put<Commande>(this.resourceUrl, commande);
    }

    find(id: number): Observable<Commande> {
        return this.http.get<Commande>(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<HttpResponse<Commande[]>> {
        const options = createRequestOption(req);
        return this.http.get<Commande[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    queryNotClosed(supplierId: number, req?: any): Observable<HttpResponse<Commande[]>> {
        const options = createRequestOption(req);
        return this.http.get<Commande[]>(`${this.resourceUrl}/${supplierId}/open`, { params: options, observe: 'response' });
    }

    queryByProduct(id: number, req?: any): Observable<HttpResponse<Commande[]>> {
        const options = createRequestOption(req);
        return this.http.get<Commande[]>(`${this.resourceUrl}/product/${id}`, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<HttpResponse<Commande[]>> {
        const options = this.createAdvancedSearchOption(req);
        return this.http.get<Commande[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }

    createAdvancedSearchOption = (req?: any): HttpParams => {
        let Params = new HttpParams();
        if (req) {
            // Begin assigning parameters
            if (req.page) {
                Params = Params.append('page', req.page)
            }
            if (req.size) {
                Params = Params.append('size', req.size)
            }
            if (req.query) {
                Params = Params.append('q', req.query)
            }
            if (req.filter) {
                Params = Params.append('filter', req.filter)
            }
            if (req.sort) {
                for (const s of req.sort) {
                    Params = Params.append('sort', s);
                }
            }
            if (req.supplier) {
                Params = Params.append('supplier', req.supplier)
            }
            if (req.status) {
                Params = Params.append('status', req.status)
            }
            if (req.fromOrder) {
                Params = Params.append('fromOrd', req.fromOrder)
            }
            if (req.toOrder) {
                Params = Params.append('toOrd', req.toOrder)
            }
        }
        return Params;
    };

    convertDate(commande: Commande) {
        commande.orderDate = this.dateUtils.toDate(commande.orderDate);
        commande.requiredDeliveryDate = this.dateUtils.toDate(commande.requiredDeliveryDate);
    }

    loadOrders(commande: Commande,type:String,lang:string): Observable<any> {

        return this.http.get<any>(`${this.resourceUrl}/${commande.id}/print/?type=`+type+'&lang='+lang);     
    }
}
