import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { CommandeComponent } from './commande.component';
import { CommandeByProductComponent } from './commande-by-product.component';
import { CommandeDetailComponent } from './commande-detail.component';
import { CommandePopupComponent } from './commande-dialog.component';
import { CommandeDeletePopupComponent } from './commande-delete-dialog.component';

@Injectable()
export class CommandeResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const commandeRoute: Routes = [
    {
        path: 'commande',
        component: CommandeComponent,
        resolve: {
            'pagingParams': CommandeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.commande.default'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'commande/:id',
        component: CommandeDetailComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.commande.default'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'product/commande/:productId',
        component: CommandeByProductComponent,
        resolve: {
            'pagingParams': CommandeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.commande.default'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const commandePopupRoute: Routes = [
    {
        path: 'commande-new',
        component: CommandePopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.commande.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'commande/:id/edit',
        component: CommandePopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.commande.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'commande/:id/delete',
        component: CommandeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER','ROLE_ADMIN'],
            pageTitle: 'entity.commande.default'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
