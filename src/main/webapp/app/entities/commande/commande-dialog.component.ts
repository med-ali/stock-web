import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Commande } from './commande.model';
import { CommandePopupService } from './commande-popup.service';
import { CommandeService } from './commande.service';
import { Payment, PaymentService } from '../payment';
import { Supplier, SupplierService } from '../supplier';
import { ResponseWrapper } from '../../shared';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'jhi-commande-dialog',
    templateUrl: './commande-dialog.component.html' 
     
})
export class CommandeDialogComponent implements OnInit {

    commande: Commande;
    isSaving: boolean;
    supplierModal: boolean;
    step = 1;
    descriptionSupplier:string;
    descriptionPaymentRequiremnt:string;
    orderCondition:any;
    orderConditionModal: boolean;
    descriptionOrderCondition: string;
    paymentRequiremntModal: boolean;
    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private commandeService: CommandeService,
        private datePipe: DatePipe,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.supplierModal = false;
        if (this.commande.id === undefined) {
            this.commande.requiredDeliveryDate = new Date().toJSON().slice(0, 19);
        }
        if(this.commande.supplier){
            this.descriptionSupplier = this.commande.supplier.companyName + "-" + this.commande.supplier.email;
        }
        if(this.commande.paymentRequiremnt){
            this.descriptionPaymentRequiremnt = this.commande.paymentRequiremnt.code + "-" + this.commande.paymentRequiremnt.description;
        }
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.commande.id !== undefined) {
            this.subscribeToSaveResponse(
                this.commandeService.update(this.commande));
        } else {
            this.subscribeToSaveResponse(
                this.commandeService.create(this.commande));
        }
        this.clear()
    }

    private subscribeToSaveResponse(result: Observable<Commande>) {
        result.subscribe((res: Commande) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Commande) {
        this.eventManager.broadcast({ name: 'commandeListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    openMyModal(event) {
        if (event === 'supplierModal2') {
            this.supplierModal = true;
        }
        if (event === 'orderConditionModal2') {
            this.orderConditionModal = true;
        }
        if (event === 'paymentRequiremntModal2') {
            this.paymentRequiremntModal = true;
        }
        document.querySelector('#' + event).classList.add('md-show');
    }

    closeModal(event) {
        this.supplierModal = false;
        (document.querySelector('#' + event).parentElement.parentElement).classList.remove('md-show');
    }

    closeMyModalEvent(event) {
        ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
    }

    assignSupplier(supplier) {
        this.commande.supplier = supplier;
        this.commande.header = supplier.header
        this.commande.paymentRequiremnt = this.commande.supplier.payment;
        this.commande.billingRequirement = this.commande.supplier.billingRequirement;
        this.descriptionSupplier = this.commande.supplier.companyName + "-" + this.commande.supplier.email;
        this.descriptionPaymentRequiremnt = this.commande.paymentRequiremnt.code + "-" + this.commande.paymentRequiremnt.description;
        this.closeModal('closeSupplierBtn2');
    }
    assignOrderConditions(orderCondition){
        this.orderCondition = orderCondition;
        this.descriptionOrderCondition = this.orderCondition.deliveryAddress + '-' + this.orderCondition.deliveryTerm
        orderCondition =  null; 
        this.commande.deliveryAddress =  this.orderCondition.deliveryAddress; 
        this.commande.deliveryTerm = this.orderCondition.deliveryTerm;
        this.commande.indicationForDelivery = this.orderCondition.indicationForDelivery;
        this.commande.installation = this.orderCondition.installation;
        this.commande.methodOfPayment=this.orderCondition.methodOfPayment;
        this.commande.guaranty=this.orderCondition.guaranty;
        this.closeModal('closeOrderConditionBtn2');
    }
    assignPaymentRequiremnts(payment){
        this.commande.paymentRequiremnt = payment;
        this.descriptionPaymentRequiremnt = payment.code + "-" + payment.description;
        this.closeModal('closePaymentRequiremntBtn2');
    }
}

@Component({
    selector: 'jhi-commande-popup',
    template: ''
})
export class CommandePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private commandePopupService: CommandePopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.commandePopupService
                    .open(CommandeDialogComponent as Component, params['id']);
            } else {
                this.commandePopupService
                    .open(CommandeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
