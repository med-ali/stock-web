import {
    OrderDetailsService,
    OrderDetailsPopupService,
    OrderDetailsComponent,
    OrderDetailsDetailComponent,
    OrderDetailsDialogComponent,
    OrderDetailsPopupComponent,
    OrderDetailsDeletePopupComponent,
    OrderDetailsDeleteDialogComponent,
    orderDetailsRoute,
    orderDetailsPopupRoute,
    OrderDetailsResolvePagingParams,
    ProductModalComponent,
} from './';

export const orderDetails_declarations = [
    OrderDetailsComponent,
    ProductModalComponent,
    OrderDetailsDetailComponent,
    OrderDetailsDialogComponent,
    OrderDetailsDeleteDialogComponent,
    OrderDetailsPopupComponent,
    OrderDetailsDeletePopupComponent,
];

export const orderDetails_entryComponents = [
    OrderDetailsComponent,
    ProductModalComponent,
    OrderDetailsDialogComponent,
    OrderDetailsPopupComponent,
    OrderDetailsDeleteDialogComponent,
    OrderDetailsDeletePopupComponent,
];

export const orderDetails_providers = [
    OrderDetailsService,
    OrderDetailsPopupService,
    OrderDetailsResolvePagingParams,
];
