import { Component, OnInit, OnDestroy, Input, Inject } from '@angular/core';
import { ActivatedRoute, Route } from '@angular/router';
import { Response } from '@angular/http';

import { Observable, Subscription } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiParseLinks } from 'ng-jhipster';

import { OrderDetails } from './order-details.model';
import { OrderDetailsPopupService } from './order-details-popup.service';
import { OrderDetailsService } from './order-details.service';
import { Article, ArticleService } from '../article';
import { Kit, KitService } from '../kit';
import { PackagingMethod, PackagingMethodService } from '../packaging-method';
import { Product, ProductService } from '../product';
import { Commande, CommandeService } from '../commande';
import { ResponseWrapper, ITEMS_PER_FILTER_PAGE, PagerService, ITEMS_MAX } from '../../shared';
import { OrderDetailsComponent } from './order-details.component';
import swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'jhi-order-details-dialog',
    templateUrl: './order-details-dialog.component.html'
})
export class OrderDetailsDialogComponent implements OnInit {
    orderId:number;
    orderDetails: OrderDetails;
    productSupplierCommand: any;
    isSaving: boolean;
    kit: boolean;
    routeSub: any;
    step = 1;

    eventSubscriber: Subscription;
    listAssignedArticlesProduct = [];
    listAssignedKitsProduct = [];
    description: string;
    code: string;
    minOrderPackage: number;

    products: Product[];
    pagedProducts: Product[];
    articles: Article[];
    kits: Kit[];
    productSearch: string;
    articleSearch: string;
    kitSearch: string;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    pager: any = {};

    articleModal: boolean;
    kitModal: boolean;
    productModal: boolean;
    descriptionProduct: string;
    descriptionArticle: string;
    descriptionKit: string;
    descriptionPackagingMethod: string;
    pathToArticles;
    pathToKits;
    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private orderDetailsService: OrderDetailsService,
        private commandeService: CommandeService,
        private productService: ProductService,
        private eventManager: JhiEventManager,
        private translateService: TranslateService,
        private pagerService: PagerService,
        private route: ActivatedRoute
    ) {
        this.itemsPerPage = ITEMS_PER_FILTER_PAGE;
        this.page = 1;
        this.previousPage = 1;
        this.reverse = true;
        this.predicate = 'description';
    }

    ngOnInit() {
        /* if (this.orderDetails.order.orderState === 'CL') {
            this.jhiAlertService.error('error.orderclose');
        } */
        this.pathToArticles = 'products/getAllAssignedArticles/'+this.orderDetails.order.id;
        this.pathToKits = 'products/getAllAssignedKits/'+this.orderDetails.order.id;
        //this.orderId = this.route.snapshot.params['id'];
        this.isSaving = false;
        this.kit = this.articleModal = this.kitModal = this.productModal = false;
        this.productSupplierCommand = {
            supplier: this.orderDetails.order.supplier,
            orderId: this.orderDetails.order.id
        };
        if (this.orderDetails.id !== undefined) {
            if (this.orderDetails.kit) {
                this.kit = true;
            }
        }
        if (this.orderDetails.product)
        {
            this.descriptionProduct =this.orderDetails.product.code +"-"+ this.orderDetails.product.description;
        }
        if (!this.orderDetails.kit && this.orderDetails.article)
        {
            this.descriptionArticle =this.orderDetails.article.code +"-"+ this.orderDetails.article.description;
        } 
        else if (this.orderDetails.kit && !this.orderDetails.article)
        {
            this.descriptionKit =this.orderDetails.kit.code +"-"+ this.orderDetails.kit.description;
        }
        if (this.orderDetails.packagingMethod)
        {
            this.descriptionPackagingMethod =this.orderDetails.packagingMethod.code +"-"+ this.orderDetails.packagingMethod.description;
        }
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    updateQuantityPerPackage(event) {
        this.orderDetails.quantityPerPackage = event;
        if (event != null && this.orderDetails.orderPackage != null) {
            this.orderDetails.orderedQuantity = this.orderDetails.quantityPerPackage * this.orderDetails.orderPackage;
            if (this.orderDetails.unitPrice != null && this.orderDetails.discount != null) {
                this.orderDetails.orderPrice = (this.orderDetails.unitPrice - ((this.orderDetails.unitPrice * this.orderDetails.discount) / 100)) * this.orderDetails.orderPackage;
            }
        }
    }
    updateOrderPackage(event) {
        this.orderDetails.orderPackage = event;
        if (event != null) {
            if (this.orderDetails.quantityPerPackage != null) {
                this.orderDetails.orderedQuantity = this.orderDetails.quantityPerPackage * this.orderDetails.orderPackage;
            }
            if (this.orderDetails.unitPrice != null && this.orderDetails.discount != null) {
                this.orderDetails.orderPrice = (this.orderDetails.unitPrice - ((this.orderDetails.unitPrice * this.orderDetails.discount) / 100)) * this.orderDetails.orderPackage;
            }
        }
    }
    updateUnitPrice(event) {
        this.orderDetails.unitPrice = event;
        if (event != null && this.orderDetails.discount != null && this.orderDetails.orderPackage) {
            this.orderDetails.orderPrice = (this.orderDetails.unitPrice - ((this.orderDetails.unitPrice * this.orderDetails.discount) / 100)) * this.orderDetails.orderPackage;
        }
    }
    updateDiscount(event) {
        this.orderDetails.discount = event;
        if (event != null && this.orderDetails.unitPrice != null && this.orderDetails.orderPackage) {
            this.orderDetails.orderPrice = (this.orderDetails.unitPrice - ((this.orderDetails.unitPrice * this.orderDetails.discount) / 100)) * this.orderDetails.orderPackage;
        }
    }

    save() {
        this.isSaving = true;
        if (this.orderDetails.article !== null && this.orderDetails.article !== undefined) {
            this.orderDetails.code = this.orderDetails.article.code;
            this.orderDetails.description = this.orderDetails.article.description;
        } else if (this.orderDetails.kit !== null && this.orderDetails.kit !== undefined) {
            this.orderDetails.code = this.orderDetails.kit.code;
            this.orderDetails.description = this.orderDetails.kit.description;
        }
        if (this.orderDetails.id !== undefined) {
            this.subscribeToSaveResponse(
                this.orderDetailsService.update(this.orderDetails.order.id, this.orderDetails));
        } else {
            this.subscribeToSaveResponse(
                this.orderDetailsService.create(this.orderDetails.order.id, this.orderDetails));
        }
    }

    private subscribeToSaveResponse(result: Observable<OrderDetails>) {
        result.subscribe((res: OrderDetails) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: OrderDetails) {
        this.eventManager.broadcast({ name: 'orderDetailsListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    public initSubs(entity) {
        if (entity === 'artkit') {
            this.orderDetails.article = null;
            this.orderDetails.kit = null;
            this.orderDetails.packagingMethod = null;
        }
        if (entity === 'product') {
            this.orderDetails.product = null;
            this.orderDetails.article = null;
            this.orderDetails.kit = null;
            this.orderDetails.packagingMethod = null;
        }
    }

    openBasicSwal(entity) {
        const title = this.translateService.instant('entity.' + entity + '.swal.title');
        const text = this.translateService.instant('entity.' + entity + '.swal.text');
        swal({
            'title': title,
            'text': text,
            'type': 'error'

        }).catch(swal.noop);
    }

    openMyModal(event) {
        if (event === 'articleModal') {
            this.articleModal = true;
        } else if (event === 'kitModal') {
            this.kitModal = true;
        } else if (event === 'productModal') {
            this.productModal = true;
            this.loadProducts();
        }
        document.querySelector('#' + event).classList.add('md-show');
    }

    loadProducts() {
        const params: any = {
            size: ITEMS_MAX,
            sort: this.sort()
        }
        if (this.productSearch) {
            params['query'] = this.productSearch;
        }
        this.productService.listProductsBySupplier(this.productSupplierCommand, params).subscribe((res) => {
            this.products = res.body;
            console.log('size is ' + this.products.length)
            this.setPage(this.page);
            this.totalItems = this.products.length;
            this.queryCount = this.totalItems;
        });
    }

    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            console.log('tes')
            return;
        }
        // get pager object from service
        this.pager = this.pagerService.getPager(this.products.length, page, this.itemsPerPage);
        // get current page of items
        this.pagedProducts = this.products.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    closeModal(event) {
        this.resetParameters();
        this.articleModal = this.kitModal = this.productModal = false;
        (document.querySelector('#' + event).parentElement.parentElement).classList.remove('md-show');
    }

    resetParameters() {
        this.page = 1;
        this.predicate = 'description';
        this.reverse = true;
        this.productSearch = '';
    }

    closeMyModalEvent(event) {
        ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
    }

    assignArticle(normalisation) {
        this.description = normalisation.article.description;
        this.code = normalisation.article.code;
        this.orderDetails.article = normalisation.article;
        this.orderDetails.kit = null;
        this.orderDetails.code = normalisation.article.code;
        this.orderDetails.packagingMethod = normalisation.article.packagingMethod;
        this.orderDetails.quantityPerPackage = normalisation.article.quantityPerPackage;
        this.orderDetails.discount = normalisation.article.discount;
        this.orderDetails.unitPrice = normalisation.article.unitPrice;
        this.orderDetails.orderPackage = normalisation.article.minPackagingOrder;
        this.minOrderPackage = normalisation.article.minPackagingOrder;
        this.getOrderPrice();
        this.calculateQuantity();
        this.kit = false;
        this.descriptionArticle =this.orderDetails.article.code +"-"+ this.orderDetails.article.description;
        this.closeModal('closeArticleBtn');
    }

    assignKit(normalisation) {
        this.description = normalisation.kit.description;
        this.code = normalisation.kit.code;
        this.orderDetails.kit = normalisation.kit;
        this.orderDetails.article = null;
        this.orderDetails.code = normalisation.kit.code;
        this.orderDetails.packagingMethod = normalisation.kit.packagingMethod;
        this.orderDetails.discount = normalisation.kit.discount;
        this.orderDetails.unitPrice = normalisation.kit.unitPrice;
        this.orderDetails.quantityPerPackage = normalisation.kit.quantityPerPackage;
        this.orderDetails.orderPackage = normalisation.kit.minPackagingOrder;
        this.minOrderPackage = normalisation.kit.minPackagingOrder;
        this.getOrderPrice();
        this.calculateQuantity();
        this.kit = true;
        this.descriptionKit =this.orderDetails.kit.code +"-"+ this.orderDetails.kit.description;
        this.closeModal('closeKitBtn');
    }

    calculateQuantity() {
        this.orderDetails.orderedQuantity = this.orderDetails.orderPackage * this.orderDetails.quantityPerPackage;
        this.getOrderPrice();
    }

    assignProduct(product) {
        this.orderDetails.product = product;
        this.orderDetails.orderedQuantity = 1;
        this.assignDefaultArticleKit(this.orderDetails.product);
        this.getOrderPrice();
        this.descriptionProduct =this.orderDetails.product.code +"-"+ this.orderDetails.product.description;
        this.closeModal('closeProductBtn');
    }

    assignDefaultArticleKit(product) {
        this.listAssignedArticlesProduct = [];
        this.listAssignedKitsProduct = [];
        if (product) { 
            this.productService.getAllAssignedProductArticles(product,this.orderDetails.order.id).subscribe((data) => {
                this.listAssignedArticlesProduct = data.body;
                if (this.listAssignedArticlesProduct && this.listAssignedArticlesProduct.length > 0) {
                    this.orderDetails.article = this.listAssignedArticlesProduct[0].article;
                    this.orderDetails.code = this.orderDetails.article.code;
                    this.description = this.orderDetails.article.description;
                    this.orderDetails.kit = null;
                    this.kit = false;
                    this.assignArticle(this.listAssignedArticlesProduct[0]);
                } else {
                    this.productService.getAllAssignedProductKits(product,this.orderDetails.order.id).subscribe((res) => {
                        this.listAssignedKitsProduct = res.body;
                        if (this.listAssignedKitsProduct && this.listAssignedKitsProduct.length > 0) {
                            this.orderDetails.kit = this.listAssignedKitsProduct[0].kit;
                            this.orderDetails.code = this.orderDetails.kit.code;
                            this.description = this.orderDetails.kit.description;
                            this.orderDetails.article = null;
                            this.kit = true;
                            this.assignKit(this.listAssignedKitsProduct[0]);
                        }
                    });
                }
            });
        }
    }

    getOrderPrice() {
        if (this.orderDetails.discount >= 0 && this.orderDetails.unitPrice >= 0 && this.orderDetails.orderPackage > 0) {
            this.orderDetails.orderPrice = Number((this.orderDetails.unitPrice * ((100 - this.orderDetails.discount) / 100) * this.orderDetails.orderPackage).toFixed(3));
        } else {
            this.orderDetails.orderPrice = 0;
        }
    }
}

@Component({
    selector: 'jhi-order-details-popup',
    template: ''
})
export class OrderDetailsPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private orderDetailsPopupService: OrderDetailsPopupService) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.orderDetailsPopupService
                    .open(OrderDetailsDialogComponent as Component, params['id'], params['idOrder']);
            } else {
                this.orderDetailsPopupService
                    .open(OrderDetailsDialogComponent as Component, null, params['idOrder']);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
