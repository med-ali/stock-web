import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { OrderDetails } from './order-details.model';
import { createRequestOption } from '../../shared';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { JhiDateUtils } from 'ng-jhipster';

@Injectable()
export class OrderDetailsService {

    private resourceUrl = SERVER_API_URL + 'api/order-details';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(orderId: number, orderDetails: OrderDetails): Observable<OrderDetails> {
        this.convertDate(orderDetails);
        return this.http.post<OrderDetails>(`${this.resourceUrl}/${orderId}`, orderDetails);
    }

    update(orderId: number, orderDetails: OrderDetails): Observable<OrderDetails> {
        this.convertDate(orderDetails);
        return this.http.put<OrderDetails>(`${this.resourceUrl}/${orderId}`, orderDetails);
    }

    find(id: number): Observable<OrderDetails> {
        return this.http.get<OrderDetails>(`${this.resourceUrl}/${id}`);
    }

    queryByOrder(orderId: number, req?: any): Observable<HttpResponse<OrderDetails[]>> {
        const options = createRequestOption(req);
        return this.http.get<OrderDetails[]>(`${this.resourceUrl}/order/${orderId}`, { params: options, observe: 'response' });
    }

    queryByOrderPartiallyReceived(orderId: number, req?: any): Observable<HttpResponse<OrderDetails[]>> {
        const options = createRequestOption(req);
        return this.http.get<OrderDetails[]>(`${this.resourceUrl}/order/${orderId}/progress`, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(orderId: number, req?: any): Observable<HttpResponse<OrderDetails[]>> {
        const options = this.createAdvancedSearchOption(req);
        return this.http.get<OrderDetails[]>(`${this.resourceSearchUrl}/${orderId}/order-details`, { params: options, observe: 'response' });
    }

    createAdvancedSearchOption = (req?: any): HttpParams => {
        let Params = new HttpParams();
        if (req) {
            // Begin assigning parameters
            if (req.page) {
                Params = Params.append('page', req.page)
            }
            if (req.size) {
                Params = Params.append('size', req.size)
            }
            if (req.query) {
                Params = Params.append('q', req.query)
            }
            if (req.filter) {
                Params = Params.append('filter', req.filter)
            }
            if (req.sort) {
                for (const s of req.sort) {
                    Params = Params.append('sort', s);
                }
            }
            if (req.orderId) {
                Params = Params.append('order', req.orderId)
            }
            if (req.suppfromDellier) {
                Params = Params.append('fromDel', req.fromDel)
            }
            if (req.toDel) {
                Params = Params.append('toDel', req.toDel)
            }
        }
        return Params;
    };

    convertDate(orderDetails: OrderDetails) {
        orderDetails.deliveryDate = this.dateUtils.toDate(orderDetails.deliveryDate);
    }
}
