import { Injectable, Component } from '@angular/core';
import {Router, ActivatedRoute } from '@angular/router';
import {NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {DatePipe } from '@angular/common';
import {OrderDetails } from './order-details.model';
import {OrderDetailsService } from './order-details.service';
import {CommandeService } from '../commande/commande.service';

@Injectable()
export class OrderDetailsPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private route: ActivatedRoute,
        private commandeService: CommandeService,
        private orderDetailsService: OrderDetailsService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component | any, id?: number | any, idOrder?: number): Promise < NgbModalRef >  {
        return new Promise < NgbModalRef > ((resolve, reject) =>  {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }
            if (id) {
                this.orderDetailsService.find(id).subscribe((orderDetails) =>  {
                    orderDetails.deliveryDate = this.datePipe
                        .transform(orderDetails.deliveryDate, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.orderDetailsModalRef(component, orderDetails);
                    resolve(this.ngbModalRef);
                });
            }else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() =>  {
                    const orderDetails: OrderDetails = new OrderDetails();
                    if (idOrder) {
                        this.commandeService.find(idOrder).subscribe((order) =>  {
                                orderDetails.order = order;
                                this.ngbModalRef = this.orderDetailsModalRef(component, orderDetails);
                        resolve(this.ngbModalRef);
                        });
                    }
                }, 0);
            }
        });
    }

    orderDetailsModalRef(component: Component, orderDetails: OrderDetails): NgbModalRef {
        const modalRef = this.modalService.open(component,  {size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.orderDetails = orderDetails;
        modalRef.result.then((result) =>  {
            this.router.navigate([ {outlets: {popup: null }}],  {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        }, (reason) =>  {
            this.router.navigate([ {outlets: {popup: null }}],  {replaceUrl: true, queryParamsHandling: 'merge'});
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
