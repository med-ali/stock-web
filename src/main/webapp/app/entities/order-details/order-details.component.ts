import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { OrderDetails } from './order-details.model';
import { OrderDetailsService } from './order-details.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { CommandeService } from '../commande';
import { PackagingMethod, PackagingMethodService } from '../packaging-method';
import {NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OrderDetailsDialogComponent} from './';
import {
    OrderDetailsPopupService, 
} from './';
@Component({
    selector: 'jhi-order-details',
    templateUrl: './order-details.component.html'
})
export class OrderDetailsComponent implements OnInit, OnDestroy {

    currentAccount: any;
    orderDetails: OrderDetails[];
    defaultPackagingMethod: PackagingMethod;
    orderId: number;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    fromDel: any;
    toDel: any;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    order: any;

    constructor(private modalService: NgbModal,
        private servicePop:OrderDetailsPopupService,
        private orderDetailsService: OrderDetailsService,
        private commandeService: CommandeService,
        private packagingMethodService: PackagingMethodService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            if (data.pagingParams){
                this.page = data.pagingParams.page;
                this.previousPage = data.pagingParams.page;
                this.reverse = data.pagingParams.ascending;
                this.predicate = data.pagingParams.predicate;
            }
            
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    ngOnInit() {
        //this.loadDefaultPackagingMethod();
        this.eventSubscriber = this.activatedRoute.params.subscribe((params) => {
            if (!this.orderId) {
                this.orderId = params['orderId'];
                this.loadByOrder(this.orderId);
                this.commandeService.find(this.orderId).subscribe((order) => {
                    this.order = order;
                });
            }
        });
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInOrderDetails();
    }
    
    loadDefaultPackagingMethod() {
        this.packagingMethodService.getDefaultPackagingMethod().subscribe((data) => {
            if (data) {
                this.defaultPackagingMethod = data;
            }
        });

    };

    loadByOrder(orderId) {
        if (this.currentSearch) {
            const params: any = {
                page: this.page - 1,
                query: this.currentSearch,
                orderId: this.orderId,
                fromDel: this.fromDel,
                toDel: this.toDel,
                size: this.itemsPerPage,
                sort: this.sort()
            };
            this.orderDetailsService.search(this.orderId, params).subscribe(
                (res) => this.onSuccess(res.body, res.headers),
                (res) => this.onError(res.body)
            );
            return;
        }
        this.orderDetailsService.queryByOrder(orderId, {
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res) => this.onSuccess(res.body, res.headers),
            (res) => this.onError(res.body)
            );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/order/' + this.orderId + '/order-details'], {
            queryParams:
                {
                    page: this.page,
                    size: this.itemsPerPage,
                    search: this.currentSearch,
                    sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
                }
        });
        this.loadByOrder(this.orderId);
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/order/' + this.orderId + '/order-details', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadByOrder(this.orderId);
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/order/' + this.orderId + '/order-details', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadByOrder(this.orderId);
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: OrderDetails) {
        return item.id;
    }
    registerChangeInOrderDetails() {
        this.eventSubscriber = this.eventManager.subscribe('orderDetailsListModification', (response) => this.loadByOrder(this.orderId));
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        //this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.orderDetails = data;
    }
    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
    openNew() {
        this.servicePop.open(OrderDetailsDialogComponent,undefined,this.orderId)
        /*const modalRef = this.modalService.open( OrderDetailsDialogComponent);
        const orderDetails: OrderDetails = new OrderDetails();
        orderDetails.order = this.order
        modalRef.componentInstance.orderDetails = orderDetails;
       modalRef.componentInstance.orderId = this.orderId;*/
    }
    openEdit(id:number) {
        this.servicePop.open(OrderDetailsDialogComponent,id,this.orderId)
        /*const modalRef = this.modalService.open( OrderDetailsDialogComponent);
        const orderDetails: OrderDetails = new OrderDetails();
        orderDetails.order = this.order
        modalRef.componentInstance.orderDetails = orderDetails;
       modalRef.componentInstance.orderId = this.orderId;*/
    }    
}
