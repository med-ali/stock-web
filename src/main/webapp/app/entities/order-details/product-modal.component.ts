import { Component, OnInit, OnDestroy, EventEmitter, Output, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { ITEMS_PER_FILTER_PAGE, FilterService } from '../../shared';

@Component({
    selector: 'jhi-product-modal',
    templateUrl: './product-modal.component.html'
})
export class ProductModalComponent implements OnDestroy, OnChanges {

    error: any;
    success: any;
    currentSearch: string;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    @Output()
    onSelect = new EventEmitter();

    @Input() entity: any;
    @Input() path: any;
    @Input() method: any;
    @Input() objPost: any;
    @Input() headers: any[];
    @Input() searchNotAllowed: boolean;
    products: any[];

    constructor(
        private filterService: FilterService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
    ) {
        this.itemsPerPage = ITEMS_PER_FILTER_PAGE;
        this.page = 1;
        this.previousPage = 1;
        this.reverse = true;
        this.predicate = 'id';
    }

    loadAll(path: string, method: string, objPost?: any) {
        if (this.currentSearch) {
            this.filterService.search(path, {
                query: this.currentSearch,
                sort: this.sort()
            }).subscribe(
                (res) => this.onSuccess(res.body, res.headers),
                (res) => this.onError(res.body)
            );
            return;
        }
        this.filterService.queryPost(path, objPost, {
            sort: this.sort()
        }).subscribe(
            (res) => this.onSuccess(res.body, res.headers),
            (res) => this.onError(res.body)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.page = 1;
        this.loadAll(this.path, this.method, this.objPost);
    }

    clear() {
        this.page = 1;
        this.currentSearch = '';
        this.loadAll(this.path, this.method, this.objPost);
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll(this.path, this.method, this.objPost);
    }

    ngOnChanges(changes: SimpleChanges) {
        this.loadAll(this.path, this.method, this.objPost);
    }

    ngOnDestroy() {

    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.totalItems = data.length;
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.products = data;
    }
    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    selectEntity(entity) {
        this.onSelect.emit(entity);
    }
}
