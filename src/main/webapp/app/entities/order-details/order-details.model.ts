import { BaseEntity } from './../../shared';

export class OrderDetails implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public description?: string,
        public measure?: string,
        public quantityPerPackage?: number,
        public unitPrice?: number,
        public orderedQuantity?: number,
        public receivedQuantity?: number,
        public orderPackage?: number,
        public orderPrice?: number,
        public discount?: number,
        public deliveryDate?: any,
        public billingRequirement?: string,
        public article?: any,
        public kit?: any,
        public packagingMethod?: any,
        public product?: any,
        public order?: any,
    ) {
    }
}
